package org.filters;

import javafx.util.Pair;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 03.09.16.
 */
@Data
@AllArgsConstructor
public class CommonFilter {
    private String name;
    private Enum enumName;
    private FilterType filterType;
    private Object value;
    private Object from;
    private Object to;

    interface FilterMatcher{public boolean match(Object o);}

    private Map<FilterType, FilterMatcher> matches = new HashMap<>();

    public enum FilterType{
        SINGLE_INT,
        SINGLE_LONG,
        DATE_RANGE,
        LONG_RANGE,
        STRING_MATCH,
        STRING_CONTAIN
    }


    public CommonFilter() {
        matches.put(FilterType.SINGLE_LONG, o -> o instanceof Long && o.equals(value));
        matches.put(FilterType.STRING_MATCH, o -> o instanceof String && o.equals(value));
        matches.put(FilterType.STRING_CONTAIN, o -> o instanceof String && ((String) o).contains(((String) value)));
        matches.put(FilterType.DATE_RANGE, o -> {
            if(o instanceof Date)
            {
                Date date = (Date) o;
                return ((Date) from).before(date) && ((Date) to).after(date);
            }
            if(o instanceof Pair)
            {
                Date fromDate = (Date) ((Pair) o).getKey();
                Date toDate = (Date) ((Pair) o).getValue();
                return ((Date) from).before(fromDate) && toDate.after(((Date)to));
            }
            return false;
        });
    }

    public boolean match(Object o)
    {
        return matches.get(filterType).match(o);
    }

    public static CommonFilter create(FilterType filterType, Enum e, Object value)
    {
        CommonFilter result = new CommonFilter();
        result.setFilterType(filterType);
        result.setEnumName(e);
        result.setValue(value);
        return result;
    }

}
