package org.filters;

import javafx.util.Pair;
import lombok.Getter;
import org.freshcode.dto.LogPeriodDto;
import org.freshcode.gui.utils.Etype;

import java.time.Period;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 03.09.16.
 */
public class DtoFilters {
    @Getter
    private Map<Etype, DtoSingleFilter> filters = new HashMap<>();

    public DtoFilters() {
        DtoSingleFilter<LogPeriodDto, LogPeriodDto.Filters> logPeriodFilter = new DtoSingleFilter<>();
        logPeriodFilter.getPredicates().put(LogPeriodDto.Filters.MEMBER_ID,
                (t, filter) -> filter.match(t.getMember().getPerson().getId()));
        logPeriodFilter.getPredicates().put(LogPeriodDto.Filters.DATE,
                (t, filter) -> filter.match(new Pair<>(t.getFromDate(), t.getToDate())));

        filters.put(Etype.LOG_PERIOD, logPeriodFilter);



    }
}
