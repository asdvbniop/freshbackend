package org.filters;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 03.09.16.
 */
public class DtoSingleFilter<E,F extends Enum<F>> {

    public interface Filter<E> {
        public boolean process(E t, CommonFilter filter);
    }

    @Getter
    private Map<Enum<F>,Filter<E>> predicates = new HashMap<>();

    public boolean match(E e, CommonFilter filter)
    {
        return predicates.get(filter.getName()).process(e, filter);
    }
}
