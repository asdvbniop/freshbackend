package org.freshcode.api;

import org.freshcode.domain.*;
import org.freshcode.dto.AgreementDto;
import org.freshcode.dto.BidDto;
import org.freshcode.dto.LeadDto;
import org.freshcode.repository.*;
import org.freshcode.utils.CommonTransform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by user on 04.07.16.
 */
@RestController("agreements")
@Transactional
public class AgreementApi {

    @Autowired
    AgreementRepository agreementRepository;
    @Autowired
    LeadRepository leadRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    InvoiceTemplateRepository invoiceTemplateRepository;

    @RequestMapping(path = "/agreements", method = RequestMethod.GET)
    public List<AgreementDto> getAllAgreements()
    {
        return CommonTransform.transformBeans(agreementRepository.findByActiveTrue(), AgreementDto.class).stream()
                .map(AgreementDto::cut).collect(Collectors.toList());
    }

    @RequestMapping(path = "/agreement/{id}", method = RequestMethod.GET)
    public AgreementDto getAgreementById(@PathVariable(value = "id") Long id)
    {
        Agreement agreement = agreementRepository.findOne(id);
        //TODO workaround, need fix for future
        List<Long> notActiveMembers = agreement.getMembers().stream().filter(m -> !m.isActive()).map(AbstractEntity::getId).collect(Collectors.toList());
        AgreementDto result = CommonTransform.transformBean(agreement, AgreementDto.class).cutOuter();
        result.getMembers().removeIf(m -> notActiveMembers.contains(m.getId()));
        return result;
    }

    @RequestMapping(path = "/agreement", method = RequestMethod.POST)
    public AgreementDto createAgreement(@RequestBody AgreementDto agreementDto)
    {
        Assert.notNull(agreementDto.getLead());
        Assert.notNull(agreementDto.getLead().getId());
        LeadDto leadDto = agreementDto.getLead();
        InvoiceTemplate template = new InvoiceTemplate(agreementDto.getCurrency(),null,null,null,true,true,true,new HashMap<>());
        agreementDto.cut();

        Agreement agreement = agreementRepository.save(CommonTransform.transformBean(agreementDto, Agreement.class));
        agreement.setInvoiceTemplate(invoiceTemplateRepository.save(template));
        agreementRepository.save(agreement);

        Lead lead = leadRepository.findOne(leadDto.getId());
        agreement.setLead(lead);
        lead.setAgreement(agreement);
        return getAgreementById(agreement.getId());
    }

    @RequestMapping(path = "/agreement", method = RequestMethod.PATCH)
    public AgreementDto updateAgreement(@RequestBody AgreementDto agreementDto)
    {
        return agreementDto;
    }

}
