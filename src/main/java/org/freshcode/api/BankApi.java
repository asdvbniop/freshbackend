package org.freshcode.api;

import org.freshcode.domain.*;
import org.freshcode.domain.enums.EntityType;
import org.freshcode.dto.*;
import org.freshcode.repository.*;
import org.freshcode.utils.CommonTransform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by user on 04.07.16.
 */
@RestController("banks")
@Transactional
public class BankApi {

    @Autowired
    BankRepository bankRepository;

    @Autowired
    FinancialInfoRepository financialInfoRepository;

    @Autowired
    UserDataRepository userDataRepository;

    @Autowired
    AddrRepository addrRepository;

    @RequestMapping(path = "/banks", method = RequestMethod.GET)
    public List<BankDto> getAllBanks()
    {
        List<Bank> result = bankRepository.findByActiveTrue();
        return CommonTransform.transformBeans(result, BankDto.class);
    }

    @RequestMapping(path = "/bank/{id}", method = RequestMethod.GET)
    public BankDto getBankById(@PathVariable(value = "id") Long id)
    {
        Bank result = bankRepository.findOne(id);
        return CommonTransform.transformBean(result, BankDto.class);
    }

    @RequestMapping(path = "/bank", method = RequestMethod.PATCH)
    public BankDto updateBank(@RequestBody BankDto bankDto)
    {
        Assert.notNull(bankDto.getId());
        Bank bank = bankRepository.findOne(bankDto.getId());
        CommonTransform.copyBean(bankDto,bank);
        bankRepository.save(bank);
        return getBankById(bankDto.getId());

    }

    @RequestMapping(path = "/bank", method = RequestMethod.POST)
    public BankDto createBank(@RequestBody BankDto bankDto)
    {
        Assert.isNull(bankDto.getId());

        FullAddressData eng=null,rus=null;

        if(bankDto.getEng()!=null)
            eng = addrRepository.save(CommonTransform.transformBean(bankDto.getEng(), FullAddressData.class));
        if(bankDto.getRus()!=null)
            rus = addrRepository.save(CommonTransform.transformBean(bankDto.getRus(), FullAddressData.class));

        bankDto.setEng(null);
        bankDto.setRus(null);


        Bank bank = bankRepository.save(CommonTransform.transformBean(bankDto, Bank.class));
        if(eng!=null)
            bank.setEng(eng);
        if(rus!=null)
            bank.setRus(rus);
        bankRepository.save(bank);
        return getBankById(bank.getId());
    }

    @RequestMapping(path = "/financialInfo/{id}", method = RequestMethod.GET)
    public FinancialInfoDto getFinancialInfoDto(@PathVariable Long id)
    {
        return CommonTransform.transformBean(financialInfoRepository.findOne(id), FinancialInfoDto.class);
    }

    @RequestMapping(path = "/financialInfo", method = RequestMethod.PATCH)
    public FinancialInfoDto updateFinancialInfoDto(@RequestBody FinancialInfoDto infoDto)
    {
        FinancialInfo old = financialInfoRepository.findOne(infoDto.getId());
        CommonTransform.copyBean(infoDto, old);
        financialInfoRepository.save(old);
        return getFinancialInfoDto(infoDto.getId());
    }

    @RequestMapping(path = "/financialInfo", method = RequestMethod.POST)
    public FinancialInfoDto createFinancialInfoDto(@RequestBody FinancialInfoDto infoDto)
    {
        Assert.isNull(infoDto.getId());
        Assert.notNull(infoDto.getBank());
        FinancialInfo info = financialInfoRepository.save(CommonTransform.transformBean(infoDto, FinancialInfo.class));
        if(infoDto.getParentId()!=null && infoDto.getParentType().equals(EntityType.USER_DATA))
        {
            PersonFullData data = userDataRepository.findOne(infoDto.getParentId());
            switch (info.getCurrency()) {
                case USD:
                    data.setUsdAcc(info);
                    break;
                case EUR:
                    data.setEurAcc(info);
                    break;
                case UAH:
                    data.setUahAcc(info);
                    break;
            }
            userDataRepository.save(data);
        }
        return getFinancialInfoDto(info.getId());
    }
}
