package org.freshcode.api;

import org.freshcode.domain.Bid;
import org.freshcode.domain.Lead;
import org.freshcode.dto.BidDto;
import org.freshcode.dto.LeadDto;
import org.freshcode.repository.BidRepository;
import org.freshcode.repository.LeadRepository;
import org.freshcode.utils.CommonTransform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by user on 04.07.16.
 */
@RestController("bids")
@Transactional
public class BidApi {

    @Autowired
    BidRepository bidRepository;

    @Autowired
    LeadRepository leadRepository;

    @RequestMapping(path = "/bids", method = RequestMethod.GET)
    public List<BidDto> getAllBids()
    {
        List<Bid> result = bidRepository.findByActiveTrue();

        return CommonTransform.transformBeans(result, BidDto.class).stream()
                .map(BidDto::cut).collect(Collectors.toList());
    }

    @RequestMapping(path = "/bid/{id}", method = RequestMethod.GET)
    public BidDto getBidById(@PathVariable(value = "id") String id)
    {
        Bid result = bidRepository.findOne(Long.valueOf(id));
        return CommonTransform.transformBean(result, BidDto.class).cut();
    }

    @RequestMapping(path = "/bid", method = RequestMethod.POST)
    public BidDto createBid(@RequestBody BidDto bidDto)
    {
        LeadDto leadDto = bidDto.getLead();
        bidDto.setLead(null);
        Bid bid = CommonTransform.transformBean(bidDto, Bid.class);
        bid = bidRepository.save(bid);
        if(leadDto!=null)
        {
            Lead lead = leadRepository.findOne(leadDto.getId());
            bid.setLead(lead);
            lead.setBid(bid);
        }
        return CommonTransform.transformBean(bid, BidDto.class).cut();
    }

    @RequestMapping(path = "/bid", method = RequestMethod.PATCH)
    public BidDto updateBid(@RequestBody BidDto bidDto)
    {
        bidDto.setLead(null);
        Bid bid = bidRepository.findOne(bidDto.getId());
        bidDto.copy(bid);
        bidRepository.save(bid);
        return CommonTransform.transformBean(bid, BidDto.class).cut();
    }

}
