package org.freshcode.api;

import org.freshcode.domain.Client;
import org.freshcode.domain.ContactInfo;
import org.freshcode.domain.FinancialInfo;
import org.freshcode.domain.Lead;
import org.freshcode.dto.ClientDto;
import org.freshcode.dto.ContactInfoDto;
import org.freshcode.dto.FinancialInfoDto;
import org.freshcode.gui.controllers.LeadController;
import org.freshcode.repository.ClientRepository;
import org.freshcode.repository.ContactInfoRepository;
import org.freshcode.repository.FinancialInfoRepository;
import org.freshcode.repository.LeadRepository;
import org.freshcode.utils.CommonTransform;
import org.freshcode.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by user on 04.07.16.
 */
@RestController("clients")
@Transactional
public class ClientApi {

    @Autowired
    ClientRepository clientRepository;
    @Autowired
    ContactInfoRepository contactInfoRepository;
    @Autowired
    FinancialInfoRepository financialInfoRepository;
    @Autowired
    LeadRepository leadRepository;


    @RequestMapping(path = "/clients", method = RequestMethod.GET)
    public List<ClientDto> getAllClients() {
        return CommonTransform.transformBeans(clientRepository.findByActiveTrue(), ClientDto.class).stream()
                .map(ClientDto::cutInner).collect(Collectors.toList());
    }

    @RequestMapping(path = "/client/{id}", method = RequestMethod.GET)
    public ClientDto getClientById(@PathVariable(value = "id") Long id) {
        return CommonTransform.transformBean(clientRepository.findOne(id), ClientDto.class).cutOuter();
    }

    @RequestMapping(path = "/client", method = RequestMethod.POST)
    public ClientDto newClient(@RequestBody ClientDto clientDto) {
        Lead lead = null;
        if(clientDto.getLead()!=null)
        {
            lead = leadRepository.findOne(clientDto.getLead().getId());
        }
        clientDto.setLead(null);
        Client client = CommonTransform.transformBean(clientDto, Client.class);
        clientRepository.save(client);
        if(lead!=null)
        {
            lead.setClient(client);
            leadRepository.save(lead);
        }
        return getClientById(client.getId());
    }

    @RequestMapping(path = "/client", method = RequestMethod.PATCH)
    public ClientDto updateClient(@RequestBody ClientDto clientDto){
        return clientDto;
    }

    @RequestMapping(path = "/client/{id}", method = RequestMethod.DELETE)
    public void deleteClient(@PathVariable(value = "id") Long id){
        Client client = clientRepository.findOne(id);
        client.setActive(false);
        clientRepository.save(client);
    }

    @RequestMapping(path = "/contactInfo/{id}", method = RequestMethod.GET)
    public ContactInfoDto getContactInfo(@PathVariable Long id)
    {
        return CommonTransform.transformBean(contactInfoRepository.findOne(id), ContactInfoDto.class);
    }

    @RequestMapping(path = "/contactInfo", method = RequestMethod.PATCH)
    public ContactInfoDto updateContactInfo(@RequestBody ContactInfoDto contactInfoDto)
    {
        ContactInfo old = contactInfoRepository.findOne(contactInfoDto.getId());
        CommonTransform.copyBean(contactInfoDto, old);
        contactInfoRepository.save(old);
        return getContactInfo(contactInfoDto.getId());
    }


}
