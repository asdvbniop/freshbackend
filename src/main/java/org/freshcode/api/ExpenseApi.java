package org.freshcode.api;

import org.freshcode.domain.*;
import org.freshcode.dto.*;
import org.freshcode.repository.AgreementRepository;
import org.freshcode.repository.ExpenseRepository;
import org.freshcode.repository.PersonRepository;
import org.freshcode.repository.ProjectRepository;
import org.freshcode.utils.CommonTransform;
import org.freshcode.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by user on 13.08.16.
 */
@Transactional
@RestController("expenses")
public class ExpenseApi {

    @Autowired
    ExpenseRepository expenseRepository;
    @Autowired
    PersonRepository personRepository;
    @Autowired
    AgreementRepository agreementRepository;

    @RequestMapping(path = "expenses", method = RequestMethod.GET)
    public List<ExpenseDto> getAll()
    {
        List<Agreement> agreements = agreementRepository.findByActiveTrue();
        List<ExpenseDto> results = new ArrayList<>();

        for (Agreement agreement : agreements) {
            AgreementDto dtoa = CommonTransform.transformBean(agreement, AgreementDto.class).cut();
            dtoa.setExpenses(null);
            dtoa.setInvoices(null);
            List<ExpenseDto> dtoList = CommonTransform.transformBeans(agreement.getExpenses(), ExpenseDto.class);
            dtoList.forEach(expenseDto -> expenseDto.setAgreement(dtoa));
            results.addAll(dtoList);

        }
        return results;
    }

    @RequestMapping(path = "expense/{id}", method = RequestMethod.GET)
    public ExpenseDto getExpenseById(@PathVariable Long id)
    {
        return CommonTransform.transformBean(expenseRepository.findOne(id), ExpenseDto.class).cut();
    }

    @RequestMapping(path = "expense", method = RequestMethod.POST)
    public ExpenseDto createExpense(@RequestBody ExpenseDto expenseDto)
    {
        Assert.isNull(expenseDto.getId());
        UserDto userDto = expenseDto.getPerson();
        AgreementDto agreementDto = expenseDto.getAgreement();
        expenseDto.setAgreement(null);expenseDto.setPerson(null);
        Expense expense = CommonTransform.transformBean(expenseDto, Expense.class);
        expense = expenseRepository.save(expense);
        if(userDto!=null)
        {
            Person person = personRepository.findOne(userDto.getId());

        }
        if(agreementDto!=null)
        {
            Agreement agreement = agreementRepository.findOne(agreementDto.getId());
            expense.setAgreement(agreement);
            agreement.setExpenses(Utils.alw(agreement.getExpenses(), expense));
        }
        return CommonTransform.transformBean(expenseRepository.findOne(expense.getId()), ExpenseDto.class).cut();
    }

    @RequestMapping (path = "expense", method = RequestMethod.PATCH)
    public ExpenseDto updateExpense(@RequestBody ExpenseDto expenseDto)
    {
        return expenseDto;
    }

}
