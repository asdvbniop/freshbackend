package org.freshcode.api;

import org.freshcode.domain.FileModel;
import org.freshcode.domain.Lead;
import org.freshcode.dto.FileDto;
import org.freshcode.repository.FileRepository;
import org.freshcode.repository.LeadRepository;
import org.freshcode.utils.CommonTransform;
import org.freshcode.sevice.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 28.08.16.
 */
@RestController("files")
@Transactional
public class FileApi {

    @Autowired
    FileRepository fileRepository;
    @Autowired
    LeadRepository leadRepository;
    @Autowired
    StorageService storageService;


    @RequestMapping(path = "filesForLead/{id}", method = RequestMethod.GET)
    public List<FileDto> getFilesForLead(@PathVariable("id") Long leadId)
    {
        Lead lead = leadRepository.findOne(leadId);
//        return CommonTransform.transformBeans(lead.getFileModels(), FileDto.class);
        return new ArrayList<>();
    }

    @RequestMapping(path = "file/{id}", method = RequestMethod.GET)
    public FileDto getFileById(@PathVariable("id") Long id)
    {
        return CommonTransform.transformBean(fileRepository.findOne(id), FileDto.class);
    }

    @RequestMapping(path = "file", method = RequestMethod.PATCH)
    public FileDto updateFile(@RequestBody FileDto fileDto)
    {
        FileModel fileModel = fileRepository.findOne(fileDto.getId());
        fileModel.setFilename(fileDto.getFilename());
        fileModel.setUpdated(new Date());
        fileRepository.save(fileModel);
        //TODO rename real fileModel here
        return getFileById(fileModel.getId());
    }

    @RequestMapping(value = "/file/upload", method = RequestMethod.POST)
    public FileDto handleFileUpload(@RequestParam("file") MultipartFile file) {
        storageService.store(file);
        FileModel fl = new FileModel();
        fl.setFilename(file.getOriginalFilename());
        fl.setCreated(new Date());
        fl.setPath("");
        fl = fileRepository.save(fl);
        return CommonTransform.transformBean(fl, FileDto.class);
    }

    @RequestMapping(value = "/files/{fileName:.+}", method = RequestMethod.GET, produces = "application/pdf")
    public ResponseEntity<InputStreamResource> downloadPDFFile(@PathVariable("fileName") String filename)throws IOException {
        return ResponseEntity
                .ok()
                .contentLength(new FileInputStream("upload-dir/"+filename).available())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(new FileInputStream(filename)));
    }

    @RequestMapping(value = "/getFile/{id}", method = RequestMethod.GET, produces = "application/pdf")
    public ResponseEntity<InputStreamResource> downloadPDFFile(@PathVariable Long id) throws IOException {
        FileModel file = fileRepository.findOne(id);
        return ResponseEntity
                .ok()
                .contentLength(new FileInputStream("upload-dir/"+file.getFullPath()).available())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(new FileInputStream("upload-dir/"+file.getFullPath())));
    }
}
