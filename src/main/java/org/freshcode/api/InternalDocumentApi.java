package org.freshcode.api;

import org.freshcode.domain.FileModel;
import org.freshcode.domain.InternalDocument;
import org.freshcode.domain.enums.EntityType;
import org.freshcode.domain.enums.FileType;
import org.freshcode.dto.FileDto;
import org.freshcode.dto.InternalDocumentDto;
import org.freshcode.dto.UserDto;
import org.freshcode.repository.DocumentRepository;
import org.freshcode.repository.FileRepository;
import org.freshcode.repository.PersonRepository;
import org.freshcode.sevice.GoogleService;
import org.freshcode.templates.internal_documents.InternalBaseTemplate;
import org.freshcode.templates.internal_documents.InternalInvoiceTemplate;
import org.freshcode.utils.CommonTransform;
import org.freshcode.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by user on 04.07.16.
 */
@RestController("idocs")
@Transactional
public class InternalDocumentApi {

    @Autowired
    DocumentRepository documentRepository;
    @Autowired
    PersonRepository personRepository;
    @Autowired
    FileRepository fileRepository;
    @Autowired
    GoogleService googleService;

    @RequestMapping(path = "/internalDocument/{id}", method = RequestMethod.GET)
    public InternalDocumentDto getInternalDocumentById(@PathVariable Long id)
    {
        InternalDocument document = documentRepository.findOne(id);
        return CommonTransform.transformBean(document,InternalDocumentDto.class).cutOuter();
    }

    @RequestMapping(path = "/internalDocument/{type}/{id}", method = RequestMethod.GET)
    public List<InternalDocumentDto> getInternalDocumentsByTypeAndUserId(String type,Long id)
    {
        documentRepository.findByPersonIdAndType(id, type);
        return processDocList(documentRepository.findByPersonIdAndType(id, type));
    }

    @RequestMapping(path = "/internalDocuments/{id}", method = RequestMethod.GET)
    public List<InternalDocumentDto> getInternalDocumentsByUserId(@PathVariable Long id)
    {
        List<InternalDocument> documents = documentRepository.findByPersonId(id).stream()
                .filter(i -> i.getParent()==null).collect(Collectors.toList());
        return processDocList(documents);
    }

    @RequestMapping(path = "/internalDocument", method = RequestMethod.POST)
    public InternalDocumentDto createInternalDocuments(@RequestBody InternalDocumentDto dto)
    {
        Assert.isNull(dto.getId());
        Assert.notNull(dto.getPerson().getId());

        switch (dto.getType()) {
            case SERVICE_AGREEMENT:
            case RENT_SPACE:
            case RENT_EQUIPMENT:
            case STUDY_AGREEMENT:
                dto.setParent(null);
                break;
            case INVOICE:
            case SERVICE_AGREEMENT_ACCEPTANCE:
                dto.setChildren(null);
                break;
        }
        switch (dto.getType()) {
            case INVOICE:
                dto.setFromDate(dto.getDate());
                dto.setToDate(dto.getDate());
                break;
            case RENT_SPACE:
                break;
            case RENT_EQUIPMENT:
                break;
            case STUDY_AGREEMENT:
                break;
            case SERVICE_AGREEMENT:
                dto.setFromDate(dto.getDate());
                break;
            case SERVICE_AGREEMENT_ACCEPTANCE:
                break;
        }



        InternalDocument result = documentRepository.save(CommonTransform.transformBean(dto,InternalDocument.class));

        if(dto.getParentDocForAcceptance()!=null)
        {
            InternalDocument parent = documentRepository.findOne(dto.getParentDocForAcceptance());
            parent.setAcceptance(result);
            documentRepository.save(parent);
        }
        else if(dto.getParent()!=null && dto.getParent().getId()!=null)
        {
            InternalDocument parent = documentRepository.findOne(dto.getParent().getId());
            Utils.alw(parent.getChildren(), result);
            documentRepository.save(parent);
        }
        return getInternalDocumentById(result.getId());
    }

    @RequestMapping(path = "/internalDocument", method = RequestMethod.PATCH)
    public InternalDocumentDto updateInternalDocuments(@RequestBody InternalDocumentDto dto)
    {
        //you can update Name, Price, dates, State
        Assert.notNull(dto.getId());
        InternalDocument document = documentRepository.findOne(dto.getId());
        dto.copy(document);
        return CommonTransform.transformBean(document, InternalDocumentDto.class).cutOuter();
    }


    @RequestMapping(path = "/internalDocument/render/{id}", method = RequestMethod.DELETE)
    public InternalDocumentDto deleteRender(@PathVariable Long id)
    {
        InternalDocument document = documentRepository.findOne(id);
        if(document.getFile()!=null)
        {
            FileModel toDel = fileRepository.findOne(document.getFile().getId());
            new File("upload-dir/" + toDel.getFullPath()).deleteOnExit();
            document.setFile(null);
            fileRepository.delete(toDel);
            documentRepository.save(document);
        }
        return getInternalDocumentById(document.getId());
    }

    @RequestMapping(path = "/internalDocument/render/{id}", method = RequestMethod.GET)
    public FileDto render(@PathVariable Long id)
    {
        InternalDocument document = documentRepository.findOne(id);
        if(document.getFile()!=null){
            return CommonTransform.transformBean(document.getFile(), FileDto.class);
        }
        InternalBaseTemplate template = InternalBaseTemplate.getTemplate(document.getType());
        try {
            FileModel file = template.render(document);
            file.setParentId(document.getPerson().getId());

            file = fileRepository.save(file);
            document.setFile(file);
            documentRepository.save(document);
            return CommonTransform.transformBean(file, FileDto.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new FileDto();
    }

    @RequestMapping(path = "/getDocsForPrint", method = RequestMethod.GET, produces = "application/pdf")
    public ResponseEntity<InputStreamResource> buildPdfForPrint(@RequestParam(value = "idList") String[] idList) throws IOException {
        FileModel file = buildPDF(idList);
//        FileModel file = fileRepository.findOne(id);
//        return ResponseEntity
//                .ok()
//                .contentLength(new FileInputStream("upload-dir/"+file.getFullPath()).available())
//                .contentType(MediaType.parseMediaType("application/octet-stream"))
//                .body(new InputStreamResource(new FileInputStream("upload-dir/"+file.getFullPath())));
        return null;
    }

    @RequestMapping(path = "/printTable", method = RequestMethod.GET)
    public void printTable(@RequestParam(value = "idList") String[] idList) throws IOException {
        List<Long> list = new ArrayList();
        for (String s : idList) {
            list.add(Long.valueOf(s));
        }
        googleService.printTable(list);
    }

    @RequestMapping(path = "/tr", method = RequestMethod.POST)
    public String tr(@RequestBody String string)
    {
        return Utils.tru.transliterate(string);
    }

    private FileModel buildPDF(String[] idList) {
        FileModel model = new FileModel();
        return model;
    }

    private List<InternalDocumentDto> processDocList(List<InternalDocument> documents)
    {
        if(documents.size()>0)
        {
            UserDto user = CommonTransform.transformBean(documents.get(0).getPerson(), UserDto.class);
            List<InternalDocumentDto> result = CommonTransform.transformBeans(documents,InternalDocumentDto.class);
            result.forEach(d->d.setPerson(user));
            result.forEach(InternalDocumentDto::cutOuter);
            return result;
        }
        return new ArrayList<>();
    }
}
