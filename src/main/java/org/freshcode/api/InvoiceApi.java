package org.freshcode.api;

import org.freshcode.domain.*;
import org.freshcode.domain.enums.EntityType;
import org.freshcode.domain.enums.FileType;
import org.freshcode.dto.*;
import org.freshcode.repository.*;
import org.freshcode.sevice.FileService;
import org.freshcode.templates.ITextInvoiceRender;
import org.freshcode.utils.CommonTransform;
import org.freshcode.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@Transactional
@RestController("invoices")
public class InvoiceApi {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    AgreementRepository agreementRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    FileService fileService;

    @RequestMapping(path = "/invoices", method = RequestMethod.GET)
    public List<InvoiceDto> getAllInvoices() {

        List<Agreement> agreements = agreementRepository.findByActiveTrue();
        List<InvoiceDto> results = new ArrayList<>();

        for (Agreement agreement : agreements) {
            AgreementDto dto = CommonTransform.transformBean(agreement, AgreementDto.class);
            List<InvoiceDto> dtoList = CommonTransform.transformBeans(agreement.getInvoices(),InvoiceDto.class);
            dtoList.forEach(i->i.setAgreement(dto));
            dtoList.forEach(InvoiceDto::cutOuter);
            results.addAll(dtoList);
        }
        return results;
    }

    @RequestMapping(path = "/invoices/{id}", method = RequestMethod.GET)
    public List<InvoiceDto> getAllInvoicesByAgreementId(@PathVariable Long id) {
        Agreement agreement = agreementRepository.findOne(id);
        AgreementDto dto = CommonTransform.transformBean(agreement, AgreementDto.class);
        List<InvoiceDto> dtoList = CommonTransform.transformBeans(agreement.getInvoices(),InvoiceDto.class);
        dtoList.forEach(i->i.setAgreement(dto));
        dtoList.forEach(InvoiceDto::cutOuter);
        return dtoList;
    }

    @RequestMapping(path = "/invoice/{id}", method = RequestMethod.GET)
    public InvoiceDto getInvoiceById(@PathVariable("id") Long id) {
        Invoice i = invoiceRepository.findOne(id);
        Agreement agreement = agreementRepository.findOne(i.getAgreement().getId());
        for (Invoice invoice : agreement.getInvoices()) {
            if(i.getId().equals(invoice.getId()))
            {
                InvoiceDto result = CommonTransform.transformBean(i,InvoiceDto.class);
                result.getAgreement().setInvoices(null);
                result.getAgreement().setExpenses(null);
                result.getPayments().forEach(p->p.setInvoice(null));
                return result;
            }
        }
        return null;
    }

    @RequestMapping(path = "invoice", method = RequestMethod.POST)
    public InvoiceDto create(@RequestBody InvoiceDto invoiceDto)
    {
        Assert.isNull(invoiceDto.getId());
        Assert.notNull(invoiceDto.getAgreement());
        Assert.notNull(invoiceDto.getAgreement().getId());
        Agreement agreement = agreementRepository.findOne(invoiceDto.getAgreement().getId());
        invoiceDto.setAgreement(null);
        Invoice invoice = invoiceRepository.save(CommonTransform.transformBean(invoiceDto, Invoice.class));
        invoice.setAgreement(agreement);
        agreement.setInvoices(Utils.alw(agreement.getInvoices(),invoice));
        return getInvoiceById(invoice.getId());
    }

    @RequestMapping(path = "invoice", method = RequestMethod.PATCH)
    public InvoiceDto update(@RequestBody InvoiceDto invoiceDto)
    {
        Assert.notNull(invoiceDto.getId());
        Invoice invoice = invoiceRepository.findOne(invoiceDto.getId());
        invoiceDto.copy(invoice);
        return CommonTransform.transformBean(invoice, InvoiceDto.class).cutOuter();
    }

    @RequestMapping(path = "invoice/render/{id}", method = RequestMethod.GET)
    public FileDto renderInvoice(@PathVariable Long id) throws IOException {
        Invoice invoice = invoiceRepository.findOne(id);
        InvoiceTemplate template = invoice.getAgreement().getInvoiceTemplate();
        ITextInvoiceRender render = (template1, datasource) -> null;
        FileModel model = new FileModel();
        model.setParentId(id);
        model.setParentType(EntityType.INVOICE);
        model.setFilename(invoice.getNumber());
        model.setFileType(FileType.PDF);
        model = fileService.saveFile(render.render(template,invoice),model);
        return CommonTransform.transformBean(model, FileDto.class);
    }

}
