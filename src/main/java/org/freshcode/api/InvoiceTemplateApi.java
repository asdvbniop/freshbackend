package org.freshcode.api;

import org.freshcode.domain.InvoiceTemplate;
import org.freshcode.dto.InvoiceTemplateDto;
import org.freshcode.repository.InvoiceTemplateRepository;
import org.freshcode.utils.CommonTransform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * Created by user on 26.08.16.
 */
@RestController("invoiceTemplate")
@Transactional
public class InvoiceTemplateApi {
    @Autowired
    InvoiceTemplateRepository invoiceTemplateRepository;

    @RequestMapping(path = "invoiceTemplate/{id}", method = RequestMethod.GET)
    public InvoiceTemplateDto getInvoiceTemplate(@PathVariable Long id)
    {
        return CommonTransform.transformBean(invoiceTemplateRepository.findOne(id), InvoiceTemplateDto.class);
    }

    @RequestMapping(path = "invoiceTemplate", method = RequestMethod.PATCH)
    public InvoiceTemplateDto updateInvoiceTemplate(@RequestBody InvoiceTemplateDto  dto)
    {
        InvoiceTemplate oldTemplate = invoiceTemplateRepository.findOne(dto.getId());
        CommonTransform.copyBean(dto, oldTemplate);
        invoiceTemplateRepository.save(oldTemplate);
        return getInvoiceTemplate(dto.getId());
    }
}
