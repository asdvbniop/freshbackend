package org.freshcode.api;

import org.freshcode.domain.Bid;
import org.freshcode.domain.Client;
import org.freshcode.domain.Lead;
import org.freshcode.domain.Project;
import org.freshcode.dto.BidDto;
import org.freshcode.dto.ClientDto;
import org.freshcode.dto.LeadDto;
import org.freshcode.dto.ProjectDto;
import org.freshcode.repository.BidRepository;
import org.freshcode.repository.ClientRepository;
import org.freshcode.repository.LeadRepository;
import org.freshcode.repository.ProjectRepository;
import org.freshcode.utils.CommonTransform;
import org.freshcode.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by user on 04.07.16.
 */
@RestController("leads")
@Transactional
public class LeadApi {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    BidRepository bidRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ProjectRepository projectRepository;

    @RequestMapping(path = "/leads", method = RequestMethod.GET)
    public List<LeadDto>getAllLeads()
    {
        List<Lead> leads = leadRepository.findByActiveTrue();
        List<LeadDto> result = CommonTransform.transformBeans(leads, LeadDto.class);
        result.forEach(LeadDto::cutInner);
        return result;
    }

    @RequestMapping(path = "/lead/{id}", method = RequestMethod.GET)
    public LeadDto getLeadById(@PathVariable(value = "id") String id)
    {
        return CommonTransform.transformBean(leadRepository.findOne(Long.valueOf(id)),LeadDto.class).cutOuter();
    }

    @RequestMapping(path = "/lead", method = RequestMethod.POST)
    public LeadDto createLead(@RequestBody LeadDto leadDto)
    {
        Assert.isNull(leadDto.getId());
        BidDto bidDto = leadDto.getBid();
        ClientDto clientDto = leadDto.getClient();
        leadDto.setBid(null);
        leadDto.setClient(null);
        leadDto.setAgreement(null);
        Lead lead = CommonTransform.transformBean(leadDto, Lead.class);
        lead.setActive(true);
        lead = leadRepository.save(lead);

        if(bidDto!=null)
        {
            Bid bid = bidRepository.findOne(bidDto.getId());
            bid.setLead(lead);
            lead.setBid(bid);
        }

        if(clientDto!=null)
        {
            Client client = clientRepository.findOne(clientDto.getId());
            lead.setClient(client);
        }
        lead = leadRepository.save(lead);
        return getLeadById(String.valueOf(lead.getId()));
    }

    @RequestMapping(path = "/lead", method = RequestMethod.PATCH)
    public LeadDto updateLead(@RequestBody LeadDto leadDto)
    {
        Assert.notNull(leadDto.getId());
        Lead lead = leadRepository.findOne(leadDto.getId());
        leadDto.copy(lead);
        leadRepository.save(lead);
        updateClient(lead, leadDto.getClient()==null?null:leadDto.getClient().getId());
        return CommonTransform.transformBean(leadRepository.findOne(lead.getId()),LeadDto.class).cutOuter();
    }

    @RequestMapping(path = "/lead/{id}", method = RequestMethod.DELETE)
    public void deleteLead(@PathVariable(value = "id") String id)
    {
        Lead l = leadRepository.findOne(Long.valueOf(id));
        l.setActive(false);
        leadRepository.save(l);
    }

    private void updateClient(Lead lead, Long newClientId)
    {
        if(newClientId==null)
            return;
        Client newClient = clientRepository.getOne(newClientId);

        Client oldClient = lead.getClient();
        if(oldClient==null)
        {
            lead.setClient(newClient);
            leadRepository.save(lead);
        }
        else if (!oldClient.getId().equals(newClient.getId()))
        {
            lead.setClient(newClient);
            leadRepository.save(lead);
        }
    }
}
