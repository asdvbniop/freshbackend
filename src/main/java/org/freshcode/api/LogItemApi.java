package org.freshcode.api;

import org.freshcode.domain.LogPeriod;
import org.freshcode.domain.Member;
import org.freshcode.dto.LogItemDto;
import org.freshcode.dto.LogPeriodDto;
import org.freshcode.dto.MemberDto;
import org.freshcode.repository.LogItemRepository;
import org.freshcode.repository.LogPeriodRepository;
import org.freshcode.repository.MemberRepository;
import org.freshcode.utils.CommonTransform;
import org.freshcode.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@RestController("logItem")
@Transactional
public class LogItemApi {
    @Autowired
    LogItemRepository logItemRepository;
    @Autowired
    LogPeriodRepository logPeriodRepository;

    @RequestMapping(path = "logItems/{id}", method = RequestMethod.GET)
    public List<LogItemDto> getAllLogItemsByLogPeriod(@PathVariable Long id)
    {
        LogPeriod logPeriod = logPeriodRepository.findOne(id);
        return CommonTransform.transformBeans(logPeriod.getLog(), LogItemDto.class);
    }


    @RequestMapping(path = "/logItem/{id}", method = RequestMethod.GET)
    public LogItemDto getLogItemById(@PathVariable Long id)
    {
        return CommonTransform.transformBean(logItemRepository.findOne(id), LogItemDto.class);
    }

    @RequestMapping(path = "/logItem/", method = RequestMethod.PATCH)
    public LogItemDto updateLogItem(@RequestBody LogItemDto logItemDto)
    {
        return logItemDto;
    }
}
