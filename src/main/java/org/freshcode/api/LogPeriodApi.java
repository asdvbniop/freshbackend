package org.freshcode.api;

import org.filters.DtoFilters;
import org.filters.DtoSingleFilter;
import org.freshcode.domain.Agreement;
import org.freshcode.domain.LogPeriod;
import org.freshcode.domain.Member;
import org.freshcode.dto.LogPeriodDto;
import org.freshcode.dto.MemberDto;
import org.freshcode.gui.utils.Etype;
import org.freshcode.repository.*;
import org.filters.CommonFilter;
import org.freshcode.utils.CommonTransform;
import org.freshcode.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@RestController("logPeriod")
@Transactional
public class LogPeriodApi {
    @Autowired
    MemberRepository memberRepository;
    @Autowired
    LogPeriodRepository logPeriodRepository;
    @Autowired
    LogItemRepository logItemRepository;
    @Autowired
    AgreementRepository agreementRepository;
    @Autowired
    ProjectRepository projectRepository;

    @RequestMapping(path = "/logPeriods/{id}", method = RequestMethod.GET)
    public List<LogPeriodDto> getAllLogPeriodsByMemberId(@PathVariable Long id, @RequestBody(required = false) List<CommonFilter> filters)
    {
        Member member = memberRepository.findOne(id);
        List<LogPeriodDto> result = CommonTransform.transformBeans(member.getLog(), LogPeriodDto.class);
        MemberDto dto = CommonTransform.transformBean(member, MemberDto.class).cutInner();
        result.forEach(log -> log.setMember(dto));

        if(filters!=null)
        {
            DtoSingleFilter<LogPeriodDto, LogPeriodDto.Filters> dtoFilter = new DtoFilters().getFilters().get(Etype.LOG_PERIOD);
            for (CommonFilter filter : filters) {
                result.removeIf(periodDto -> dtoFilter.match(periodDto, filter));
            }
        }
        return result;
    }

    @RequestMapping(path = "/logPeriodsByAgreement/{id}", method = RequestMethod.GET)
    public List<LogPeriodDto> getAllLogPeriodsByAgreementId(@PathVariable Long id, @RequestBody(required = false) List<CommonFilter> filters)
    {
        Agreement agreement = agreementRepository.findOne(id);
        List<LogPeriodDto> periods = new ArrayList<>();
        List<LogPeriodDto> result = new ArrayList<>();
        for (Member member : agreement.getMembers())
        {
            MemberDto memberDto = CommonTransform.transformBean(member, MemberDto.class).cutInner();
            periods.clear();
            periods.addAll(CommonTransform.transformBeans(member.getLog(), LogPeriodDto.class));
            periods.forEach(periodDto -> periodDto.setMember(memberDto));
            result.addAll(periods);
        }
        return result;
    }

    @RequestMapping(path = "/logPeriodsByProject/{id}", method = RequestMethod.GET)
    public List<LogPeriodDto> getAllLogPeriodsByProjectId(@PathVariable Long id, @RequestBody(required = false) List<CommonFilter> filters)
    {
        return new ArrayList<>();
    }

    @RequestMapping(path = "/logPeriod/{id}", method = RequestMethod.GET)
    public LogPeriodDto getLogPeriodById(@PathVariable(value = "id") Long id)
    {
        LogPeriod logPeriod = logPeriodRepository.findOne(id);
        return CommonTransform.transformBean(logPeriod, LogPeriodDto.class).cutOuter();
    }

    @RequestMapping(path = "/logPeriod", method = RequestMethod.POST)
    public LogPeriodDto createLogPeriod(@RequestBody LogPeriodDto periodDto)
    {
        Assert.notNull(periodDto.getMember());
        Assert.notNull(periodDto.getMember().getId());
        Member member = memberRepository.findOne(periodDto.getMember().getId());
        if(member.getLog()!=null && member.getLog().size()>0)
        {
            LogPeriod last = member.getLog().get(member.getLog().size()-1);
            Assert.isTrue(periodDto.getFromDate().after(last.getToDate()));
        }
        periodDto.setMember(null);
        LogPeriod logPeriod = logPeriodRepository.save(CommonTransform.transformBean(periodDto, LogPeriod.class));
        member.setLog(Utils.alw(member.getLog(), logPeriod));
        logPeriod.setMember(member);
        return getLogPeriodById(logPeriod.getId());
    }

    @RequestMapping(path = "/logPeriod", method = RequestMethod.PATCH)
    public LogPeriodDto updateLogPeriod(@RequestBody LogPeriodDto periodDto)
    {
        LogPeriod old = logPeriodRepository.findOne(periodDto.getId());
        periodDto.copy(old);
        logPeriodRepository.save(old);
        return periodDto;
    }
}
