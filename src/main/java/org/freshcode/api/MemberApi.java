package org.freshcode.api;

import org.freshcode.domain.Agreement;
import org.freshcode.domain.Member;
import org.freshcode.domain.Project;
import org.freshcode.dto.MemberDto;
import org.freshcode.repository.AgreementRepository;
import org.freshcode.repository.MemberRepository;
import org.freshcode.repository.ProjectRepository;
import org.freshcode.utils.CommonTransform;
import org.freshcode.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@Transactional
@RestController("members")
public class MemberApi {
    @Autowired
    MemberRepository memberRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    AgreementRepository agreementRepository;

    @RequestMapping(path = "/member/{id}", method = RequestMethod.GET)
    public MemberDto getMemberById(@PathVariable(value = "id") Long id) {
        return CommonTransform.transformBean(memberRepository.findOne(id), MemberDto.class);
    }

    @RequestMapping(path = "/member", method = RequestMethod.POST)
    public MemberDto newMember(@RequestBody MemberDto memberDto) {
        Member member = CommonTransform.transformBean(memberDto, Member.class);
        member = memberRepository.save(member);
        MemberDto result = CommonTransform.transformBean(member, MemberDto.class);

        if(memberDto.getProject()!=null)
        {
            Project project = projectRepository.findOne(memberDto.getProject().getId());
            project.setMembers(Utils.alw(project.getMembers(),member));
            projectRepository.save(project);
            return result;
        }

        if(memberDto.getAgreement()!=null)
        {
            Agreement agreement = agreementRepository.findOne(memberDto.getAgreement().getId());
            agreement.setMembers(Utils.alw(agreement.getMembers(), member));
            agreementRepository.save(agreement);
        }
        return result;
    }

    @RequestMapping(path = "/member", method = RequestMethod.PATCH)
    public MemberDto updateMember(@RequestBody MemberDto dto) throws Exception {
        Member oldMember = memberRepository.findOne(dto.getId());
        dto.copy(oldMember);
        memberRepository.save(oldMember);
        return CommonTransform.transformBean(oldMember, MemberDto.class);
    }

    @RequestMapping(path = "/member/{id}", method = RequestMethod.DELETE)
    public void deleteMember(@PathVariable Long id) throws Exception {
        Member member = memberRepository.findOne(id);
        member.setActive(false);
        memberRepository.save(member);
    }
}
