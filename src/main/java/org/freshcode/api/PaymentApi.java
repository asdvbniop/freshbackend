package org.freshcode.api;

import com.google.common.base.Preconditions;
import org.freshcode.domain.*;
import org.freshcode.dto.*;
import org.freshcode.repository.*;
import org.freshcode.utils.CommonTransform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by user on 04.07.16.
 */
@RestController("payments")
@Transactional
public class PaymentApi {

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    AgreementRepository agreementRepository;

    @RequestMapping(path = "/payments", method = RequestMethod.GET)
    public List<PaymentDto> getAllPayments()
    {
        List<Agreement> agreements = agreementRepository.findByActiveTrue();
        List<PaymentDto> results = new ArrayList<>();

        for (Agreement agreement : agreements) {
            AgreementDto dtoa = CommonTransform.transformBean(agreement, AgreementDto.class).cutInner();
            dtoa.setInvoices(null);
            dtoa.setExpenses(null);
            for (Invoice invoice : agreement.getInvoices()) {
                InvoiceDto dto = CommonTransform.transformBean(invoice, InvoiceDto.class).cutInner();
                dto.setAgreement(dtoa);
                List<PaymentDto> dtoList = CommonTransform.transformBeans(invoice.getPayments(), PaymentDto.class);
                dtoList.forEach(paymentDto -> paymentDto.setInvoice(dto));
                dtoList.forEach(PaymentDto::cutOuter);
                results.addAll(dtoList);
            }
        }
        return results;
    }

    @RequestMapping(path = "/payment/{id}", method = RequestMethod.GET)
    public PaymentDto getPaymentById(@PathVariable(value = "id") String id)
    {
        PaymentDto result;
        result = CommonTransform.transformBean(paymentRepository.findOne(Long.valueOf(id)), PaymentDto.class);
        return result.cut();
    }

    @RequestMapping(path = "/payment", method = RequestMethod.POST)
    public PaymentDto create(@RequestBody PaymentDto paymentDto)
    {
        Assert.notNull(paymentDto.getInvoice());
        Assert.notNull(paymentDto.getInvoice().getId());
        Assert.isNull(paymentDto.getId());
        Long invoiceId = paymentDto.getInvoice().getId();
        paymentDto.setInvoice(null);
        Payment p = CommonTransform.transformBean(paymentDto, Payment.class);
        Invoice invoice = invoiceRepository.findOne(invoiceId);
        p.setActive(true);
        p.setInvoice(invoice);
        p = paymentRepository.save(p);
        invoice.getPayments().add(p);
        return CommonTransform.transformBean(p, PaymentDto.class).cut();
    }
}
