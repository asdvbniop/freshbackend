package org.freshcode.api;

import org.freshcode.domain.*;
import org.freshcode.dto.PersonFullDataDto;
import org.freshcode.dto.UserDto;
import org.freshcode.repository.AddrRepository;
import org.freshcode.repository.PersonRepository;
import org.freshcode.repository.ProjectRepository;
import org.freshcode.repository.UserDataRepository;
import org.freshcode.utils.CommonTransform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by user on 04.07.16.
 */
@RestController("users")
public class PersonApi {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ProjectApi projectApi;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    UserDataRepository userDataRepository;

    @Autowired
    AddrRepository addrRepository;

    @RequestMapping(path = "/users", method = RequestMethod.GET)
    public List<UserDto> getAllUsers() {
        return CommonTransform.transformBeans(personRepository.findByActiveTrue(), UserDto.class)
                .stream().map(UserDto::cutOuter).collect(Collectors.toList());
    }

    @RequestMapping(path = "/user/{id}", method = RequestMethod.GET)
    public UserDto getUserById(@PathVariable("id") String id) {
        return CommonTransform.transformBean(personRepository.findOne(Long.valueOf(id)),UserDto.class).cutOuter();
    }


    @RequestMapping(path = "/user", method = RequestMethod.POST)
    public UserDto newUser(@RequestBody UserDto user) {
        user.setId(null);
        user.setMemberList(null);
        Person person = CommonTransform.transformBean(user, Person.class);
        person.setActive(true);
        return CommonTransform.transformBean(personRepository.save(person), UserDto.class);
    }

    @RequestMapping(path = "/user", method = RequestMethod.PATCH)
    public UserDto updateUser(@RequestBody UserDto person) throws Exception {
        Assert.notNull(person.getId());
        updateUserData(person.getPersonData());
        person.cut();
        Person oldPerson = personRepository.findOne(person.getId());
        person.copyValues(oldPerson);
        personRepository.save(oldPerson);
        return CommonTransform.transformBean(personRepository.findOne(person.getId()), UserDto.class);
    }

    @RequestMapping(path = "/user/{id}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable(value =  "id") String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("User id is null");
        Person p = personRepository.findOne(Long.valueOf(id));
        p.setActive(false);
        personRepository.save(p);
    }

    @RequestMapping(path = "/userData/{id}", method = RequestMethod.GET)
    public PersonFullDataDto getUserDataById(@PathVariable Long id)
    {
        return CommonTransform.transformBean(userDataRepository.findOne(id), PersonFullDataDto.class).cutOuter();
    }

    @RequestMapping(path = "/userData", method = RequestMethod.PATCH)
    public PersonFullDataDto updateUserData(@RequestBody PersonFullDataDto userData)
    {
        Assert.notNull(userData.getId());
        PersonFullData data = userDataRepository.findOne(userData.getId());
        CommonTransform.copyBean(userData,data);
        userDataRepository.save(data);
        return getUserDataById(userData.getId());
    }

    @RequestMapping(path = "/userData", method = RequestMethod.POST)
    public PersonFullDataDto createUserData(@RequestBody PersonFullDataDto dataDto)
    {
        FullAddressData eng=null, rus=null, ua=null;
        Assert.notNull(dataDto.getUser());
        Person user = personRepository.findOne(dataDto.getUser().getId());
        Assert.notNull(user);
        dataDto.setUser(null);

        if(dataDto.getEngAddr()!=null)
            eng = addrRepository.save(CommonTransform.transformBean(dataDto.getEngAddr(), FullAddressData.class));
        if(dataDto.getRusAddr()!=null)
            rus = addrRepository.save(CommonTransform.transformBean(dataDto.getRusAddr(), FullAddressData.class));
        if(dataDto.getUaAddr()!=null)
            ua=addrRepository.save(CommonTransform.transformBean(dataDto.getUaAddr(), FullAddressData.class));

        dataDto.setEngAddr(null);
        dataDto.setRusAddr(null);
        dataDto.setUaAddr(null);
        PersonFullData data = userDataRepository.save(CommonTransform.transformBean(dataDto, PersonFullData.class));

        if(eng!=null)
            data.setEngAddr(eng);
        if(rus!=null)
            data.setRusAddr(rus);
        if(ua!=null)
            data.setUaAddr(ua);

        user.setPersonData(data);
        personRepository.save(user);
        return getUserDataById(data.getId());
    }

/////////////////////////////////////////////////////////////////////////
//
//    @RequestMapping(path = "/payments", method = RequestMethod.PUT)
//    public void addPayment(@RequestBody Payment payment) {
//
//    }
//
//    @RequestMapping(path = "/payments", method = RequestMethod.DELETE)
//    public void deletePayment() {
//
//    }
//
//    @RequestMapping(path = "/payments", method = RequestMethod.PATCH)
//    public void editPayment(@RequestBody Payment payment) {
//
//    }
//
//    //////////////////////////////////////////////////////////////////
//
//    @RequestMapping(path = "/futureRaise", method = RequestMethod.PUT)
//    public void addFutureRaise(@RequestBody Payment payment) {
//
//    }
//
//    @RequestMapping(path = "/futureRaise", method = RequestMethod.PATCH)
//    public void editFutureRaise(@RequestBody Payment payment) {
//
//    }
//
//    @RequestMapping(path = "/futureRaise", method = RequestMethod.DELETE)
//    public void deleteFutureRaise() {
//
//    }
//
//    //////////////////////////////////////////////////////////////////
//
//    @RequestMapping(path = "/plannedRaise", method = RequestMethod.PUT)
//    public void addPlannedRaise(@RequestBody Payment payment) {
//
//    }
//
//    @RequestMapping(path = "/plannedRaise", method = RequestMethod.PATCH)
//    public void editPlannedRaise(@RequestBody Payment payment) {
//
//    }
//
//    @RequestMapping(path = "/plannedRaise", method = RequestMethod.DELETE)
//    public void deletePlannedRaise() {
//
//    }
//
//    //////////////////////////////////////////////////////////////////
//
//    @RequestMapping(path = "/overtime", method = RequestMethod.PUT)
//    public void addOvertime(@RequestBody Overtime overtime) {
//
//    }
//
//    @RequestMapping(path = "/overtime", method = RequestMethod.PATCH)
//    public void editOvertime(@RequestBody Overtime overtime) {
//
//    }
//
//    @RequestMapping(path = "/overtime", method = RequestMethod.DELETE)
//    public void deleteOvertime() {
//
//    }
}
