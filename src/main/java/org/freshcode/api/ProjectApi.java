package org.freshcode.api;

import org.freshcode.domain.*;
import org.freshcode.domain.enums.EntityType;
import org.freshcode.dto.AbstractDto;
import org.freshcode.dto.AgreementDto;
import org.freshcode.dto.LeadDto;
import org.freshcode.dto.ProjectDto;
import org.freshcode.repository.AgreementRepository;
import org.freshcode.repository.LeadRepository;
import org.freshcode.repository.PersonRepository;
import org.freshcode.repository.ProjectRepository;
import org.freshcode.utils.CommonTransform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@Transactional
@RestController("projects")
public class ProjectApi {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    AgreementRepository agreementRepository;

    ////////Project CRUD
    @RequestMapping(path = "/projects", method = RequestMethod.GET)
    public List<ProjectDto> getAllProjects() {
        List<ProjectDto> results = CommonTransform.transformBeans(projectRepository.findByActiveTrue(), ProjectDto.class);
        results.forEach(ProjectDto::cutInner);
        return results;
    }

    @RequestMapping(path = "/project/{id}", method = RequestMethod.GET)
    public ProjectDto getProjectById(@PathVariable(value = "id") String id) {
        return CommonTransform.transformBean(
                projectRepository.findOne(Long.valueOf(id)), ProjectDto.class).cutOuter();
    }

    @RequestMapping(path = "/project", method = RequestMethod.POST)
    public ProjectDto createProject(@RequestBody ProjectDto projectDto) {
        Assert.isNull(projectDto.getId());
        AgreementDto agreementDto = projectDto.getAgreement();
        projectDto.setAgreement(null);
        Project project = CommonTransform.transformBean(projectDto, Project.class);
        project.setActive(true);
        project = projectRepository.save(project);
        if(agreementDto!=null)
        {
            Agreement agreement = agreementRepository.findOne(agreementDto.getId());
            project.setAgreement(agreement);
            agreement.setProject(project);
        }
        return CommonTransform.transformBean(
                projectRepository.findOne(project.getId()), ProjectDto.class).cutInner();
    }

//    @RequestMapping(path = "/projectDto", method = RequestMethod.PATCH)
//    public Project updateProject(@RequestBody Project projectDto) throws Exception {
//    }
//
//    @RequestMapping(path = "/projectDto", method = RequestMethod.DELETE)
//    public void deleteProject(@RequestBody Project projectDto) throws Exception {
//    }

//    ////////Project Members CRUD
//    @RequestMapping(path = "/projectMember", method = RequestMethod.POST)
//    public void addMembers(@RequestBody Member member) {
//        projectService.addMember(member);
//    }
//
//    @RequestMapping(path = "/projectMember", method = RequestMethod.PATCH)
//    public void updateMember(@RequestBody Member member) {
//        projectService.updateMember(member);
//    }
//
//    @RequestMapping(path = "/projectMember", method = RequestMethod.DELETE)
//    public void deleteMember(@RequestBody Member member) {
//        projectService.deleteMember(member);
//    }
}
