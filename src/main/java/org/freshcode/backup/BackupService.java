package org.freshcode.backup;

import org.freshcode.domain.*;
import org.freshcode.repository.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 24.04.17.
 */
@Service
public class BackupService {
    @Autowired
    PersonRepository personRepository;

    @Autowired
    FinancialInfoRepository financialInfoRepository;
    @Autowired
    FileRepository fileRepository;
    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    BankRepository bankRepository;
    @Autowired
    AddrRepository addrRepository;
    @Autowired
    UserDataRepository userDataRepository;

    private HashMap<Class, JpaRepository> classToRepository=new HashMap<>();
    private HashMap<Class, List<Long>> knownEntity = new HashMap<>();

    @PostConstruct
    void init()
    {
        classToRepository.put(Person.class, personRepository);
        classToRepository.put(FinancialInfo.class, financialInfoRepository);
        classToRepository.put(FileModel.class, fileRepository);
        classToRepository.put(InternalDocument.class, documentRepository);
        classToRepository.put(Bank.class, bankRepository);
        classToRepository.put(FullAddressData.class, addrRepository);
        classToRepository.put(PersonFullData.class, userDataRepository);

        List<Class> entity = new ArrayList<>();
        entity.add(Person.class);
        entity.add(PersonFullData.class);
        entity.add(FinancialInfo.class);

        entity.forEach(c -> knownEntity.put(c, new ArrayList<>()));

    }

}
