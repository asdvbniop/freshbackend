package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by user on 29.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Absence extends AbstractEntity{

    enum AbsenceReason
    {
        VACATION,
        ILLNESS
    }

    enum AbsenceState
    {
        ACCEPTED,
        REJECTED
    }

    private String name;
    private String description;
    private Date fromDate;
    private Date toDate;
    private AbsenceReason absenceReason;
    private AbsenceState absenceState;
}
