package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.AgreementState;
import org.freshcode.domain.enums.AgreementType;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Agreement extends PriceAble {

    private String name;
    private AgreementState state;

    @OneToOne
    private Lead lead;

    @OneToOne
    private Project project;

    @OneToOne
    private InvoiceTemplate invoiceTemplate;

    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    private List<Member> members = new ArrayList<>();

    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    private List<Invoice> invoices = new ArrayList<>();

    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    private List<Expense> expenses = new ArrayList<>();

    @OneToMany
    private List<HistoryItem> history = new ArrayList<>();

    private AgreementType agreementType;

    public Agreement(String name, Date date, AgreementState state, Lead lead, Price price, AgreementType type)
    {
        setName(name);
        setDate(date);
        setState(state);
        setLead(lead);
        setPrice(price);
        setAgreementType(type);
    }

}
