package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.Language;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Created by user on 15.08.16.
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Bank extends AbstractEntity{
    private String name;
    private String swift;
    private String mfo;
    private String edr;


    @OneToOne
    private FullAddressData rus;
    @OneToOne
    private FullAddressData eng;
    @OneToOne
    private FullAddressData ua;

    public String getAddressAsString(Language l) {
        switch (l) {
            case ENG:
                return getEng()==null?"eng address is null":getEng().asString(l);
            case RUS:
                return getRus()==null?"eng address is null":getEng().asString(l);
        }
            return "!!!error!!!";
    }

    public String getFieldForLanguage(FullAddressData.Fields field, Language language)
    {
        switch (language) {
            case ENG:
                return getEng()!=null?getEng().getField(field):"is null";
            case RUS:
                return getRus()!=null?getRus().getField(field):"is null";
            case UA:
                return getUa()!=null?getUa().getField(field):"is null";
        }
        return "!!!error!!!";
    }
}
