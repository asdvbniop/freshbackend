package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.Currency;

import javax.persistence.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Bid extends PriceAble{

    public enum BidState {
        DRAFT,
        SENT,
        REJECTED,
        ACCEPTED,
        NOT_RESPONDED
    }

    private String url;
    private BidState bidState;
    private String duration;
    private String reason;

    @OneToOne
    private Lead lead;

    public Bid(Price price, String url, Date date, BidState state, String duration, String reason, Lead lead)
    {
        setPrice(price);
        setUrl(url);
        setDate(date);
        setBidState(state);
        setDuration(duration);
        setReason(reason);
        setLead(lead);
    }
}
