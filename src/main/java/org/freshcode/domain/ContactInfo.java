package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.InfoType;

import javax.persistence.Entity;

/**
 * Created by user on 15.08.16.
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class ContactInfo extends AbstractEntity{
    private String name;
    private InfoType type;
    private String value;
    private String role;
}
