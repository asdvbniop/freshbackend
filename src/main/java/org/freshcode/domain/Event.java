package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.xml.transform.sax.SAXTransformerFactory;
import java.util.Date;

/**
 * Created by user on 29.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Event extends AbstractEntity{
    private String name;
    private String description;
    private Date date;
}
