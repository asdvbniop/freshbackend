package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.ExpenseType;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.Date;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Expense extends PriceAble {

    private String comment;
    private ExpenseType type;

    @ManyToOne
    private Person person;
    @ManyToOne
    private Agreement agreement;

    public Expense (Price price, Date date, String comment, ExpenseType type, Person person, Agreement agreement)
    {
        setPrice(price);
        setDate(date);
        setComment(comment);
        setType(type);
        setPerson(person);
        setAgreement(agreement);
    }

}
