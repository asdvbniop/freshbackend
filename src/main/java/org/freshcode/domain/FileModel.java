package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.EntityType;
import org.freshcode.domain.enums.FileType;

import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by user on 28.08.16.
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class FileModel extends AbstractEntity{
    private Long parentId;
    private EntityType parentType;
    private String filename;
    private String path;
    private FileType fileType;
    private Date created;
    private Date updated;
    private Date archived;

    public String getFullPath()
    {
        return path+"/"+filename;
    }
}
