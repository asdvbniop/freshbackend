package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.InfoType;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Created by user on 15.08.16.
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class FinancialInfo extends AbstractEntity{
    private String name;
    private String account;
    private Currency currency;

    @OneToOne
    private Bank bank;
}
