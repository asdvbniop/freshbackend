package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.Language;
import org.freshcode.domain.enums.UserRole;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class FullAddressData extends AbstractEntity {
    public enum Fields{
        NAME,INDEX,COUNTRY,CITY,DISTRICT,STREET,FLAT}

    private String name;

    private String index;
    private String country;
    private String city;
    private String district;
    private String street;
    private String flat;

    public String getField(Fields field)
    {
        switch (field) {
            case NAME:
                return name;
            case INDEX:
                return index;
            case COUNTRY:
                return country;
            case CITY:
                return city;
            case DISTRICT:
                return district;
            case STREET:
                return street;
            case FLAT:
                return flat;
        }
        return "!!!error!!!";
    }

    public String asString(Language lang) {
        switch (lang) {
            case ENG:
                return getEngString();
            case RUS:
                return getRusString();
            case UA:
                return getUaString();
        }
        return "error";
    }

    private String getRusString(){
        if(flat.isEmpty())
        {
            return "ул. "+street+"\n"+
                    "г."+city+", "+index+"\n"+
                    country;
        }
        return "ул. "+street+", кв. "+flat+"\n"+
                "г."+city+", "+index+"\n"+
                country;
    }

    private String getUaString(){
        return "вул. "+street+", кв. "+flat+", м."+city+"\n"+
                district+" область "+index+"\n"+
                country;
    }

    private String getEngString(){
        if(flat.isEmpty())
        {
            return street+"\n"+
                    city+", "+index+"\n"+
                    country;
        }
        return street+", flat "+flat+"\n"+
                city+", "+index+"\n"+
                country;
    }
}
