package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class HistoryItem extends AbstractEntity {

    public enum HistoryAction {ADD, EDIT, REMOVE}

    public enum ItemType {USER, PROJECT, MEMBER, CUSTOMER}

    private HistoryAction historyAction;
    private ItemType itemType;
    private String variable;
    private String value;

}
