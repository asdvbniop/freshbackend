package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.AgreementState;
import org.freshcode.domain.enums.InternalDocumentType;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class InternalDocument extends AbstractEntity {

    private String name;
    private Date date;
    private Date fromDate;
    private Date toDate;
    private Price price;

    private AgreementState state;
    @NotNull
    private InternalDocumentType type;

    private String templateName;

    @OneToOne
    private Person person;

    @OneToOne
    private Person customer;

    @OneToOne
    private FileModel file;

    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    private List<InternalDocument> children = new ArrayList<>();

    @OneToOne
    private
    InternalDocument parent;

    @OneToOne
    private InternalDocument acceptance;
}
