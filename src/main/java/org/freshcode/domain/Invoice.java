package org.freshcode.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Invoice extends PriceAble {

    public enum InvoiceState {
        DRAFT,
        PENDING,
        CLOSED
    }
    @ManyToOne
    private Agreement agreement;
    private String number;
    @JsonFormat(shape=JsonFormat.Shape.NUMBER)
    private InvoiceState invoiceState;
    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    List<Payment> payments = new ArrayList<>();

    @ElementCollection
    List<InvoiceRow> rows = new ArrayList<>();

    @OneToMany
    List<FileModel>files = new ArrayList<>();

    public Invoice(Agreement agreement, String name, Date date, Price price, InvoiceState invoiceState, List<Payment> payments)
    {
        setAgreement(agreement);
        setNumber(name);
        setDate(date);
        setPrice(price);
        setInvoiceState(invoiceState);
        setPayments(payments);
    }
}
