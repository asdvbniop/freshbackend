package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Entity;

/**
 * Created by user on 26.08.16.
 */
@EqualsAndHashCode(callSuper = true)
@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceRow extends AbstractEntity {
    private String item;
    private Integer quantity;
    private Float rate;
    private Float amount;
}
