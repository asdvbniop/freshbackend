package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.InvoiceTemplateOption;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 25.08.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class InvoiceTemplate extends AbstractEntity {

    private Currency currency;

    @ManyToOne
    private Client client;
    @ManyToOne
    private Client ourClient;
    @ManyToOne
    private FinancialInfo ourFinancialInfo;

    boolean showQuantity;
    boolean showRate;
    boolean showAmount;

    @ElementCollection
    @MapKeyEnumerated(EnumType.STRING)
    private Map<InvoiceTemplateOption, String> options = new HashMap<>();
}
