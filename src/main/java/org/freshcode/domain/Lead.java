package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.LeadState;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Lead extends PriceAble {
    private String title;
    private String url;
    private LeadState leadState;

    @OneToOne
    private Agreement agreement;

    @ManyToOne
    private Client client;

    @OneToMany
    private List<FileModel> files = new ArrayList<>();

    @OneToOne
    private Bid bid;

    @OneToMany
    private List<Email> emails = new ArrayList<>();
}
