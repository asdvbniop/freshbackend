package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 21.08.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class LogPeriod extends AbstractEntity {
    public enum LogPeriodType {JIRA, MANUAL, LOG_ITEM}
    private Date fromDate;
    private Date toDate;
    private LogPeriodType periodType;
    private Integer minutes;

    @ManyToOne
    private Member member;


    @OneToMany
    List<LogItem> log = new ArrayList<>();
}
