package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.AgreementType;
import org.freshcode.domain.enums.UserRole;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Member extends PriceAble {

    @OneToOne
    private Person person;
    private int ratio;
    private UserRole role;
    private AgreementType type;

    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    List<LogPeriod> log = new ArrayList<>();

    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    List<Overtime> overtimes = new ArrayList<>();

    public Member(Person person, Integer ratio, UserRole role, Price price, AgreementType type)
    {
        setPerson(person);
        setRatio(ratio);
        setRole(role);
        setPrice(price);
        setType(type);
    }
}
