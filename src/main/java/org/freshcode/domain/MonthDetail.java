package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.time.Month;

/**
 * Created by user on 26.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class MonthDetail extends AbstractEntity {

    public float exchangeRate;
    public int workingHours;

    private Month month;
    private Integer year;
}
