package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.PaymentState;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Payment extends PriceAble {
    @ManyToOne
    private Invoice invoice;
    private PaymentState state;

    public Payment(Date date, Price price, Invoice invoice, PaymentState paymentState)
    {
        setDate(date);
        setPrice(price);
        setInvoice(invoice);
        setState(paymentState);
    }
}
