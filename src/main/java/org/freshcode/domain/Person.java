package org.freshcode.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.UserRole;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Person extends AbstractEntity {

    private String username;
    private String jiraId;
    private String name;
    @Embedded
    private Price salary;
    private UserRole userRole;

    @OneToOne
    PersonFullData personData;


    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    List<Expense> payments = new ArrayList<>();

    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    List<Expense> futureRise = new ArrayList<>();

    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    List<Expense> plannedRise = new ArrayList<>();
}
