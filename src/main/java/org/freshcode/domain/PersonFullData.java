package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.Language;
import org.freshcode.domain.enums.UserRole;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class PersonFullData extends AbstractEntity {
    private String fullNameEng;
    private String fullNameRus;
    private String fullNameUa;
    private Date birthday;
    private String phone;
    private String taxNumber;
    private String password;

    @OneToOne
    private FinancialInfo usdAcc;
    @OneToOne
    private FinancialInfo eurAcc;
    @OneToOne
    private FinancialInfo uahAcc;

    @OneToOne
    private FullAddressData engAddr;
    @OneToOne
    private FullAddressData rusAddr;
    @OneToOne
    private FullAddressData uaAddr;


    public String getFullName(Language l) {
        switch (l) {
            case ENG:
                return fullNameEng;
            case RUS:
                return fullNameRus;
            case UA:
                return fullNameUa;
        }
        return "!!!error!!!";
    }

    public String getAddr(Language language)
    {
        switch (language) {
            case ENG:
                return getEngAddr().asString(Language.ENG);
            case RUS:
                return getRusAddr().asString(Language.RUS);
            case UA:
                return getUaAddr().asString(Language.UA);
        }
        return "!!!error!!!";
    }
}
