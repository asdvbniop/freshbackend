package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.Currency;

import javax.persistence.Embeddable;

/**
 * Created by user on 06.08.16.
 */
@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Price {
    private Integer value;
    private Currency currency;

    public String asString()
    {
        return  value + " " + currency.toString();
    }
}
