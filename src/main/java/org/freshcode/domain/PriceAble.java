package org.freshcode.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by user on 17.08.16.
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public abstract class PriceAble extends  AbstractEntity {

    @Embedded
    private Price price;
    private Date date;
}
