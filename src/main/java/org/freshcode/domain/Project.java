package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Project extends AbstractEntity {
    public enum Color {
        RED,
        YELLOW,
        GREEN
    }

    public enum State
    {
        DRAFT,
        ACTIVE,
        PENDING,
        CLOSED
    }

    public Project(Long id)
    {
        setId(id);
    }

    private String name;
    private Color color;
    private State state;

    @OneToOne
    private Agreement agreement;

    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    List<Member> members = new ArrayList<>();

    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.MERGE})
    List<HistoryItem> history = new ArrayList<>();

}
