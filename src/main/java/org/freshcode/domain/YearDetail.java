package org.freshcode.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.time.Month;
import java.util.List;

/**
 * Created by user on 26.07.16.
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class YearDetail extends AbstractEntity {

    private Integer year;
    @OneToMany
    private List<MonthDetail> monthsDetails;
    @OneToMany
    private List<Holiday> holidays;
}
