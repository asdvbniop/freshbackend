package org.freshcode.domain.enums;

/**
 * Created by user on 16.08.16.
 */
public enum AgreementState {
    DRAFT,
    ACTIVE,
    SUSPENDED,
    FINISHED
}
