package org.freshcode.domain.enums;

/**
 * Created by user on 15.08.16.
 */
public enum AgreementType {
    FIXED,
    PER_HOUR,
    MIXED
}
