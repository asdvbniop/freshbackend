package org.freshcode.domain.enums;

/**
 * Created by user on 06.08.16.
 */
public enum EntityType {
    USER,
    USER_DATA,
    PROJECT,
    BID,
    LEAD,
    MEMBER,
    INVOICE,
    PAYMENT,
    CLIENT,
    EXPENSE,
    OVERTIME,
    AGREEMENT
}
