package org.freshcode.domain.enums;

/**
 * Created by user on 02.08.16.
 */
public enum ExpenseType {
    SINGLE,
    REGULAR,
    INVESTMENT,
    PROJECT,
    SALARY,
    OTHER
}
