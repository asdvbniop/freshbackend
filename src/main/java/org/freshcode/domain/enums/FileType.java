package org.freshcode.domain.enums;

/**
 * Created by user on 28.08.16.
 */
public enum FileType {
    PDF,
    JPG,
    PNG,
    DOC
}
