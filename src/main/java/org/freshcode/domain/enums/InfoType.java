package org.freshcode.domain.enums;

/**
 * Created by user on 01.08.16.
 */
public enum InfoType {
    SKYPE,
    EMAIL,
    SLACK,
    HANGOUT,
    TRELLO
}
