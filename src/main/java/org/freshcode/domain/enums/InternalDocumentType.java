package org.freshcode.domain.enums;

/**
 * Created by user on 15.08.16.
 */
public enum InternalDocumentType {
    INVOICE,
    RENT_SPACE,
    RENT_EQUIPMENT,
    STUDY_AGREEMENT,
    SERVICE_AGREEMENT,
    SERVICE_AGREEMENT_ACCEPTANCE
}
