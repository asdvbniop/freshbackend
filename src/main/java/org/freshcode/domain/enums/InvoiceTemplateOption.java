package org.freshcode.domain.enums;

/**
 * Created by user on 25.08.16.
 */
public enum InvoiceTemplateOption {
    SHOW_DUE_DATE,
    HEADER_BLOCK
}
