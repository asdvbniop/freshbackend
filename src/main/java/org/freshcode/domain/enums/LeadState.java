package org.freshcode.domain.enums;

/**
 * Created by user on 01.08.16.
 */
public enum LeadState {
    DRAFT,
    SENT,
    REJECTED,
    ACCEPTED,
    NOT_RESPONDED
}
