package org.freshcode.domain.enums;

/**
 * Created by user on 07.08.16.
 */
public enum PaymentState {
    DRAFT,
    SENT,
    ACCEPT,
    REJECT,
    REFUNDED
}
