package org.freshcode.domain.enums;

/**
 * Created by user on 04.07.16.
 */
public enum UserRole {
    ROOT,
    DEVELOPER,
    PROJECT_MANAGER,
    DESIGNER,
    CUSTOMER
}
