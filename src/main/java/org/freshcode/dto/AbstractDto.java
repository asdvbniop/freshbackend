package org.freshcode.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by user on 04.08.16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public abstract class AbstractDto<R> {
    private Long id;

    @Deprecated
    public R cut(){return (R) this;}
    @Deprecated
    public R cutAll(){return (R) this;}

    public Object getField(Enum e){return null;}


    /***
     * Set all inner references to null
     * @return
     */
    public R cutInner(){return (R) this;}

    /***
     * Set all N-1 references to null
     * @return
     */
    public R cutOuter(){return (R) this;}

    /**
     * Remove all not active entities
     * @return
     */
    public R prepare(){return (R)this;}


}
