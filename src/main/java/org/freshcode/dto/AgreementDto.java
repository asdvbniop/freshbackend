package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.InvoiceTemplate;
import org.freshcode.domain.enums.AgreementState;
import org.freshcode.domain.enums.AgreementType;
import org.freshcode.domain.enums.Currency;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 04.08.16.
 */
@Getter
@Setter
@EqualsAndHashCode(exclude = {"lead","invoices", "expenses", "history"}, callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class AgreementDto extends PriceAbleDto<AgreementDto> {
    private String name;
    private AgreementState state;
    private LeadDto lead;
    private ProjectDto project;
    private InvoiceTemplateDto invoiceTemplate;
    private List<MemberDto> members = new ArrayList<>();
    private List<InvoiceDto> invoices = new ArrayList<>();
    private List<ExpenseDto> expenses = new ArrayList<>();
    private List<HistoryItemDto> history = new ArrayList<>();
    private AgreementType agreementType;

    public static AgreementDto create()
    {
        AgreementDto dto = new AgreementDto();
        dto.setState(AgreementState.DRAFT);
        dto.setAgreementType(AgreementType.MIXED);
        return dto;
    }


    @Override
    public AgreementDto cut() {
        if(getProject()!=null)
        {
            getProject().setAgreement(null);
        }
        if(getInvoices()!=null)
            getInvoices().forEach(i->i.setAgreement(null));
        if(getExpenses()!=null)
            getExpenses().forEach(e->e.setAgreement(null));
        return this;
    }

    @Override
    public AgreementDto cutInner() {
        setLead(null);
        setProject(null);
        setMembers(null);
        setInvoices(null);
        setHistory(null);
        return this;
    }

    @Override
    public AgreementDto cutOuter() {
        if(lead!=null)
            lead.cutInner();
        if(project!=null)
            project.cutInner();
        if(members!=null)
            members.forEach(MemberDto::cutInner);
        if(invoices!=null)
            invoices.forEach(InvoiceDto::cutInner);
        if(expenses!=null)
            expenses.forEach(ExpenseDto::cutInner);
        return this;
    }

    @JsonIgnore
    public Currency getCurrency() {
        if(agreementType.equals(AgreementType.FIXED) || agreementType.equals(AgreementType.MIXED))
            return getPrice().getCurrency();
        if(agreementType.equals(AgreementType.PER_HOUR) && getMembers()!=null)
            return getMembers().get(0).getPrice().getCurrency();
        return Currency.USD;
    }
}
