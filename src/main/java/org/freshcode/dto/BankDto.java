package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.AbstractEntity;
import org.freshcode.domain.FullAddressData;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Created by user on 15.08.16.
 */
@Getter
@Setter
@EqualsAndHashCode(exclude = {"rus","eng"}, callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class BankDto extends AbstractDto <BankDto>{
    private String name;
    private String swift;
    private String mfo;
    private String edr;

    private FullAddressDataDto rus;
    private FullAddressDataDto eng;
    private FullAddressDataDto ua;
}
