package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.Bid;
import org.freshcode.domain.Lead;
import org.freshcode.domain.Price;
import org.freshcode.domain.PriceAble;
import org.freshcode.domain.enums.Currency;
import org.freshcode.utils.CommonTransform;

import java.util.Date;

/**
 * Created by user on 04.08.16.
 */
//@Data
@Getter
@Setter
@EqualsAndHashCode(exclude = {"lead"}, callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class BidDto extends PriceAbleDto<BidDto> {
    private String url;
    private Bid.BidState bidState;
    private String duration;
    private String reason;
    private LeadDto lead;

    public static BidDto create()
    {
        BidDto result = new BidDto("",Bid.BidState.DRAFT, "", "", null);
        result.setPrice(new PriceDto(0, Currency.USD));
        result.setDate(new Date());
        return result;
    }

    public BidDto cut()
    {
        if(getLead()!=null)
        {
            getLead().setAgreement(null);
            getLead().setBid(null);
        }
        return this;
    }

    public void copy(Bid bid) {
        bid.setUrl(getUrl());
        bid.setDate(getDate());
        bid.setPrice(CommonTransform.transformBean(getPrice(), Price.class));
        bid.setBidState(getBidState());
        bid.setDuration(getDuration());
        bid.setReason(getReason());
    }

    @Override
    public BidDto cutInner() {
        setLead(null);
        return this;
    }

    @Override
    public BidDto cutOuter() {
        if(lead!=null)
            lead.cutOuter();
        return this;
    }
}
