package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.Lead;

import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 04.08.16.
 */
@EqualsAndHashCode(exclude = {"leads", "contacts", "bankAccounts"},callSuper = true)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ClientDto extends AbstractDto<ClientDto>{
    private String name;
    private String address;

    private LeadDto lead;
    private List<ContactInfoDto> contacts = new ArrayList<>();
    private List<FinancialInfoDto> bankAccounts = new ArrayList<>();

    @Override
    public ClientDto cutInner() {
        setLead(null);
        setContacts(null);
        setBankAccounts(null);
        return this;
    }
}
