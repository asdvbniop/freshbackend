package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.enums.InfoType;

/**
 * Created by user on 24.08.16.
 */
@EqualsAndHashCode(exclude = "client", callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ContactInfoDto extends AbstractDto<ContactInfoDto> {
    private String name;
    private InfoType type;
    private String value;
    private String role;
    private ClientDto client;
}
