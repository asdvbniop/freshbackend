package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.freshcode.domain.Person;
import org.freshcode.domain.Price;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.ExpenseType;

import java.util.Date;

/**
 * Created by user on 04.08.16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ExpenseDto extends PriceAbleDto<ExpenseDto>{
    private String comment;
    private ExpenseType type;
    private AgreementDto agreement;
    private UserDto person;

    public static ExpenseDto create() {
        return new ExpenseDto("", ExpenseType.OTHER, null, null);
    }

    @JsonIgnore
    public String getPriceString(){return getPrice().format();}

    @Override
    public ExpenseDto cut() {
        if(getAgreement()!=null){
            getAgreement().cut();
        }
        if(getPerson()!=null){
            getPerson().cut();
        }
        return this;
    }

    @Override
    public ExpenseDto cutInner() {
        setAgreement(null);
        setPerson(null);
        return this;
    }

    @Override
    public ExpenseDto cutOuter() {
        if(agreement!=null)
            agreement.cutInner();
        if(person!=null)
            person.cutInner();
        return this;
    }
}
