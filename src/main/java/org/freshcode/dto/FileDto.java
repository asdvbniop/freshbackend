package org.freshcode.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.EntityType;
import org.freshcode.domain.enums.FileType;

import java.util.Date;

/**
 * Created by user on 28.08.16.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class FileDto extends AbstractDto<FileDto> {
    public enum Filters{PARENT_ID,PARENT_TYPE,FILENAME,PATH,FILE_TYPE,CREATED,UPDATED,ARCHIVED }
    private Long parentId;
    private EntityType parentType;
    private String filename;
    private String path;
    private FileType fileType;
    private Date created;
    private Date updated;
    private Date archived;
}
