package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.Bank;
import org.freshcode.domain.FinancialInfo;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.EntityType;

import javax.persistence.OneToOne;

/**
 * Created by user on 24.08.16.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class FinancialInfoDto extends AbstractDto<FinancialInfo>{
    private String name;
    private String account;
    private Currency currency;
    private BankDto bank;

    private Long parentId;
    private EntityType parentType;
}
