package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.AbstractEntity;

import javax.persistence.Entity;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class FullAddressDataDto extends AbstractDto<FullAddressDataDto> {
    public String name = "";
    public String index = "";
    public String country = "";
    public String city = "";
    public String district = "";
    public String street = "";
    public String flat = "";
}

