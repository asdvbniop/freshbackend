package org.freshcode.dto;

import org.freshcode.domain.HistoryItem;

/**
 * Created by user on 15.08.16.
 */
public class HistoryItemDto extends AbstractDto{
    private HistoryItem.HistoryAction historyAction;
    private HistoryItem.ItemType itemType;
    private String variable;
    private String value;
}
