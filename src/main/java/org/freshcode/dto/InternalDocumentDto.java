package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.InternalDocument;
import org.freshcode.domain.Price;
import org.freshcode.domain.enums.AgreementState;
import org.freshcode.domain.enums.InternalDocumentType;
import org.freshcode.utils.CommonTransform;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 04.07.16.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class InternalDocumentDto extends AbstractDto<InternalDocumentDto> {
    private String name;
    private Date date;
    private Date fromDate;
    private Date toDate;
    private PriceDto price;
    private AgreementState state;
    private InternalDocumentType type;
    private String templateName;
    private UserDto person;
    private UserDto customer;
    private FileDto file;
    private List<InternalDocumentDto> children = new ArrayList<>();
    private InternalDocumentDto parent;
    private InternalDocumentDto acceptance;

    private Long parentDocForAcceptance;

    @Override
    public InternalDocumentDto cutInner() {
        setChildren(null);
        setParent(null);
        setAcceptance(null);
        setPerson(null);
        return this;
    }

    @Override
    public InternalDocumentDto cutOuter() {
        if(children!=null)
            children.forEach(InternalDocumentDto::cutInner);
        if(acceptance!=null)
            acceptance.cutInner();
        if(parent!=null)
            parent.cutInner();
        return this;
    }

    public void copy(InternalDocument document) {
        document.setName(getName());
        document.setDate(getDate());
        document.setFromDate(getFromDate());
        document.setToDate(getToDate());
        document.setState(getState());
        document.setPrice(CommonTransform.transformBean(getPrice(), Price.class));
    }
}
