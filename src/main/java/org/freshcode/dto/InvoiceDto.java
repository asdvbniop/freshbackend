package org.freshcode.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.freshcode.domain.FileModel;
import org.freshcode.domain.Invoice;
import org.freshcode.domain.InvoiceRow;
import org.freshcode.domain.Price;
import org.freshcode.domain.enums.EntityType;
import org.freshcode.utils.CommonTransform;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 04.08.16.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"agreement","payments"}, callSuper = true)
@ToString
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class InvoiceDto extends PriceAbleDto<InvoiceDto>{
    public enum Filters{AGREEMENT,FROM_DATE,TO_DATE,PRICE,STATE}
    private AgreementDto agreement;
    private String number;
    @JsonFormat(shape=JsonFormat.Shape.NUMBER)
    private Invoice.InvoiceState invoiceState;
    private List<PaymentDto> payments;
    private List<InvoiceRowDto> rows = new ArrayList<>();
    private List<FileModel>files = new ArrayList<>();


    @JsonIgnore
    public String getDateAsString()
    {
        return getDate().toString();
    }

    @JsonIgnore
    public String getPriceString(){return getPrice().format();}

    @JsonIgnore
    public String getAgreementName(){
        return agreement.getName();
    }


    @Override
    public InvoiceDto cutInner() {
        setAgreement(null);
        setPayments(null);
        return this;
    }

    @Override
    public InvoiceDto cutOuter() {
        if(agreement!=null)
            agreement.cutInner();
        if(payments!=null)
            payments.forEach(PaymentDto::cutInner);
        return this;
    }

    public void copy(Invoice invoice) {
        invoice.setNumber(getNumber());
        invoice.setDate(getDate());
        invoice.setPrice(CommonTransform.transformBean(getPrice(), Price.class));
        invoice.setInvoiceState(getInvoiceState());
    }
}
