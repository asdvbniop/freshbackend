package org.freshcode.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by user on 26.08.16.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceRowDto {
    private String item;
    private Integer quantity;
    private Float rate;
    private Float amount;
}
