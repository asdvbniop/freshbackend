package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.InvoiceTemplateOption;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 25.08.16.
 */
@EqualsAndHashCode(exclude = {"client", "financialInfo", "ourClient", "ourFinancialInfo"}, callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Data
public class InvoiceTemplateDto extends AbstractDto<InvoiceTemplateDto> {
    private Currency currency;

    private ClientDto client;
    private ClientDto ourClient;
    private FinancialInfoDto ourFinancialInfo;
    boolean showQuantity;
    boolean showRate;
    boolean showAmount;

    private Map<InvoiceTemplateOption, String> options = new HashMap<>();
}
