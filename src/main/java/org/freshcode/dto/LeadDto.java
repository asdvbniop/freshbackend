package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.*;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.LeadState;
import org.freshcode.utils.CommonTransform;

import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 04.08.16.
 */
@Getter
@Setter
@EqualsAndHashCode(exclude = {"agreement","client"}, callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class LeadDto extends PriceAbleDto<LeadDto> {
    private String title;
    private String url;
    private LeadState leadState;
//    private List<Email> emails = new ArrayList<>();
    private List<FileDto> files = new ArrayList<>();
    private BidDto bid;
    private ClientDto client;
    private AgreementDto agreement;

    @JsonIgnore
    public String getPriceString(){return getPrice().format();}

    public static LeadDto create() {
        return new LeadDto("","", LeadState.DRAFT, null, null, null, null);
    }

    public void copy(Lead lead) {
        lead.setTitle(getTitle());
        lead.setDate(getDate());
        lead.setPrice(getPrice().transformToPrice());
        lead.setUrl(getUrl());
        lead.setLeadState(getLeadState());
    }

    @Override
    public LeadDto cutInner() {
        setAgreement(null);
        setClient(null);
        setBid(null);
        return this;
    }

    @Override
    public LeadDto cutOuter() {
        if(agreement!=null)
            agreement.cutInner();
        if(bid!=null)
            bid.cutInner();
        if(client!=null)
            client.cutInner();
        return this;
    }
}
