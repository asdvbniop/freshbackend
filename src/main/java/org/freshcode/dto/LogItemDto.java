package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import java.time.Duration;
import java.util.Date;

/**
 * Created by user on 22.08.16.
 */
@EqualsAndHashCode(callSuper = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class LogItemDto extends AbstractDto<LogItemDto> {
    private Integer minutes;
    private String description;
    private Date date;
    private String jiraUrl;
}
