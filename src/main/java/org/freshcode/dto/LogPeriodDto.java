package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.LogPeriod;

import java.util.*;

/**
 * Created by user on 22.08.16.
 */
@EqualsAndHashCode(exclude = "member", callSuper = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class LogPeriodDto extends AbstractDto<LogPeriodDto> {
    private Date fromDate;
    private Date toDate;
    private LogPeriod.LogPeriodType periodType;
    private Integer minutes;
    private MemberDto member;


    private List<LogItemDto> log = new ArrayList<>();

    public void copy(LogPeriod old) {
        old.setMinutes(getMinutes());
    }

    public enum Filters {MEMBER_ID,AGREEMENT_ID,DATE,MINUTES}

    @Override
    public LogPeriodDto cutInner() {
        setMember(null);
        return this;
    }

    @Override
    public LogPeriodDto cutOuter() {
        getMember().cutInner();
        return this;
    }
}
