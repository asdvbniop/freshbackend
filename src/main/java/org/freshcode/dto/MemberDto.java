package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.*;
import org.freshcode.domain.enums.AgreementType;
import org.freshcode.domain.enums.UserRole;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 04.08.16.
 */
@EqualsAndHashCode(exclude = {"log","overtimes"}, callSuper = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class MemberDto extends PriceAbleDto<MemberDto>{
    private UserDto person;
    private int ratio;
    private UserRole role;
    private AgreementType type;
    private List<OvertimeDto> overtimes = new ArrayList<>();
    private List<LogPeriodDto> log = new ArrayList<>();
    private ProjectDto project;
    private AgreementDto agreement;

    public MemberDto(Long id) {
        setId(id);
    }

    public void copy(Member oldMember) {
        oldMember.setRatio(ratio);
        oldMember.setRole(role);
        oldMember.setType(type);
        oldMember.setPrice(getPrice().transformToPrice());
    }

    @Override
    public MemberDto cutInner() {
        if(person!=null)
            person.cutInner();
        setOvertimes(null);
        setProject(null);
        setAgreement(null);
        setLog(null);
        return this;
    }

    @Override
    public MemberDto cutOuter() {
        if(person!=null)
            person.cutInner();
        if(agreement!=null)
            agreement.cutInner();
        if(project!=null)
            project.cutInner();
        if(overtimes!=null)
            overtimes.forEach(OvertimeDto::cutInner);
        if(log!=null)
            log.forEach(LogPeriodDto::cutInner);
        return this;
    }

    @JsonIgnore
    public String getUsername()
    {
        return person==null?"null":person.getUsername();
    }
}
