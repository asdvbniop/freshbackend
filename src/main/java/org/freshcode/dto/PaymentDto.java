package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.Agreement;
import org.freshcode.domain.Price;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.PaymentState;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.method.P;

import java.util.Date;

/**
 * Created by user on 04.08.16.
 */
@Getter
@Setter
@EqualsAndHashCode(exclude = {"agreement","invoice"}, callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class PaymentDto extends PriceAbleDto<PaymentDto>{
    public enum Filters{INVOICE_ID,AGREEMENT_ID,DATE,PRICE,STATE}
    private InvoiceDto invoice;
    private PaymentState state;
    private AgreementDto agreement;

    @Override
    public PaymentDto cutInner() {
        setInvoice(null);
        setAgreement(null);
        return this;
    }

    @Override
    public PaymentDto cutOuter() {
        if(invoice!=null)
            invoice.cutInner();
        if(agreement!=null)
            agreement.cutInner();
        return this;
    }

    @Override
    public Object getField(Enum e) {
        switch ((Filters) e) {
            case INVOICE_ID:
                return invoice.getId();
            case AGREEMENT_ID:
                return agreement.getId();
            case DATE:
                return getDate();
            case PRICE:
                return getPrice();
            case STATE:
                return state;
        }
        return null;
    }

    @JsonIgnore
    public String getPriceAsString()
    {
        return getPrice().format();
    }
}
