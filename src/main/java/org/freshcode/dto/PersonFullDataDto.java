package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.AbstractEntity;
import org.freshcode.domain.FinancialInfo;
import org.freshcode.domain.FullAddressData;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import java.util.Date;

/**
 * Created by user on 04.07.16.
 */
@Getter
@Setter
@EqualsAndHashCode(exclude = {"user","usdAcc","eurAcc","uahAcc","engAddr","rusAddr"}, callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class PersonFullDataDto extends AbstractDto<PersonFullDataDto> {
    private String fullNameEng;
    private String fullNameRus;
    private String fullNameUa;
    private Date birthday;
    private String phone;
    private String taxNumber;
    private String password;

    private FinancialInfoDto usdAcc;
    private FinancialInfoDto eurAcc;
    private FinancialInfoDto uahAcc;

    private FullAddressDataDto engAddr;
    private FullAddressDataDto rusAddr;
    private FullAddressDataDto uaAddr;

    ///////////
    private UserDto user;

    @Override
    public PersonFullDataDto cutOuter() {
        if(usdAcc!=null)
            usdAcc.setBank(null);
        if(eurAcc!=null)
            eurAcc.setBank(null);
        if(uahAcc!=null)
            uahAcc.setBank(null);
        return this;
    }
}
