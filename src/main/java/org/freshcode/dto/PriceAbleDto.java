package org.freshcode.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.freshcode.domain.enums.Currency;

import java.util.Date;

/**
 * Created by user on 18.08.16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PriceAbleDto<T> extends AbstractDto<T> {
    private PriceDto price=new PriceDto(0, Currency.USD);
    private Date date=new Date();
}
