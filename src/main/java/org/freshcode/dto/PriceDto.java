package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.freshcode.domain.Price;
import org.freshcode.domain.enums.Currency;
import org.freshcode.utils.CommonTransform;

/**
 * Created by user on 06.08.16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PriceDto {
    private Integer value=0;
    private Currency currency=Currency.USD;

    public String format() {
        return value.toString()+" "+currency.toString();
    }

    @JsonIgnore
    public Price transformToPrice(){
        return CommonTransform.transformBean(this, Price.class);
    }
}
