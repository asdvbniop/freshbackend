package org.freshcode.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.freshcode.domain.Agreement;
import org.freshcode.domain.HistoryItem;
import org.freshcode.domain.Member;
import org.freshcode.domain.Project;
import org.freshcode.domain.enums.EntityType;
import org.hibernate.annotations.Filter;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 04.08.16.
 */
@Getter
@Setter
@EqualsAndHashCode(exclude = {"members", "history", "agreement"}, callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ProjectDto extends AbstractDto<ProjectDto> {

    public enum Filters{NAME,COLOR,STATE}

    private String name;
    private Project.Color color;
    private Project.State state;
    private AgreementDto agreement;
    private List<MemberDto> members = new ArrayList<>();
    private List<HistoryItemDto> history = new ArrayList<>();

    public static ProjectDto create() {
        return new ProjectDto("", Project.Color.GREEN, Project.State.DRAFT, null, new ArrayList<>(), new ArrayList<>());
    }


    public ProjectDto cutInner() {
        setAgreement(null);
        setMembers(null);
        setHistory(null);
        return this;
    }

    @Override
    public ProjectDto cutOuter() {
        if(agreement!=null)
            agreement.cutInner();
        if(members!=null)
            members.forEach(MemberDto::cutInner);
        setHistory(null);
        return this;
    }
}
