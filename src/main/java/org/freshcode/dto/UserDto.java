package org.freshcode.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.freshcode.domain.Person;
import org.freshcode.domain.PersonFullData;
import org.freshcode.domain.Price;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.UserRole;

import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 04.08.16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDto extends  AbstractDto <UserDto>{
    private String username;
    private String jiraId;
    private String name;
    private PriceDto salary;
    private UserRole userRole;

    @OneToOne
    PersonFullDataDto personData;

    private List<ExpenseDto> payments = new ArrayList<>();
    private List<ExpenseDto> futureRise = new ArrayList<>();
    private List<ExpenseDto> plannedRise = new ArrayList<>();

    private List<MemberDto> memberList;

    public static UserDto create()
    {
        return new UserDto("","","",new PriceDto(0, Currency.UAH),UserRole.DEVELOPER,new PersonFullDataDto(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),new ArrayList<>());
    }

    public UserDto cut() {
        return cutInner();
    }

    @Override
    public UserDto cutInner() {
        setMemberList(null);
        setPlannedRise(null);
        setPayments(null);
        setFutureRise(null);
        setPersonData(null);
        return this;
    }

    @Override
    public UserDto cutOuter() {
        if(payments!=null)
            payments.forEach(ExpenseDto::cutInner);
        if(getFutureRise()!=null)
            futureRise.forEach(ExpenseDto::cutInner);
        if(plannedRise!=null)
            plannedRise.forEach(ExpenseDto::cutInner);
        if(memberList!=null)
            memberList.forEach(MemberDto::cutInner);
        if(personData!=null)
            personData.cutOuter();
        return this;
    }

    public void copyValues(Person oldPerson) {
        oldPerson.setUsername(getUsername());
        oldPerson.setJiraId(getJiraId());
        oldPerson.setName(getName());
        oldPerson.setUserRole(getUserRole());
    }
}
