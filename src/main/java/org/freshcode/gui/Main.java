package org.freshcode.gui;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.freshcode.gui.service.BaseService;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.Convert;


import java.io.*;

/**
 * Created by user on 27.06.16.
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("main.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 1000, 750));
        primaryStage.show();

        Convert.services = getHostServices();
    }

    public static void main(String[] args) throws IOException {
        System.setProperty("org.apache.http.wire", "fatal");
        System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "fatal");
        BaseService.init();
        TService.initServices();
        BasicEntity.initMap();
        launch(args);
    }
}
