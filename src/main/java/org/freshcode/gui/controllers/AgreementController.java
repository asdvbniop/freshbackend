package org.freshcode.gui.controllers;

import com.google.common.collect.Lists;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.util.Pair;
import org.filters.CommonFilter;
import org.freshcode.domain.Invoice;
import org.freshcode.domain.enums.AgreementState;
import org.freshcode.domain.enums.AgreementType;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.UserRole;
import org.freshcode.dto.*;
import org.freshcode.gui.service.LogPeriodService;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.*;
import org.freshcode.utils.CommonTransform;

import java.net.URL;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static java.time.temporal.TemporalAdjusters.firstDayOfMonth;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

/**
 * Created by user on 31.07.16.
 */
public class AgreementController extends BaseEntityController<AgreementDto> {
    public ListView<MemberDto> members;
    public TextField priceValue;
    public ComboBox<Currency> priceCurrency;
    public ComboBox<AgreementType> type;
    public DatePicker date;
    public TextField name;
    public TextField lead;
    public Button goLead;
    public Button assignLead;
    public Button goProject;
    public Button assignProject;
    public Button createProject;
    public ComboBox<AgreementState> state;
    public TextField project;
    public HBox priceHbox;
    public Button createMember;
    public Button editInvoiceTemplate;

    private AgreementDto agreement;
    private LeadDto parentLead;
    private ProjectDto childProject;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        date.setConverter(new DateTimeConverter());
        type.getItems().addAll(AgreementType.values());
        state.getItems().addAll(AgreementState.values());
        priceCurrency.getItems().addAll(Currency.values());

        members.setCellFactory(p -> new ColoredListCell());
        members.setOnMouseClicked(event -> {
            if(event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount()==2)
            {
                MemberDto item = members.getSelectionModel().getSelectedItem();
                if(item!=null)
                {
                    eventBus.post(NavigationEvent.createViewView(item, agreement));
                }
            }
        });
//        members.getSelectionModel().selectedItemProperty().addListener((o, old, n) -> setMember(n));

//        type.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<AgreementType>() {
//            @Override
//            public void changed(ObservableValue<? extends AgreementType> observable, AgreementType oldValue, AgreementType newValue) {
//                switch (newValue) {
//                    case FIXED:
//                        priceHbox.setVisible(true);
//                        memberPriceHbox.setVisible(false);
//                        paymentType.getItems().setAll(AgreementType.FIXED);
//                        break;
//                    case PER_HOUR:
//                        memberPriceHbox.setVisible(true);
//                        priceHbox.setVisible(false);
//                        paymentType.getItems().setAll(AgreementType.PER_HOUR);
//                        break;
//                    case MIXED:
//                        paymentType.getItems().setAll(Lists.newArrayList(AgreementType.FIXED, AgreementType.PER_HOUR));
//                        memberPriceHbox.setVisible(true);
//                        priceHbox.setVisible(true);
//                        break;
//                }
//                paymentType.getSelectionModel().selectFirst();
//            }
//        });

        assignProject.setOnAction(ev->ObjectExplorer.select(Etype.PROJECT, null, e->setProject(e.getObject())));
        assignLead.setOnAction(ev->ObjectExplorer.select(Etype.LEAD, null, e->setLead(e.getObject())));

        goProject.setOnAction(ev->eventBus.post(NavigationEvent.createViewView(childProject, agreement)));
        goLead.setOnAction(ev->eventBus.post(NavigationEvent.createViewView(parentLead, agreement)));
        createProject.setOnAction(ev->eventBus.post(NavigationEvent.createCreateView(Etype.PROJECT, agreement, agreement)));
        createMember.setOnAction(ev -> eventBus.post(NavigationEvent.createCreateView(Etype.MEMBER, agreement, agreement)));
        editInvoiceTemplate.setOnAction(ev -> eventBus.post(NavigationEvent.createViewView(agreement.getInvoiceTemplate(), agreement)));
    }


    @Override
    public void setEntity(AgreementDto agree) {
        agreement = agree;
        name.setText(agree.getName());
        date.setValue(DateUtils.asLocalDate(agree.getDate()));
        state.getSelectionModel().select(agree.getState());
        type.getSelectionModel().select(agree.getAgreementType());
        priceValue.setText(agree.getPrice().getValue().toString());
        priceCurrency.getSelectionModel().select(agree.getPrice().getCurrency());
        setLead(agree.getLead());
        setProject(agree.getProject());

        if(agree.getMembers()!=null)
        {
            members.getItems().setAll(agree.getMembers());
            members.getSelectionModel().selectFirst();
        }
    }

    @Override
    public AgreementDto createEntity(BasicEntity entity) {
        AgreementDto dto = AgreementDto.create();
        if(entity!=null)
        {
            if(entity.getType().equals(Etype.LEAD))
            {
                dto.setLead(entity.getObject());
                dto.setPrice(CommonTransform.transformBean(dto.getLead().getPrice(), PriceDto.class));
            }
        }
        return dto;
    }

    private AgreementDto fill() {
        agreement.setName(name.getText());
        agreement.setDate(DateUtils.asUtilDate(date.getValue()));
        agreement.setState(state.getSelectionModel().getSelectedItem());
        agreement.setAgreementType(type.getSelectionModel().getSelectedItem());
        agreement.getPrice().setValue(Integer.valueOf(priceValue.getText()));
        agreement.getPrice().setCurrency(priceCurrency.getSelectionModel().getSelectedItem());
        agreement.setLead(parentLead);
        agreement.setProject(childProject);
        return agreement;
    }

    private void setProject(ProjectDto projectDto)
    {
        childProject = projectDto;
        project.setText(projectDto==null?"":projectDto.getName());
        goProject.setVisible(projectDto!=null);
        createProject.setVisible(agreement.getId()!=null && projectDto==null);
    }

    private void setLead(LeadDto leadDto)
    {
        parentLead=leadDto;
        lead.setText(leadDto==null?"":leadDto.getId().toString());
        goLead.setVisible(leadDto!=null);
    }

    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.AGREEMENT).saveOrUpdate(fill()));
    }

    public void onPayments(ActionEvent actionEvent) {
    }

    public void onInvoices(ActionEvent actionEvent) {
        NavigationEvent event = new NavigationEvent();
        event.setTx(NavigationEvent.Action.LIST, Etype.INVOICE);
        event.setRx(NavigationEvent.Action.VIEW, agreement);
        event.getTx().setParenEntity(new BasicEntity(agreement));
        eventBus.post(event);
    }

    public void onExpenses(ActionEvent actionEvent) {
        NavigationEvent event = new NavigationEvent();
        event.setTx(NavigationEvent.Action.LIST, Etype.PAYMENT);
        event.setRx(NavigationEvent.Action.VIEW, agreement);
        event.getTx().getFilters().add(CommonFilter.create(
                CommonFilter.FilterType.SINGLE_LONG,
                PaymentDto.Filters.AGREEMENT_ID,
                agreement.getId()));
        eventBus.post(event);
    }

    public void onHistory(ActionEvent actionEvent) {
    }

    public void onCreateInvoice(ActionEvent actionEvent) {
        eventBus.post(NavigationEvent.createCreateView(Etype.INVOICE,agreement,agreement));
    }

    private Color getColor(MemberDto dto)
    {
        if(dto.getPerson()==null)
        {
            return Color.color(1, 0.65, 0.65);
        }else if(dto.getId()==null){
            return Color.color(0.88, 0.75, 0.25);
        }
//        if (membersToUpdate.contains(dto))
//        {
//            return Color.color(0.5, 0.35, 0.35);
//        }
        else
        {
            return Color.color(0.2, 0.75, 0.55);
        }
    }

    public void onViewLog(ActionEvent actionEvent) {
        TService service = TService.get(Etype.LOG_PERIOD);
        NavigationEvent event = new NavigationEvent();
        event.setTx(NavigationEvent.Action.LIST, Etype.LOG_PERIOD);
        event.getTx().setSource(NavigationEvent.Source.PROVIDER);
        event.getTx().setListProvider((filters) -> service.withFilters(filters).getAllByParentId(agreement.getId(),"logPeriodsByAgreement"));
        event.getTx().setParenEntity(new BasicEntity(agreement));
        LocalDate now = LocalDate.now();
        Pair<Date,Date> range = new Pair<>(
                DateUtils.asUtilDate(now.with(firstDayOfMonth())),
                DateUtils.asUtilDate(now.with(lastDayOfMonth())));
        event.getTx().getFilters().add(CommonFilter.create(CommonFilter.FilterType.DATE_RANGE, LogPeriodDto.Filters.DATE,range));
        event.setRx(NavigationEvent.Action.VIEW, agreement);
        eventBus.post(event);
    }

    private class ColoredListCell extends ListCell<MemberDto> {
        @Override
        protected void updateItem(MemberDto item, boolean empty) {
            super.updateItem(item, empty);
            setText(empty ? "" : item.getPerson()==null?"no user":item.getPerson().getUsername()+" :: "+item.getRole());
            setBackground(new Background(new BackgroundFill(empty ? null : getColor(item), CornerRadii.EMPTY, Insets.EMPTY)));
        }
    }
}
