package org.freshcode.gui.controllers;

import com.google.common.collect.Lists;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.freshcode.domain.Bank;
import org.freshcode.domain.Bid;
import org.freshcode.domain.Person;
import org.freshcode.domain.enums.Currency;
import org.freshcode.dto.*;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.*;
import org.freshcode.utils.CommonTransform;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by user on 28.06.16.
 */
public class BankController extends BaseEntityController<BankDto>{

    public TextField name;
    public TextField swift;
    public TextField mfo;
    public TextField edr;
    public AnchorPane addrEditor;
    private BiAddrController addrController;

    private BankDto bank;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addrController = new BiAddrController().load(addrEditor);
    }


    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.BANK).saveOrUpdate(fill()));
    }

    private AbstractDto fill() {
        bank.setName(name.getText());
        bank.setSwift(swift.getText());
        bank.setMfo(mfo.getText());
        bank.setEdr(mfo.getText());
        bank.setEng(addrController.getData().getEng());
        bank.setRus(addrController.getData().getRus());
        return bank;
    }

    @Override
    public void setEntity(BankDto bankDto) {
        bank=bankDto;
        name.setText(bank.getName());
        swift.setText(bank.getSwift());
        mfo.setText(bank.getMfo());
        edr.setText(bank.getEdr());
        addrController.setAddr(new BiAddrController.BiAddrData(bank.getEng(), bank.getRus(), bank.getRus()));
    }



    @Override
    public BankDto createEntity(BasicEntity parent) {
        BankDto result = new BankDto();
        result.setEng(new FullAddressDataDto());
        result.setRus(new FullAddressDataDto());
        return result;
    }
}
