package org.freshcode.gui.controllers;

import com.google.common.eventbus.EventBus;
import javafx.fxml.Initializable;
import lombok.Setter;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.NavigationEvent;


/**
 * Created by user on 01.08.16.
 */
public abstract class BaseEntityController<T> implements Initializable {
    @Setter
    protected EventBus eventBus;
    public abstract BasicEntity getResult();

    public abstract void setEntity(T t);
    public abstract T createEntity(BasicEntity parent);

    public void processEvent(NavigationEvent.NavigationData e)
    {
        switch (e.action) {
            case CREATE:
                setEntity(createEntity(e.getParenEntity()));
                break;
            case VIEW:
                setEntity((T) e.getEntity().getObject());
                break;
            case EDIT:
                setEntity((T) e.getEntity().getObject());
                break;
        }
    }


}
