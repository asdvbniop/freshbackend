package org.freshcode.gui.controllers;

import com.google.common.eventbus.EventBus;
import javafx.fxml.Initializable;
import lombok.Getter;
import lombok.Setter;
import org.filters.CommonFilter;
import org.freshcode.dto.ProjectDto;
import org.freshcode.gui.controllers.filters.ProjectFilter;

import java.util.*;

/**
 * Created by user on 06.08.16.
 */
public abstract class BaseFilterController<T,R extends Enum<R>> implements Initializable {
    public void prepareData(List<T> results){}

    public interface Filter<T> {boolean process(T t, String value);}

    @Setter
    protected EventBus eventBus;
    @Getter
    private Map<Enum<R>,Filter<T>> predicates = new HashMap<>();

    public void setFilters(Map<Enum<R>, String> filtersValue){}

    public List<T> process(List<T> all, Map<Enum<R>, String> filters) {
        if (filters == null)
            return all;

        filters.entrySet().stream()
                .filter(e -> !e.getValue().equals("ALL"))
                .forEach(e -> all.removeIf(o -> !getPredicates().get(e.getKey()).process(o, e.getValue())));
        return all;
    }

    public List<CommonFilter> getFilters(){return new ArrayList<>();}
}
