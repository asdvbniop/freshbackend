package org.freshcode.gui.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.freshcode.domain.FullAddressData;
import org.freshcode.dto.FullAddressDataDto;
import org.freshcode.utils.Utils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by user on 28.06.16.
 */
public class BiAddrController implements Initializable {


    public TableView<AddrRow> addr;

    private BiAddrData data;

    public BiAddrController load(AnchorPane addrEditor) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("biAddrEditor.fxml"));
            AnchorPane editor = loader.load();
            addrEditor.getChildren().setAll(editor);
            return loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Data
    @AllArgsConstructor
    public class AddrRow
    {
        String type;
        String eng;
        String rus;
        String ua;
    }

    @Data
    @AllArgsConstructor
    public static class BiAddrData
    {
        public FullAddressDataDto eng;
        public FullAddressDataDto rus;
        public FullAddressDataDto ua;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TableColumn<AddrRow,String> name = new TableColumn<>("Name");
        TableColumn<AddrRow,String> rus = new TableColumn<>("Rus");
        TableColumn<AddrRow,String> eng = new TableColumn<>("Eng");
        TableColumn<AddrRow,String> ua = new TableColumn<>("Ua");

        name.setCellValueFactory(new PropertyValueFactory("type"));
        eng.setCellValueFactory(new PropertyValueFactory("eng"));
        rus.setCellValueFactory(new PropertyValueFactory("rus"));
        ua.setCellValueFactory(new PropertyValueFactory("ua"));

        addr.getColumns().addAll(name,eng,rus,ua);

        eng.setEditable(true);
        rus.setEditable(true);
        ua.setEditable(true);
        addr.setEditable(true);
        eng.setOnEditCommit(t -> t.getRowValue().setEng(t.getNewValue()));
        rus.setOnEditCommit(t -> t.getRowValue().setRus(t.getNewValue()));
        ua.setOnEditCommit(t -> t.getRowValue().setUa(t.getNewValue()));

        eng.setCellFactory(TextFieldTableCell.forTableColumn());
        rus.setCellFactory(TextFieldTableCell.forTableColumn());
        ua.setCellFactory(TextFieldTableCell.forTableColumn());

    }

    public void setAddr(BiAddrData data) {
        this.data=data;
        addr.getItems().setAll(buildRows(data.getEng(),data.getRus(), data.getUa()));
    }

    public BiAddrData getData()
    {
        for (AddrRow addrRow : addr.getItems()) {
            try {
                Field f = FullAddressDataDto.class.getDeclaredField(addrRow.getType());
                f.set(data.eng, addrRow.getEng());
                f.set(data.rus, addrRow.getRus());
                f.set(data.ua, addrRow.getUa());
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    private List<AddrRow> buildRows(FullAddressDataDto eng, FullAddressDataDto rus, FullAddressDataDto ua) {
        List<AddrRow> rows = new ArrayList<>();
        for (Field field : FullAddressDataDto.class.getDeclaredFields()) {
            rows.add(new AddrRow(field.getName(), getFieldsValue(field,eng), getFieldsValue(field,rus), getFieldsValue(field,ua)));
        }
        return rows;
    }

    private String getFieldsValue(Field field,FullAddressDataDto data)
    {
        try {
            return field.get(data).toString();
        } catch (Exception e) {
            return "";
        }
    }

    public void putCity(ActionEvent actionEvent) {
        addr.getItems().stream()
                .filter(row -> row.getType().equals("city")).forEach(row -> {
            row.setEng("Zaporozhye");row.setRus("Запорожье");row.setUa("Запоріжжя");});
        addr.refresh();
    }

    public void putCountry(ActionEvent actionEvent) {
        addr.getItems().stream()
                .filter(row -> row.getType().equals("country")).forEach(row -> {
                row.setEng("Ukraine");row.setRus("Украина");row.setUa("Україна");});
        addr.refresh();
    }

    public void transliterateForce(ActionEvent actionEvent) {
        addr.getItems().stream()
                .forEach(row -> row.setEng(Utils.tru.transliterate(row.getRus())));
        addr.refresh();

    }

    public void transliterate(ActionEvent actionEvent) {
        addr.getItems().stream()
                .filter(row -> row.getEng() == null || row.getEng().isEmpty())
                .forEach(row -> row.setEng(Utils.tru.transliterate(row.getRus())));
        addr.refresh();
    }

    public void copyIndex(ActionEvent actionEvent) {
        for (AddrRow row : addr.getItems()) {
            if(row.getType().equals("index"))
            {
                if(!row.getEng().isEmpty())
                {
                    row.setRus(row.getEng());
                    row.setUa(row.getEng());
                } else if(!row.getRus().isEmpty())
                {
                    row.setEng(row.getRus());
                    row.setUa(row.getRus());
                } else if(!row.getUa().isEmpty())
                {
                    row.setEng(row.getUa());
                    row.setRus(row.getUa());
                }
                addr.refresh();
            }
        }
    }
}
