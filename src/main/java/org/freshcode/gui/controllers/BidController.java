package org.freshcode.gui.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.*;
import org.freshcode.domain.Bid;
import org.freshcode.domain.enums.Currency;
import org.freshcode.dto.BidDto;
import org.freshcode.dto.LeadDto;
import org.freshcode.dto.PriceDto;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.*;
import org.freshcode.utils.CommonTransform;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by user on 28.06.16.
 */
public class BidController extends BaseEntityController<BidDto>{


    public TextField price;
    public ComboBox<Currency> currency;
    public TextField url;
    public ComboBox<Bid.BidState> state;
    public TextField lead;
    public Button goLeadB;
    public Button assignLeadB;
    public Button createLeadB;
    public TextArea comment;
    public DatePicker date;
    private LeadDto childLead;
    private BidDto bid;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currency.getItems().addAll(Currency.values());
        date.setConverter(new DateTimeConverter());
    }

    private void setLead(LeadDto leadDto)
    {
        childLead=leadDto;
        goLeadB.setVisible(childLead!=null);
        createLeadB.setVisible(childLead==null);
        lead.setText(childLead==null?"": Convert.fromLead(childLead));
    }

    private BidDto fill()
    {
        bid.setDate(DateUtils.asUtilDate(date.getValue()));
        bid.setBidState(state.getSelectionModel().getSelectedItem());
        bid.getPrice().setValue(Integer.valueOf(price.getText()));
        bid.getPrice().setCurrency(currency.getSelectionModel().getSelectedItem());
        bid.setUrl(url.getText());
        bid.setReason(comment.getText());
        bid.setLead(childLead);
        return bid;
    }

    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.BID).saveOrUpdate(fill()));
    }

    @Override
    public void setEntity(BidDto bidDto) {
        this.bid = bidDto;
        date.setValue(DateUtils.asLocalDate(bidDto.getDate()));
        state.getSelectionModel().select(bidDto.getBidState());
        price.setText(bidDto.getPrice().getValue().toString());
        currency.getSelectionModel().select(bidDto.getPrice().getCurrency());
        url.setText(bidDto.getUrl());
        comment.setText(bidDto.getReason());
        setLead(bidDto.getLead());
    }

    @Override
    public BidDto createEntity(BasicEntity parent) {
        BidDto result = BidDto.create();
        if(parent !=null)
        {
            result.setLead(parent.getObject());
            PriceDto priceDto = new PriceDto();
            CommonTransform.copyBean(result.getLead().getPrice(), priceDto);
            result.setPrice(priceDto);
        }
        return result;
    }

    public void goLead(ActionEvent actionEvent) {
        NavigationEvent ev = new NavigationEvent();
        ev.setTx(NavigationEvent.Action.VIEW, childLead);
        ev.setRx(NavigationEvent.Action.VIEW, bid);
        eventBus.post(ev);
    }

    public void assignLead(ActionEvent actionEvent) {
        ObjectExplorer.select(Etype.LEAD, null, e->setLead(e.getObject()));
    }

    public void createLead(ActionEvent actionEvent) {
        NavigationEvent event = new NavigationEvent();
        event.setTx(NavigationEvent.Action.CREATE, Etype.LEAD);
        event.getTx().setParenEntity(new BasicEntity(bid));
        event.setRx(NavigationEvent.Action.EDIT, bid);
        eventBus.post(event);
    }
}
