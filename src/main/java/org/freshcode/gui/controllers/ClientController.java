package org.freshcode.gui.controllers;

import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseButton;
import javafx.util.StringConverter;
import org.freshcode.dto.*;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.Etype;
import org.freshcode.gui.utils.NavigationEvent;

import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by user on 28.06.16.
 */
public class ClientController extends BaseEntityController <ClientDto> {

    public TextField name;
    public ListView<ContactInfoDto> contacts;
    public ListView<FinancialInfoDto> accounts;
    public Button addContact;
    public Button addAccount;
    public TextArea addr;

    private ClientDto clientDto;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        contacts.setCellFactory(TextFieldListCell.forListView(new StringConverter<ContactInfoDto>() {
            @Override
            public String toString(ContactInfoDto o) {
                return o.getName() + "  "+ o.getType();
            }

            @Override
            public ContactInfoDto fromString(String string) {
                return null;
            }
        }));

        contacts.setOnMouseClicked(e -> {
            if(e.getClickCount()==2 && e.getButton().equals(MouseButton.PRIMARY))
            {
                ContactInfoDto dto = contacts.getSelectionModel().getSelectedItem();
                if(dto!=null)
                    eventBus.post(NavigationEvent.createViewView(dto, clientDto));
            }
        });

        accounts.setOnMouseClicked(e -> {
            if(e.getClickCount()==2 && e.getButton().equals(MouseButton.PRIMARY))
            {
                FinancialInfoDto dto = accounts.getSelectionModel().getSelectedItem();
                if(dto!=null)
                    eventBus.post(NavigationEvent.createViewView(dto, clientDto));
            }
        });

        accounts.setCellFactory(TextFieldListCell.forListView(new StringConverter<FinancialInfoDto>() {
            @Override
            public String toString(FinancialInfoDto o) {
                return o.getName();
            }

            @Override
            public FinancialInfoDto fromString(String string) {
                return null;
            }
        }));

        addAccount.setOnAction(event -> eventBus.post(NavigationEvent.createCreateView(Etype.FINANCIAL_INFO,clientDto,clientDto)));
        addContact.setOnAction(event -> eventBus.post(NavigationEvent.createCreateView(Etype.CONTACT_INFO,clientDto,clientDto)));
    }

    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.CLIENT).saveOrUpdate(fill()));
    }

    private ClientDto fill() {
        clientDto.setName(name.getText());
        clientDto.setAddress(addr.getText());
        return clientDto;
    }

    @Override
    public void setEntity(ClientDto clientDto) {
        this.clientDto = clientDto;
        name.setText(clientDto.getName());
        addr.setText(clientDto.getAddress());
        if(clientDto.getContacts()!=null)
            contacts.getItems().setAll(clientDto.getContacts());
        if(clientDto.getBankAccounts()!=null)
            accounts.getItems().setAll(clientDto.getBankAccounts());
    }

    @Override
    public ClientDto createEntity(BasicEntity parent) {
        ClientDto clientDto = new ClientDto("","", null, null, null);
        if(parent.getType().equals(Etype.LEAD))
        {
            clientDto.setLead(parent.getObject());
        }
        return clientDto;
    }
}
