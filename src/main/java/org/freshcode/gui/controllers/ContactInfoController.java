package org.freshcode.gui.controllers;

import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.freshcode.domain.enums.InfoType;
import org.freshcode.dto.ClientDto;
import org.freshcode.dto.ContactInfoDto;
import org.freshcode.gui.utils.BasicEntity;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by user on 25.08.16.
 */
public class ContactInfoController extends BaseEntityController<ContactInfoDto> {
    public TextField role;
    public ComboBox<InfoType> type;
    public TextField name;
    public TextField value;
    public ClientDto parent;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        type.getItems().setAll(InfoType.values());
    }

    @Override
    public BasicEntity getResult() {
        return null;
    }

    @Override
    public void setEntity(ContactInfoDto contactInfoDto) {
        role.setText(contactInfoDto.getRole());
        type.getSelectionModel().select(contactInfoDto.getType());
        name.setText(contactInfoDto.getName());
        value.setText(contactInfoDto.getValue());
        parent = contactInfoDto.getClient();
    }

    @Override
    public ContactInfoDto createEntity(BasicEntity parent) {
        return new ContactInfoDto("", InfoType.EMAIL, "", "", null);
    }
}
