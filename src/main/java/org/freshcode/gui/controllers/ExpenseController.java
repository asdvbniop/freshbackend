package org.freshcode.gui.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.*;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.ExpenseType;
import org.freshcode.dto.*;
import org.freshcode.gui.utils.*;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by user on 28.06.16.
 */
public class ExpenseController extends BaseEntityController<ExpenseDto>{


    public TextField price;
    public ComboBox<Currency> currency;
    public ComboBox<ExpenseType> type;
    public TextField project;
    public Button goProjectB;
    public Button assignProjectB;
    public DatePicker date;
    public TextField user;
    public Button goUserB;
    public Button assignUserB;
    public TextArea comment;

    private ExpenseDto expense;
    private UserDto parentUser;
    private AgreementDto parentAgreement;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        type.getItems().addAll(ExpenseType.values());
        currency.getItems().addAll(Currency.values());
        date.setConverter(new DateTimeConverter());
    }

    private void setUser(UserDto userDto)
    {
        parentUser = userDto;
        user.setText(userDto==null?"":userDto.getUsername());
        goUserB.setVisible(parentUser!=null);
    }

    private void setAgreement(AgreementDto agreementDto)
    {
        parentAgreement = agreementDto;
        project.setText(agreementDto==null?"":agreementDto.getId().toString());
        goProjectB.setVisible(agreementDto!=null);
    }


    @Override
    public BasicEntity getResult() {
        return null;
    }

    @Override
    public void setEntity(ExpenseDto expenseDto) {
        expense = expenseDto;
        date.setValue(DateUtils.asLocalDate(expenseDto.getDate()));
        price.setText(expense.getPrice().getValue().toString());
        currency.getSelectionModel().select(expense.getPrice().getCurrency());
        type.getSelectionModel().select(expense.getType());
        comment.setText(expense.getComment());
        setUser(expenseDto.getPerson());
        setAgreement(expenseDto.getAgreement());
    }

    @Override
    public ExpenseDto createEntity(BasicEntity parent) {
        ExpenseDto result = ExpenseDto.create();
        if(parent !=null)
        {
            if(parent.getType().equals(Etype.USER))
            {
                setUser(parent.getObject());
            }
            else if (parent.getType().equals(Etype.PROJECT))
            {
                setAgreement(parent.getObject());
            }
        }
        return result;
    }

    public void goLead(ActionEvent actionEvent) {
    }

    public void assignLead(ActionEvent actionEvent) {
    }

    public void goUser(ActionEvent actionEvent) {
    }

    public void assignUser(ActionEvent actionEvent) {
    }
}
