package org.freshcode.gui.controllers;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import org.freshcode.domain.enums.FileType;
import org.freshcode.dto.FileDto;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.*;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by user on 28.08.16.
 */
public class FileController extends BaseEntityController<FileDto> {

    public DatePicker updated;
    public TextField name;
    public ComboBox<FileType> type;
    public DatePicker archived;
    public DatePicker created;
    public TextField path;
    public Button viewB;
    public Button downloadB;
    public Button archiveB;
    public Button deleteB;

    private FileDto file;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        type.getItems().setAll(FileType.values());
        created.setConverter(new DateTimeConverter());
        updated.setConverter(new DateTimeConverter());
        archived.setConverter(new DateTimeConverter());
        viewB.setOnAction(event -> viewFile());
    }

    private void viewFile() {
        Convert.services.showDocument("http://www.yahoo.com");
    }

    @Override
    public BasicEntity getResult() {
        file.setFilename(name.getText());
        return new BasicEntity(TService.get(Etype.FILE).saveOrUpdate(file));
    }

    @Override
    public void setEntity(FileDto fileDto) {
        file = fileDto;
        name.setText(fileDto.getFilename());
        path.setText(file.getPath());
        type.getSelectionModel().select(file.getFileType());
        created.setValue(DateUtils.asLocalDate(file.getCreated()));
        updated.setValue(DateUtils.asLocalDate(file.getUpdated()));
        if(file.getArchived()!=null)
            archived.setValue(DateUtils.asLocalDate(file.getArchived()));
    }

    @Override
    public FileDto createEntity(BasicEntity parent) {
        return null;
    }
}
