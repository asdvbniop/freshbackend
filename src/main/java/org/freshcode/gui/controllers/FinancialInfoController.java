package org.freshcode.gui.controllers;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.util.StringConverter;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.EntityType;
import org.freshcode.domain.enums.InfoType;
import org.freshcode.dto.*;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.Etype;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by user on 25.08.16.
 */
public class FinancialInfoController extends BaseEntityController<FinancialInfoDto> {
    public TextField name;
    public TextField account;
    public ComboBox<BankDto> bank;
    public ComboBox<Currency> currency;
    public Button copyToName;

    FinancialInfoDto info;
    BankDto bankDto;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currency.getItems().setAll(Currency.values());
        bank.setConverter(new StringConverter<BankDto>() {
            @Override
            public String toString(BankDto object) {
                return object.getName()==null?"":object.getName();
            }

            @Override
            public BankDto fromString(String string) {
                return null;
            }
        });
        copyToName.setOnAction(e->name.setText(currency.getSelectionModel().getSelectedItem().toString()));
    }

    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.FINANCIAL_INFO).saveOrUpdate(fill()));
    }

    private AbstractDto fill() {
        info.setName(name.getText());
        info.setAccount(account.getText());
        info.setCurrency(currency.getSelectionModel().getSelectedItem());
        info.setBank(bank.getSelectionModel().getSelectedItem());
        return info;
    }

    public void updateBanks()
    {
        bank.getItems().setAll(TService.get(Etype.BANK).getAll());
    }

    @Override
    public void setEntity(FinancialInfoDto f) {
        updateBanks();
        info = f;
        bankDto = f.getBank();
        name.setText(f.getName());
        account.setText(f.getAccount());
        currency.getSelectionModel().select(f.getCurrency());
        bank.getSelectionModel().select(f.getBank());

        if(info.getParentType() !=null && info.getParentType().equals(EntityType.USER_DATA))
            currency.setDisable(true);
    }

    //never used, new dtos passed via VIEW action
    @Override
    public FinancialInfoDto createEntity(BasicEntity parent) {
        FinancialInfoDto result = new FinancialInfoDto();
        if(parent.getObject()!=null)
        {
            if(parent.getType().equals(Etype.USER_DATA))
            {
                result.setParentType(EntityType.USER_DATA);
                result.setParentId(parent.getObject().getId());
            }
        }

        return result;
    }
}
