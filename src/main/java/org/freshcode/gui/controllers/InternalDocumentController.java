package org.freshcode.gui.controllers;

import com.sun.deploy.uitoolkit.impl.fx.ui.FXAboutDialog;
import com.sun.nio.zipfs.ZipPath;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import org.filters.CommonFilter;
import org.freshcode.domain.InternalDocument;
import org.freshcode.domain.enums.AgreementState;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.InternalDocumentType;
import org.freshcode.dto.*;
import org.freshcode.gui.service.BaseService;
import org.freshcode.gui.service.FileService;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.*;

import java.awt.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by user on 31.07.16.
 */
public class InternalDocumentController extends BaseEntityController<InternalDocumentDto> {

    public DatePicker fromDate;
    public TextField name;
    public TextField person;
    public Button goPerson;
    public Button assignPerson;
    public DatePicker toDate;
    public AnchorPane acceptancePane;
    public AnchorPane childrenPane;
    public TextField parentField;
    public Button goParent;
    public ComboBox<InternalDocumentType> docType;
    public ComboBox<AgreementState> docState;
    public TextField price;
    public ComboBox<Currency> currency;
    public Button getFile;
    public Button render;
    public ListView<InternalDocumentDto> invoices;
    public DatePicker date;
    public ComboBox<UserDto> customerC;
    public VBox helpInfo;
    public TextField helpDest;
    public TextField helpAddr;
    public TextField helpName;
    public TextField helpUsd;


    private InternalDocumentDto document;
    private UserDto childUser;
    private InternalDocumentDto parent;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initGenerators();
        helpInfo.setVisible(false);
        docType.getItems().addAll(InternalDocumentType.values());
        docState.getItems().addAll(AgreementState.values());
        currency.getItems().addAll(Currency.values());
        fromDate.setConverter(new DateTimeConverter());
        toDate.setConverter(new DateTimeConverter());
        date.setConverter(new DateTimeConverter());
        childrenPane.setVisible(false);
        acceptancePane.setVisible(false);
        goParent.setOnAction(e->eventBus.post(NavigationEvent.createViewView(parent,document)));
        goPerson.setOnAction(e->eventBus.post(NavigationEvent.createViewView(childUser,document)));
        getFile.setOnAction(this::onGetFile);
        currency.getSelectionModel().select(Currency.USD);
        customerC.setConverter(new StringConverter<UserDto>() {
            @Override
            public String toString(UserDto object) {
                return object.getName();
            }

            @Override
            public UserDto fromString(String string) {
                return null;
            }
        });

        invoices.setOnMouseClicked(event -> {
            if(event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount()==2)
            {
                InternalDocumentDto s = invoices.getSelectionModel().getSelectedItem();
                if(s!=null)
                {
                    eventBus.post(NavigationEvent.createViewView(s, document));
                }
            }
        });
        render.setOnAction(this::onRender);

        invoices.setCellFactory(TextFieldListCell.forListView(new StringConverter<InternalDocumentDto>() {
            @Override
            public String toString(InternalDocumentDto doc) {
                return DateUtils.shortFormat.format(doc.getDate())+"   |   "+doc.getName();
            }

            @Override
            public InternalDocumentDto fromString(String string) {
                return null;
            }
        }));

    }

    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.INTERNAL_DOCUMENT).saveOrUpdate(fill()));
    }

    private InternalDocumentDto fill() {
        document.setName(name.getText());
        document.getPrice().setValue(Integer.valueOf(price.getText()));
        document.getPrice().setCurrency(currency.getSelectionModel().getSelectedItem());
        document.setFromDate(DateUtils.asUtilDate(fromDate.getValue()));
        document.setToDate(DateUtils.asUtilDate(toDate.getValue()));
        document.setDate(DateUtils.asUtilDate(date.getValue()));
        document.setCustomer(customerC.getSelectionModel().getSelectedItem());
        return document;
    }

    @Override
    public void setEntity(InternalDocumentDto documentDto) {
        helpInfo.setVisible(false);
        document=documentDto;
        name.setText(documentDto.getName());
        fromDate.setValue(DateUtils.asLocalDate(documentDto.getFromDate()));
        date.setValue(DateUtils.asLocalDate(documentDto.getDate()));
        toDate.setValue(DateUtils.asLocalDate(documentDto.getToDate()));
        docType.getSelectionModel().select(document.getType());
        docState.getSelectionModel().select(documentDto.getState());
        price.setText(document.getPrice().getValue().toString());
        currency.getSelectionModel().select(document.getPrice().getCurrency());
        setUser(documentDto.getPerson());
        setParentDto(documentDto.getParent());

        if(documentDto.getId()==null) {
            goPerson.setVisible(false);
            goParent.setVisible(false);
        }

        processDocType();

        if(document.getChildren()!=null)
        {
            invoices.getItems().setAll(document.getChildren());
        }

        if(documentDto.getCustomer()!=null)
            customerC.getSelectionModel().select(documentDto.getCustomer());

        if(documentDto.getType().equals(InternalDocumentType.INVOICE))
            fillHelpInfo();
    }

    private void fillHelpInfo() {
        helpInfo.setVisible(true);
        helpName.setText(document.getPerson().getPersonData().getFullNameEng());
        helpUsd.setText(document.getPerson().getPersonData().getUsdAcc().getAccount());
        helpAddr.setText(document.getPerson().getPersonData().getEngAddr().toString());
    }

    @Override
    public InternalDocumentDto createEntity(BasicEntity parent) {
        InternalDocumentDto result = parent.getObject();
        result.setFromDate(new Date());
        result.setState(AgreementState.DRAFT);
        return result;
    }

    private void setUser(UserDto user)
    {
        childUser = user;
        goPerson.setVisible(user != null);
        person.setText(user==null?"":user.getUsername());
    }

    private void setParentDto(InternalDocumentDto parentDto)
    {
        parent=parentDto;
        parentField.setText(parent==null?"":parent.getName()+" : "+parent.getType());
        goParent.setVisible(parent!=null);
    }

    public void onCreateInvoice(ActionEvent actionEvent) {
        InternalDocumentDto newDto = new InternalDocumentDto();
        newDto.setParent(document);
        newDto.setPerson(childUser);
        newDto.setPrice(new PriceDto(0, Currency.UAH));
        newDto.setType(InternalDocumentType.INVOICE);
        eventBus.post(NavigationEvent.createViewView(newDto,document));
    }

    private void processDocType()
    {
        switch (document.getType()) {
            case INVOICE:
                return;
            case RENT_SPACE:
            case RENT_EQUIPMENT:
            case STUDY_AGREEMENT:
                childrenPane.setVisible(false);
                acceptancePane.setVisible(true);
                break;
            case SERVICE_AGREEMENT:
                childrenPane.setVisible(true);
                acceptancePane.setVisible(false);
                break;
            case SERVICE_AGREEMENT_ACCEPTANCE:
                childrenPane.setVisible(false);
                acceptancePane.setVisible(false);
        }
    }

    public void onEditAcceptance(ActionEvent actionEvent) {
        if(document.getAcceptance()==null)
        {
            InternalDocumentDto doc = new InternalDocumentDto();
            doc.setPerson(childUser);
            doc.setParent(parent);
            doc.setType(InternalDocumentType.SERVICE_AGREEMENT_ACCEPTANCE);
            doc.setParentDocForAcceptance(document.getId());
            doc.setPrice(new PriceDto(0, Currency.USD));
            eventBus.post(NavigationEvent.createViewView(doc, document));
        }
        else {
            eventBus.post(NavigationEvent.createViewView(document.getAcceptance(), document));
        }
    }

    public void onGetAcceptance(ActionEvent actionEvent) {

    }

    public void onGetFile(ActionEvent actionEvent)
    {
        FileChooser fileChooser = new FileChooser();
        File f = fileChooser.showSaveDialog(null);

        try {
            download(BaseService.baseUrl+"files/test.pdf",f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onRender(ActionEvent actionEvent)
    {
        FileService fileService = new FileService();
        FileDto result = fileService.render(document.getId());
        if(result!=null)
        {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialFileName(result.getFilename());
            File f = fileChooser.showSaveDialog(null);

            try {
                download(BaseService.baseUrl+"getFile/"+result.getId(),f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static Path download(String sourceURL, File file) throws IOException
    {
        URL url = new URL(sourceURL);
        Path targetPath = file.toPath();
        Files.copy(url.openStream(), targetPath, StandardCopyOption.REPLACE_EXISTING);
        return targetPath;
    }

    public void onGenerate(ActionEvent actionEvent) {
        name.setText(generators.get(document.getType()).generate(DateUtils.asUtilDate(date.getValue()), document.getType()));
    }

    abstract class NameGenerator{abstract String generate(Date date, InternalDocumentType type);}

    private static Map<InternalDocumentType, NameGenerator> generators = new HashMap<>();

    void initGenerators() {
        generators.clear();
        generators.put(InternalDocumentType.SERVICE_AGREEMENT, new NameGenerator(){
            @Override
            String generate(Date date, InternalDocumentType type) {
                SimpleDateFormat format = new SimpleDateFormat("DDMM/YY");
                return "CCIT-"+format.format(date);
            }
        });

        generators.put(InternalDocumentType.INVOICE, new NameGenerator() {
            @Override
            String generate(Date date, InternalDocumentType type) {
                SimpleDateFormat format = new SimpleDateFormat("MM-D/YY");
                return "CCIT-0"+format.format(date);
            }
        });

        NameGenerator defaultGen = new NameGenerator() {
            @Override
            String generate(Date date, InternalDocumentType type) {
                return String.valueOf(date.getTime());
            }
        };

        generators.put(InternalDocumentType.SERVICE_AGREEMENT_ACCEPTANCE, defaultGen);
        generators.put(InternalDocumentType.RENT_EQUIPMENT,defaultGen);
        generators.put(InternalDocumentType.STUDY_AGREEMENT,defaultGen);
        generators.put(InternalDocumentType.RENT_SPACE,defaultGen);
    }
}
