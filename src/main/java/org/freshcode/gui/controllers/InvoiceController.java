package org.freshcode.gui.controllers;

import com.google.common.collect.Lists;
import com.google.common.primitives.Floats;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseButton;
import javafx.util.Pair;
import javafx.util.StringConverter;
import org.filters.CommonFilter;
import org.freshcode.domain.Invoice;
import org.freshcode.domain.enums.Currency;
import org.freshcode.dto.*;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.*;
import org.freshcode.templates.BasicInvoiceTemplate;
import org.freshcode.utils.CommonTransform;
import org.freshcode.utils.Utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Created by user on 28.06.16.
 */
public class InvoiceController extends BaseEntityController <InvoiceDto>{


    public DatePicker date;
    public TextField price;
    public ComboBox<Currency> currency;
    public ComboBox<Invoice.InvoiceState> state;
    public ListView<PaymentDto> payments;
    public Button createPayment;
    public TextField agreement;
    public Button goAgreementB;
    public TableView<InvoiceRowDto> rows;
    public Label templateCurrency;
    public Label rowsTotal;
    public Button addRow;
    public DatePicker fromDate;
    public DatePicker toDate;

    private AgreementDto parentAgreement;
    private InvoiceDto invoice;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        rowsTotal.setText("");
        rows.setEditable(true);
        currency.getItems().addAll(Currency.values());
        state.getItems().addAll(Invoice.InvoiceState.values());
        date.setConverter(new DateTimeConverter());
        fromDate.setConverter(new DateTimeConverter());
        toDate.setConverter(new DateTimeConverter());
        payments.setCellFactory(TextFieldListCell.forListView(new StringConverter<PaymentDto>() {
            @Override
            public String toString(PaymentDto object) {
                return object.getDate().toString() +" "+ object.getPrice().format();
            }

            @Override
            public PaymentDto fromString(String string) {
                return null;
            }
        }));
        payments.setOnMouseClicked(event -> {
            if(event.getClickCount()==2 && event.getButton().equals(MouseButton.PRIMARY))
            {
                if(payments.getSelectionModel().getSelectedItem()!=null)
                {
                    eventBus.post(NavigationEvent.createViewView(payments.getSelectionModel().getSelectedItem(), invoice));
                }
            }
        });

        TableColumn<InvoiceRowDto, String> string = new TableColumn<>("string");
        string.setMinWidth(500);
        string.setCellValueFactory(new PropertyValueFactory<>("item"));

        TableColumn<InvoiceRowDto, Float> value = new TableColumn<>("value");
        value.setCellValueFactory(new PropertyValueFactory<>("amount"));

        rows.getColumns().setAll(Lists.newArrayList(string, value));
        addRow.setOnAction(ev -> rows.getItems().add(new InvoiceRowDto("empty", 0,0f,0f)));
        string.setCellFactory(TextFieldTableCell.forTableColumn());
        value.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<Float>() {
            @Override
            public String toString(Float object) {
                return object.toString();
            }

            @Override
            public Float fromString(String string) {
                return Float.parseFloat(string);
            }
        }));

        string.setOnEditCommit(event -> event.getRowValue().setItem(event.getNewValue()));
        value.setOnEditCommit(event -> {
            event.getRowValue().setAmount(event.getNewValue());
            calculateTotal();
        });

    }

    private void calculateTotal() {
        rowsTotal.setText("Total = "+ rows.getItems().stream()
                .map(InvoiceRowDto::getAmount)
                .reduce((i1, i2) -> i1+i2).orElseGet(() -> new Float(0)).toString());
    }

    private void setAgreement(AgreementDto agreementDto)
    {
        this.parentAgreement=agreementDto;
        if(agreementDto!=null)
        {
            agreement.setText(agreementDto.getName());
            goAgreementB.setVisible(true);
            templateCurrency.setText("["+agreementDto.getInvoiceTemplate().getCurrency()+"]");
        }
        else
        {
            agreement.setText("");
            goAgreementB.setVisible(false);
            templateCurrency.setText("[]");
        }
    }

    public void onCreatePayment(ActionEvent actionEvent) {
        invoice.setPayments(null);
        NavigationEvent event = new NavigationEvent();
        event.setRx(NavigationEvent.Action.VIEW, invoice);
        event.setTx(NavigationEvent.Action.CREATE, Etype.PAYMENT);
        event.getTx().setParenEntity(new BasicEntity(invoice));
        eventBus.post(event);
    }

    public void goAgreement(ActionEvent actionEvent) {
        NavigationEvent event = new NavigationEvent();
        event.setTx(NavigationEvent.Action.VIEW, parentAgreement);
        event.setRx(NavigationEvent.Action.VIEW, invoice);
        eventBus.post(event);
    }

    public InvoiceDto fill()
    {
        invoice.getPrice().setCurrency(currency.getSelectionModel().getSelectedItem());
        invoice.getPrice().setValue(Integer.valueOf(price.getText()));
        invoice.setInvoiceState(state.getSelectionModel().getSelectedItem());
        invoice.setAgreement(parentAgreement);
        invoice.setDate(DateUtils.asUtilDate(date.getValue()));
        invoice.setRows(rows.getItems());
        return invoice;
    }

    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.INVOICE).saveOrUpdate(fill()));
    }

    @Override
    public void setEntity(InvoiceDto invoiceDto) {
        this.invoice = invoiceDto;
        date.setValue(DateUtils.asLocalDate(invoiceDto.getDate()));
        currency.getSelectionModel().select(invoiceDto.getPrice().getCurrency());
        price.setText(invoiceDto.getPrice().getValue().toString());
        state.getSelectionModel().select(invoiceDto.getInvoiceState());
        payments.getItems().clear();
        if(invoiceDto.getPayments()!=null)
        {
            payments.getItems().addAll(invoiceDto.getPayments());
        }
        setAgreement(invoiceDto.getAgreement());
        if(invoiceDto.getRows()!=null)
        {
            rows.getItems().setAll(invoiceDto.getRows());
            calculateTotal();
        }

    }

    @Override
    public InvoiceDto createEntity(BasicEntity parent) {
        InvoiceDto result = new InvoiceDto();
        result.setId(null);
        result.setPrice(new PriceDto(0,Currency.USD));
        result.setInvoiceState(Invoice.InvoiceState.DRAFT);
        result.setNumber("");
        result.setDate(new Date());

        if(parent !=null)
        {
            result.setAgreement(parent.getObject());
            result.setPrice(CommonTransform.transformBean(result.getAgreement().getPrice(), PriceDto.class));
        }
        return result;
    }

    public void onFillPrice(ActionEvent actionEvent) {
        price.setText(
                rows.getItems().stream()
                        .map(InvoiceRowDto::getAmount)
                        .reduce((i1, i2) -> i1+i2).get().toString());
    }

    public void onEditTemplate(ActionEvent actionEvent) {
        eventBus.post(NavigationEvent.createViewView(parentAgreement.getInvoiceTemplate(), invoice));
    }

    public void onRenderInvoice(ActionEvent actionEvent) throws IOException {
        BasicInvoiceTemplate invoiceTemplate = new BasicInvoiceTemplate();
//        invoiceTemplate.render(parentAgreement.getInvoiceTemplate(), invoice);
    }

    public void editTime(ActionEvent actionEvent) {
        NavigationEvent event = new NavigationEvent();
        event.setTx(NavigationEvent.Action.LIST, Etype.LOG_PERIOD);
        event.getTx().setParenEntity(new BasicEntity(parentAgreement));
        Pair<Date, Date> date = new Pair<>(DateUtils.asUtilDate(fromDate.getValue()),DateUtils.asUtilDate(toDate.getValue()));
        event.getTx().getFilters().add(CommonFilter.create(CommonFilter.FilterType.DATE_RANGE,LogPeriodDto.Filters.DATE, date));
        event.setRx(NavigationEvent.Action.VIEW, invoice);
        eventBus.post(event);
    }
}
