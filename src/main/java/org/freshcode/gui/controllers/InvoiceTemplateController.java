package org.freshcode.gui.controllers;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.InvoiceTemplateOption;
import org.freshcode.dto.ClientDto;
import org.freshcode.dto.FinancialInfoDto;
import org.freshcode.dto.InvoiceTemplateDto;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.Etype;
import org.freshcode.gui.utils.ObjectExplorer;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by user on 25.08.16.
 */
public class InvoiceTemplateController extends BaseEntityController<InvoiceTemplateDto> {
    public CheckBox showDueDate;
    public ComboBox<Currency> currency;
    public ComboBox<FinancialInfoDto> ourAccount;
    public TextField client;
    public Button assignClient;
    public ComboBox<ClientDto> ourCompany;
    public CheckBox qtyCheck;
    public CheckBox amountCheck;
    public CheckBox rateCheck;
    public CheckBox headerBlockCheck;
    public TextField headerBlock;

    private ClientDto clientDto;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currency.getItems().addAll(Currency.values());
        ourAccount.setConverter(new StringConverter<FinancialInfoDto>() {
            @Override
            public String toString(FinancialInfoDto o) {
                return o.getName();
            }
            @Override
            public FinancialInfoDto fromString(String string) {
                return null;
            }
        });
        ourCompany.setConverter(new StringConverter<ClientDto>() {
            @Override
            public String toString(ClientDto o) {
                return o.getName();
            }
            @Override
            public ClientDto fromString(String string) {
                return null;
            }
        });
        assignClient.setOnAction(e -> ObjectExplorer.select(Etype.CLIENT,null,entity -> setClient(entity.getObject())));
    }

    private void setClient(ClientDto newClient)
    {
        clientDto = newClient;
        client.setText(clientDto==null?"":clientDto.getName());
    }


    @Override
    public BasicEntity getResult() {
        return null;
    }

    @Override
    public void setEntity(InvoiceTemplateDto invoiceTemplateDto) {
        setClient(invoiceTemplateDto.getClient());
        currency.getSelectionModel().select(invoiceTemplateDto.getCurrency());
        ourCompany.getSelectionModel().select(invoiceTemplateDto.getOurClient());

        String showDD = invoiceTemplateDto.getOptions().get(InvoiceTemplateOption.SHOW_DUE_DATE);
        showDueDate.setSelected(showDD!=null && Boolean.parseBoolean(showDD));

        qtyCheck.setSelected(invoiceTemplateDto.isShowQuantity());
        amountCheck.setSelected(invoiceTemplateDto.isShowAmount());
        rateCheck.setSelected(invoiceTemplateDto.isShowRate());

        headerBlockCheck.setSelected(invoiceTemplateDto.getOptions().get(InvoiceTemplateOption.HEADER_BLOCK)!=null);
        if(headerBlockCheck.isSelected())
        {
            headerBlock.setText(invoiceTemplateDto.getOptions().get(InvoiceTemplateOption.HEADER_BLOCK));
        }
        else
        {
            headerBlock.setText("");
        }
    }

    @Override
    public InvoiceTemplateDto createEntity(BasicEntity parent) {
        return null;
    }
}
