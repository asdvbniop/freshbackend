package org.freshcode.gui.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.*;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.LeadState;
import org.freshcode.dto.*;
import org.freshcode.gui.service.FileService;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.*;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by user on 28.06.16.
 */
public class LeadController extends BaseEntityController<LeadDto>{

    public TextField price;
    public TextField url;
    public ComboBox<LeadState> state;
    public TextField agreement;
    public TextField client;
    public ComboBox<Currency> currency;
    public TextField bid;
    public Button goAgreementB;
    public Button goClientB;
    public Button assignClientB;
    public Button createClientB;
    public Button goBidB;
    public Button createBidB;
    public DatePicker date;
    public Button createAgreementB;
    public Button showFiles;
    public TextField title;

    private LeadDto lead;
    private AgreementDto childAgreement=null;
    private ClientDto childClient =null;
    private BidDto childBid =null;

    FileService fileService = new FileService();
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currency.getItems().addAll(Currency.values());
        state.getItems().addAll(LeadState.values());
        date.setConverter(new DateTimeConverter());
        showFiles.setOnAction(event -> showFiles());
    }

    private void showFiles() {
        NavigationEvent event = new NavigationEvent();
        event.setTx(NavigationEvent.Action.LIST, Etype.FILE);
        event.getTx().setSource(NavigationEvent.Source.PROVIDER);
//        event.getTx().setListProvider(() -> fileService.getFileByLeadId(lead.getId()));
        event.setRx(NavigationEvent.Action.VIEW, lead);
        eventBus.post(event);
    }

    public LeadDto fill() {
        lead.setTitle(title.getText());
        lead.setPrice(new PriceDto(Integer.parseInt(price.getText()),currency.getSelectionModel().getSelectedItem()));
        lead.setDate(DateUtils.asUtilDate(date.getValue()));
        lead.setUrl(url.getText());
        lead.setLeadState(state.getSelectionModel().getSelectedItem());
        lead.setUrl(url.getText());
        lead.setBid(childBid);
        lead.setAgreement(childAgreement);
        lead.setClient(childClient);
        return lead;
    }

    private void setBid(BidDto bidDto)
    {
        childBid= bidDto;
        bid.setText(childBid==null?"":childBid.getPrice().format());
        goBidB.setVisible(childBid!=null);
        createBidB.setVisible(bidDto==null);
    }

    private void setClient(ClientDto clientDto)
    {
        childClient = clientDto;
        client.setText(childClient==null?"":childClient.getName());
        createClientB.setVisible(bid==null);
        goClientB.setVisible(childClient!=null);
        createClientB.setVisible(childClient==null);
    }

    private void setAgreement(AgreementDto agreementDto){
        childAgreement= agreementDto;
        agreement.setText(agreementDto==null?"":agreementDto.getName());
        goAgreementB.setVisible(agreementDto!=null);
        createAgreementB.setVisible(agreementDto==null);
    }

    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.LEAD).saveOrUpdate(fill()));
    }

    @Override
    public void setEntity(LeadDto leadDto) {
        this.lead = leadDto;
        price.setText(leadDto.getPrice().getValue().toString());
        currency.getSelectionModel().select(leadDto.getPrice().getCurrency());
        url.setText(leadDto.getUrl());
        state.getSelectionModel().select(leadDto.getLeadState());
        date.setValue(DateUtils.asLocalDate(leadDto.getDate()));
        title.setText(leadDto.getTitle());
        setBid(leadDto.getBid());
        setClient(leadDto.getClient());
        setAgreement(leadDto.getAgreement());

        if(leadDto.getId()==null)
        {
            createAgreementB.setVisible(false);
            createClientB.setVisible(false);
            createBidB.setVisible(false);
        }
    }

    @Override
    public LeadDto createEntity(BasicEntity parent) {
        LeadDto result = LeadDto.create();
        if(parent !=null)
        {
            switch (parent.getType()) {
                case BID:
                    result.setBid(parent.getObject());
                    break;
                case AGREEMENT:
                    result.setAgreement(parent.getObject());
                    break;
                case CLIENT:
                    result.setClient(parent.getObject());
                    break;
            }
        }
        return result;
    }

    public void assignClient(ActionEvent actionEvent) {
        ObjectExplorer.select(Etype.CLIENT, null, entity -> setClient(entity.getObject()));
    }

    public void createAgreement(ActionEvent actionEvent) {
        NavigationEvent event = new NavigationEvent();
        event.setTx(NavigationEvent.Action.CREATE, Etype.AGREEMENT);
        event.getTx().setParenEntity(new BasicEntity(lead));
        event.setRx(NavigationEvent.Action.VIEW, lead);
        eventBus.post(event);
    }

    public void createBid(ActionEvent actionEvent) {
        NavigationEvent event = new NavigationEvent();
        event.setTx(NavigationEvent.Action.CREATE, Etype.BID);
        event.getTx().setParenEntity(new BasicEntity(lead));
        event.setRx(NavigationEvent.Action.VIEW, lead);
        eventBus.post(event);
    }

    public void createClient(ActionEvent actionEvent) {
        eventBus.post(NavigationEvent.createCreateView(Etype.CLIENT,lead,lead));
    }

    public void goBid(ActionEvent actionEvent) {
        eventBus.post(NavigationEvent.createViewView(childBid, lead));
    }

    public void goClient(ActionEvent actionEvent) {
        eventBus.post(NavigationEvent.createViewView(childClient, lead));
    }

    public void goAgreement(ActionEvent actionEvent) {
        eventBus.post(NavigationEvent.createViewView(childAgreement, lead));
    }
}
