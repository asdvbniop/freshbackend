package org.freshcode.gui.controllers;

import javafx.scene.control.*;
import org.freshcode.domain.LogPeriod;
import org.freshcode.dto.LogItemDto;
import org.freshcode.dto.LogPeriodDto;
import org.freshcode.dto.MemberDto;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.*;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Created by user on 22.08.16.
 */
public class LogItemController extends BaseEntityController<LogItemDto> {

    public DatePicker date;
    public TextField minutes;
    public TextArea comment;
    public TextField jiraUrl;

    private LogItemDto itemDto;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @Override
    public BasicEntity getResult() {
        return null;
    }

    @Override
    public void setEntity(LogItemDto logItemDto) {
        itemDto=logItemDto;
        date.setValue(DateUtils.asLocalDate(logItemDto.getDate()));
        minutes.setText(logItemDto.getMinutes().toString());
        comment.setText(logItemDto.getDescription());
        jiraUrl.setText(logItemDto.getJiraUrl());
    }

    @Override
    public LogItemDto createEntity(BasicEntity parent) {
        return new LogItemDto(0,"", new Date(),"");
    }

}
