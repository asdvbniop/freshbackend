package org.freshcode.gui.controllers;

import javafx.scene.control.*;
import org.freshcode.domain.LogPeriod;
import org.freshcode.dto.AgreementDto;
import org.freshcode.dto.LogPeriodDto;
import org.freshcode.dto.MemberDto;
import org.freshcode.dto.ProjectDto;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.*;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

/**
 * Created by user on 22.08.16.
 */
public class LogPeriodController extends BaseEntityController<LogPeriodDto> {
    public TextField member;
    public ComboBox template;
    public TextField templateValue;
    public ComboBox<LogPeriod.LogPeriodType> periodType;
    public DatePicker fromDate;
    public DatePicker toDate;
    public TextField minutes;
    public Button viewLog;
    public Button addLogItem;
    public Button assignMember;
    public Label fromDateWarn;
    public Label toDateWarn;

    private LogPeriodDto periodDto;
    private MemberDto parent;
    private List<BasicEntity>allowedMembers = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fromDate.setConverter(new DateTimeConverter());
        toDate.setConverter(new DateTimeConverter());
        viewLog.setOnAction(event -> viewLog());
        assignMember.setOnAction(event -> ObjectExplorer.select(allowedMembers, entity -> setParentMember(entity.getObject())));
    }

    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.LOG_PERIOD).saveOrUpdate(fill()));
    }

    private LogPeriodDto fill() {
        periodDto.setFromDate(DateUtils.asUtilDate(fromDate.getValue()));
        periodDto.setToDate(DateUtils.asUtilDate(toDate.getValue()));
        periodDto.setMinutes(Integer.valueOf(minutes.getText()));
        periodDto.setMember(parent);
        return periodDto;
    }

    @Override
    public void setEntity(LogPeriodDto logPeriodDto) {
        periodDto = logPeriodDto;
        assignMember.setVisible(logPeriodDto.getId()==null);

        fromDate.setValue(DateUtils.asLocalDate(logPeriodDto.getFromDate()));
        toDate.setValue(DateUtils.asLocalDate(logPeriodDto.getToDate()));
        periodType.getSelectionModel().select(logPeriodDto.getPeriodType());
        minutes.setText(logPeriodDto.getMinutes().toString());
        setParentMember(logPeriodDto.getMember());
    }

    private void setParentMember(MemberDto memberDto) {
        parent = memberDto;
        member.setText(memberDto==null?"":memberDto.getPerson().getUsername());
        getWarnText(memberDto);
    }

    @Override
    public LogPeriodDto createEntity(BasicEntity parent) {
        LogPeriodDto res = new LogPeriodDto(new Date(), new Date(), LogPeriod.LogPeriodType.MANUAL,60,null,null);
        if(parent!=null)
        {
            if(parent.getType().equals(Etype.MEMBER))
            {
                res.setMember(parent.getObject());
            } else if(parent.getType().equals(Etype.AGREEMENT))
            {
                AgreementDto dto = (AgreementDto) TService.get(Etype.AGREEMENT).getById(parent.getObject().getId());
                allowedMembers = dto.getMembers().stream().map(BasicEntity::new).collect(Collectors.toList());
            }
        }
        return res;
    }

    private void getWarnText(MemberDto dto)
    {
        if(dto!=null)
        {
            List<LogPeriodDto> list = TService.get(Etype.LOG_PERIOD).getAllByParentId(dto.getId());
            if(!list.isEmpty())
            {
                LocalDate lastDate = DateUtils.asLocalDate(list.get(list.size()-1).getToDate()).plusDays(1);
                fromDateWarn.setText(lastDate.format(DateTimeFormatter.ISO_LOCAL_DATE));
                fromDate.setValue(lastDate);
                LocalDate maxDate = lastDate.with(lastDayOfMonth());
                toDateWarn.setText(maxDate.format(DateTimeFormatter.ISO_LOCAL_DATE));
                toDate.setValue(maxDate);
            }
        }
    }


    private void viewLog() {
        NavigationEvent event = new NavigationEvent();
        event.setTx(NavigationEvent.Action.LIST, Etype.LOG_ITEM);
        event.getTx().setParenEntity(new BasicEntity(periodDto));
        event.setRx(NavigationEvent.Action.VIEW, periodDto);
        event.getRx().setParenEntity(new BasicEntity(periodDto));
        eventBus.post(event);
    }
}
