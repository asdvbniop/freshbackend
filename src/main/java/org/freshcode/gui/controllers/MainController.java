package org.freshcode.gui.controllers;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.*;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Created by user on 27.06.16.
 */
public class MainController implements Initializable{

    public StackPane stack;
    public AnchorPane editorContainer;
    public Label infoLabel;
    public Button submit;
    public Button back;

    public AnchorPane mainArea;
    public Label tableInfo;

    private EventBus eventBus = new EventBus();

//    interface NameConverter{String convert(Object o);}
//    Map<Etype, NameConverter> converters = new HashMap<>();

    Map<Etype, BaseEntityController> controllers = new HashMap<>();
    Map<Etype, AnchorPane> views = new HashMap<>();
    List<NavigationEvent> events = new ArrayList<>();
    BasicEntity createdEntity=null;
    Etype currentType;
    TableController tableController;
    AnchorPane tableView;
    AnchorPane massOperations;
    UserMassOperationTableController massOperationController;




    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tableInfo.setText("USER EDITOR");
        eventBus.register(this);
//        converters.put(Etype.USER, o -> Convert.fromUser((UserDto) o));
//        converters.put(Etype.PROJECT, o -> Convert.fromProject((ProjectDto) o));
//        converters.put(Etype.LEAD, o->Convert.fromLead((LeadDto) o));
//        converters.put(Etype.BID, o->Convert.fromBid((BidDto) o));


        loadPage(Etype.USER, "userEditorCommon.fxml");
        loadPage(Etype.PROJECT, "projectEditor.fxml");
        loadPage(Etype.EXPENSE, "expenseEditor.fxml");
        loadPage(Etype.INVOICE, "InvoicesEditor.fxml");
        loadPage(Etype.LEAD, "leadEditor.fxml");
        loadPage(Etype.BID, "bidEditor.fxml");
        loadPage(Etype.PAYMENT, "paymentEditor.fxml");
        loadPage(Etype.AGREEMENT, "agreementEditor.fxml");
        loadPage(Etype.MEMBER, "memberEditor.fxml");
        loadPage(Etype.LOG_PERIOD, "logPeriodEditor.fxml");
        loadPage(Etype.LOG_ITEM, "logItemEditor.fxml");
        loadPage(Etype.CLIENT, "clientEditor.fxml");
        loadPage(Etype.CONTACT_INFO, "contactInfoEditor.fxml");
        loadPage(Etype.INVOICE_TEMPLATE, "InvoiceTemplate.fxml");
        loadPage(Etype.FINANCIAL_INFO, "financialInfoEditor.fxml");
        loadPage(Etype.FILE, "fileEditor.fxml");
        loadPage(Etype.INTERNAL_DOCUMENT, "internalAgreementEditor.fxml");
        loadPage(Etype.BANK,"bankEditor.fxml");
        loadPage(Etype.USER_DATA,"userDataEditor.fxml");

        controllers.values().forEach(c -> c.setEventBus(eventBus));

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("table.fxml"));
            tableView = loader.load();
            tableController = loader.getController();
            tableController.setEventBus(eventBus);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("userTableMassOperation.fxml"));
            massOperations = loader.load();
            massOperationController = loader.getController();
//            massOperationController.setEventBus(eventBus);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        ObjectExplorer e = new ObjectExplorer();
//        e.show();
//        e.fillTable(Etype.PROJECT, null);

    }

    @Subscribe
    public void entityListener(BasicEntity e)
    {
        createdEntity = e;
    }

    @Subscribe
    public void eventsListener(NavigationEvent event)
    {
        if(event.getRx().action!=null)
        {
            events.add(event);
        }
        processEvent(event.getTx());
    }

    public void onBack(ActionEvent actionEvent) {
        if(events.size()==0)
            return;
        NavigationEvent e = events.get(events.size()-1);
        events.remove(events.size()-1);
        processEvent(e.getRx());
    }

    private void processEvent(NavigationEvent.NavigationData data)
    {
        switch (data.action) {
            case CREATE:
            case VIEW:
            case EDIT:
                showEditor(data);
                break;
            case LIST:
                showTable(data);
                break;
        }
    }

    public void showClients(ActionEvent actionEvent) {
        showTable(Etype.CLIENT);
    }

    public void showUsers(ActionEvent actionEvent) {
        showTable(Etype.USER);
    }

    public void showProjects(ActionEvent actionEvent) {
        showTable(Etype.PROJECT);
    }

    public void showLeads(ActionEvent actionEvent) {
        showTable(Etype.LEAD);
    }

    public void showBids(ActionEvent actionEvent) {
        showTable(Etype.BID);
    }

    public void showBench(ActionEvent actionEvent) {
//        loadPage();
    }

    public void showInvoices(ActionEvent actionEvent){
        showTable(Etype.INVOICE);
    }

    public void showBanks(ActionEvent actionEvent) {
        showTable(Etype.BANK);
    }

    public void showPayments(ActionEvent actionEvent) {
        showTable(Etype.PAYMENT);
    }

    public void showExpenses(ActionEvent actionEvent) {
        showTable(Etype.EXPENSE);
    }

    public void showAgreements(ActionEvent actionEvent) {
        showTable(Etype.AGREEMENT);
    }

    private void loadPage(Etype mode, String s) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(s));
            views.put(mode,loader.load());
            controllers.put(mode,loader.getController());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showTable(Etype etype)
    {
        NavigationEvent e = new NavigationEvent();
        e.getTx().setEntity(new BasicEntity(etype));
        showTable(e.getTx());
    }

    private void showTable(NavigationEvent.NavigationData data)
    {
        tableController.fillTable(data);
        stack.getChildren().setAll(tableView);
        currentType = data.getEntity().getType();
    }

    public void onSubmit(ActionEvent actionEvent) {
        BasicEntity e = controllers.get(currentType).getResult();
        onBack(actionEvent);
    }


    private void showEditor(NavigationEvent.NavigationData data)
    {
        BasicEntity e  = data.getEntity();
        Etype type = e.getType();
        currentType = type;


        if(data.action.equals(NavigationEvent.Action.EDIT) || data.action.equals(NavigationEvent.Action.VIEW))
        {
            if(e.getObject().getId()!=null)//new entity
            {
                data.setEntity(new BasicEntity(TService.get(type).getById(e.getObject().getId())));
            }
        }
        controllers.get(type).processEvent(data);

        stack.getChildren().setAll(editorContainer);
        mainArea.setVisible(true);
        mainArea.getChildren().setAll(views.get(type));
    }

    public void showMassOperations(ActionEvent actionEvent) {
        stack.getChildren().setAll(massOperations);
        currentType = Etype.USER;
    }


}