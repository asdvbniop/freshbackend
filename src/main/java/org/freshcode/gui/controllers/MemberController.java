package org.freshcode.gui.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import org.freshcode.domain.enums.AgreementType;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.UserRole;
import org.freshcode.dto.LogPeriodDto;
import org.freshcode.dto.MemberDto;
import org.freshcode.dto.UserDto;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.Etype;
import org.freshcode.gui.utils.NavigationEvent;
import org.freshcode.gui.utils.ObjectExplorer;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by user on 20.08.16.
 */
public class MemberController extends BaseEntityController<MemberDto>{

    public TextField paymentPrice;
    public ComboBox<Currency> paymentCurrency;
    public ComboBox<AgreementType> paymentType;
    public HBox memberPriceHbox;
    public TextField user;
    public Button goUser;
    public Button assignUser;
    public ComboBox<UserRole> memberRole;
    public HBox memberRatio;
    public Slider ratioSlider;
    public TextField ratioInput;
    public Button viewLog;

    private MemberDto member;
    private UserDto childUser;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        paymentCurrency.getItems().addAll(Currency.values());
        memberRole.getItems().addAll(UserRole.values());
        goUser.setOnAction(event -> eventBus.post(NavigationEvent.createViewView(childUser,member)));
        assignUser.setOnAction(event -> ObjectExplorer.select(Etype.USER, null, entity -> setUser(entity.getObject())));
        viewLog.setOnAction(event -> viewLog());
    }


    private MemberDto fill() {
        member.setRole(memberRole.getSelectionModel().getSelectedItem());
        member.setType(paymentType.getSelectionModel().getSelectedItem());
        member.getPrice().setValue(Integer.valueOf(paymentPrice.getText()));
        member.getPrice().setCurrency(paymentCurrency.getSelectionModel().getSelectedItem());
        member.setPerson(childUser);
        return member;
    }

    private void setUser(UserDto person) {
        childUser = person;
        user.setText(person==null?"":person.getUsername());
        goUser.setVisible(person!=null);
    }

    private void viewLog() {
        NavigationEvent event = new NavigationEvent();
        event.setTx(NavigationEvent.Action.LIST, Etype.LOG_PERIOD);
        event.getTx().setParenEntity(new BasicEntity(member));
        event.setRx(NavigationEvent.Action.VIEW, member);
        event.getRx().setParenEntity(new BasicEntity(member));
        eventBus.post(event);
    }

    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.MEMBER).saveOrUpdate(fill()));
    }

    @Override
    public void setEntity(MemberDto dto) {
        member= dto;
        memberRole.getSelectionModel().select(member.getRole());
        paymentType.getSelectionModel().select(member.getType());
        paymentPrice.setText(member.getPrice().getValue().toString());
        paymentCurrency.getSelectionModel().select(member.getPrice().getCurrency());
        ratioInput.setText(String.valueOf(member.getRatio()));
        setUser(dto.getPerson());
    }

    @Override
    public MemberDto createEntity(BasicEntity parent) {
        MemberDto result = new MemberDto(null,0,UserRole.DEVELOPER,AgreementType.FIXED,null, null, null,null);
        if(parent !=null)
        {
            switch (parent.getType()) {
                case PROJECT:
                    result.setProject(parent.getObject());
                    break;
                case AGREEMENT:
                    result.setAgreement(parent.getObject());
                    break;
            }
        }
        return result;
    }


    public void addLog(ActionEvent actionEvent) {
        eventBus.post(NavigationEvent.createCreateView(Etype.LOG_PERIOD, member,member));
    }
}
