package org.freshcode.gui.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.PaymentState;
import org.freshcode.dto.InvoiceDto;
import org.freshcode.dto.PaymentDto;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.*;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Created by user on 31.07.16.
 */
public class PaymentController extends BaseEntityController<PaymentDto>{
    public TextField price;
    public ComboBox<Currency> currency;
    public ComboBox<PaymentState> state;
    public TextField invoice;
    public Button goInvoiceB;
    public Button assignInvoiceB;
    public DatePicker date;

    private PaymentDto payment;
    private InvoiceDto childInvoice;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        state.getItems().addAll(PaymentState.values());
        currency.getItems().addAll(Currency.values());
        date.setConverter(new DateTimeConverter());
    }

    private void setInvoice(InvoiceDto invoiceDto) {
        childInvoice = invoiceDto;
        goInvoiceB.setVisible(childInvoice!=null);
        invoice.setText(childInvoice==null?"":childInvoice.getId().toString());
    }

    public void goInvoice(ActionEvent actionEvent) {
        NavigationEvent ev = new NavigationEvent();
        ev.setTx(NavigationEvent.Action.VIEW, childInvoice);
        ev.setRx(NavigationEvent.Action.VIEW, payment);
        eventBus.post(ev);
    }

    public void assignInvoice(ActionEvent actionEvent) {
        ObjectExplorer.select(Etype.INVOICE, null);
        BasicEntity e = ObjectExplorer.getSelectedEntity();
        if(e.getObject()!=null)
        {
            setInvoice((InvoiceDto) e.getObject());
        }
    }

    private PaymentDto fill()
    {
        payment.getPrice().setValue(Integer.valueOf(price.getText()));
        payment.getPrice().setCurrency(currency.getSelectionModel().getSelectedItem());
        payment.setState(state.getSelectionModel().getSelectedItem());
        payment.setInvoice(childInvoice);
        payment.setDate(DateUtils.asUtilDate(date.getValue()));
        return payment;
    }

    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.PAYMENT).saveOrUpdate(fill()));
    }

    @Override
    public void setEntity(PaymentDto paymentDto) {
        this.payment = paymentDto;
        date.setValue(DateUtils.asLocalDate(paymentDto.getDate()));
        currency.getSelectionModel().select(paymentDto.getPrice().getCurrency());
        price.setText(paymentDto.getPrice().getValue().toString());
        state.getSelectionModel().select(paymentDto.getState());
        setInvoice(paymentDto.getInvoice());
    }

    @Override
    public PaymentDto createEntity(BasicEntity parent) {
        if(parent ==null)
        {
            return new PaymentDto(null,PaymentState.DRAFT, null);
        }
        else
        {
            InvoiceDto parent1 = parent.getObject();
            PaymentDto result = new PaymentDto();
            result.setDate(new Date());
            result.setPrice(parent1.getPrice());
            result.setState(PaymentState.ACCEPT);
            result.setInvoice(parent1);
            return result;
        }
    }
}
