package org.freshcode.gui.controllers;

import com.google.common.collect.Lists;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseButton;
import javafx.util.StringConverter;
import org.freshcode.domain.Project;
import org.freshcode.dto.*;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.Etype;
import org.freshcode.gui.utils.NavigationEvent;
import org.freshcode.gui.utils.ObjectExplorer;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by user on 31.07.16.
 */
public class ProjectController extends BaseEntityController<ProjectDto>{
    public ComboBox<Project.Color> projectColor;
    public TextField state;
    public TextField name;
    public TextField agreementField;
    public Button goAgreementB;
    public ListView<MemberDto> members;
    public Button assignAgreementB;
    public Button createMemberB;

    private ProjectDto project;
    private AgreementDto agreement;

    private ProjectDto fill() {
        project.setName(name.getText());
        project.setColor(projectColor.getSelectionModel().getSelectedItem());
        setAgreement(agreement);
        return project;
    }

    public void setAgreement(AgreementDto agreementDto)
    {
        agreement = agreementDto;
//        agreementField.setText(agreement==null?"":agreement.getDate());
        agreementField.setText(agreement==null?"":agreementDto.getName());
        goAgreementB.setVisible(agreement!=null);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        projectColor.getItems().addAll(Lists.newArrayList(Project.Color.values()));
        members.setCellFactory(TextFieldListCell.forListView(new StringConverter<MemberDto>() {
            @Override
            public String toString(MemberDto object) {
                return object.getPerson().getUsername()+" :: "+object.getRole().toString();
            }

            @Override
            public MemberDto fromString(String string) {
                return null;
            }
        }));
        goAgreementB.setOnAction(ev -> eventBus.post(NavigationEvent.createViewView(agreement, project)));
        assignAgreementB.setOnAction(ev->ObjectExplorer.select(Etype.AGREEMENT, null, entity -> setAgreement(entity.getObject())));
        createMemberB.setOnAction(ev -> eventBus.post(NavigationEvent.createCreateView(Etype.MEMBER, project, project)));

        members.setOnMouseClicked(event -> {
            if(event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount()==2)
            {
                MemberDto item = members.getSelectionModel().getSelectedItem();
                if(item!=null)
                {
                    eventBus.post(NavigationEvent.createViewView(item, project));
                }
            }
        });
    }

    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.PROJECT).saveOrUpdate(fill()));
    }

    @Override
    public void setEntity(ProjectDto projectDto) {
        this.project = projectDto;
        name.setText(projectDto.getName());
        state.setText(projectDto.getState().toString());
        projectColor.getSelectionModel().select(projectDto.getColor());
        setAgreement(projectDto.getAgreement());
        if(projectDto.getMembers()!=null)
            members.getItems().setAll(projectDto.getMembers());
    }

    @Override
    public ProjectDto createEntity(BasicEntity parent) {
        ProjectDto result = ProjectDto.create();
        if(parent !=null && parent.getType().equals(Etype.AGREEMENT))
        {
            result.setAgreement(parent.getObject());
            result.setName(result.getAgreement().getName());
        }
        return result;
    }

}
