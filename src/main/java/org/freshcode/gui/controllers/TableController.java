package org.freshcode.gui.controllers;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.eventbus.EventBus;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import lombok.Getter;
import org.freshcode.domain.enums.FileType;
import org.freshcode.domain.enums.UserRole;
import org.freshcode.dto.*;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.Etype;
import org.freshcode.gui.utils.NavigationEvent;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by user on 09.08.16.
 */
public class TableController implements Initializable {
    public Button createB;
    public AnchorPane actions;
    public TableView table;
    public AnchorPane filters;
    public Label statusLabel;
    public Button refreshB;

    @Getter
    private EventBus eventBus;

    private ArrayListMultimap<Etype, TableColumn> columns = ArrayListMultimap.create();
    private Map<Etype, AnchorPane> filterView = new HashMap<>();
    private Map<Etype, BaseFilterController>filterControllers = new HashMap<>();
    private List<Etype>allowCreate = new ArrayList<>();

    private NavigationEvent.NavigationData lastEvent=null;
    private BaseFilterController currentFilterController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        allowCreate.addAll(Lists.newArrayList(Etype.LEAD, Etype.CLIENT, Etype.LOG_PERIOD, Etype.LOG_ITEM, Etype.BANK));
        refreshB.setOnAction(event -> refresh());
        fillColumns();

//        loadFilter(Etype.PROJECT, "filters/projectFilters.fxml");
//        loadFilter(Etype.INVOICE, "filters/invoiceFilters.fxml");
//        loadFilter(Etype.PAYMENT, "filters/paymentFilters.fxml");
        loadFilter(Etype.LOG_PERIOD, "filters/logPeriod.fxml");
        filterControllers.values().forEach(c -> c.setEventBus(eventBus));

        table.setOnMouseClicked(event -> {
            Object e = table.getSelectionModel().getSelectedItem();
            if(event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount()==2 && e!=null)
            {
                NavigationEvent ev = new NavigationEvent();
                ev.setTx(NavigationEvent.Action.VIEW, e);
                if(lastEvent!=null){
                    ev.setRx(lastEvent);
                }else{
                    ev.setRx(NavigationEvent.Action.LIST, e);
                }
                eventBus.post(ev);
            }
        });
    }

    private void refresh() {
        if(currentFilterController!=null)
            currentFilterController.getFilters();
        fillTable(lastEvent);
    }

    public void fillTable(NavigationEvent.NavigationData data) {
        lastEvent = data;
        Etype mode = data.getEntity().getType();
        Map<Enum,String> filters = null;
        createB.setVisible(allowCreate.contains(mode));

        NavigationEvent createEvent = new NavigationEvent();
        createEvent.setTx(NavigationEvent.Action.CREATE, mode);
        createEvent.getTx().setParenEntity(data.getParenEntity());
        createEvent.setRx(NavigationEvent.Action.LIST, mode);
        createB.setOnAction(event -> eventBus.post(createEvent));

        List results;
        if(data.getSource().equals(NavigationEvent.Source.PROVIDER))
        {
            results = data.getListProvider().get(data.filters);
        }
        else {
            if(data.getParenEntity()==null)
                results = TService.get(mode).getAll();
            else
                results = TService.get(mode).getAllByParentId(data.getParenEntity().getObject().getId());
        }

        if(filterControllers.get(mode)!=null){
            currentFilterController = filterControllers.get(mode);
            filterControllers.get(mode).prepareData(results);
            table.getItems().setAll(filterControllers.get(mode).process(results,filters));
        }
        else {
            table.getItems().setAll(results);
        }
        table.getColumns().setAll(columns.get(mode));
        if(filterView.get(mode)!=null){
            this.filters.getChildren().setAll(filterView.get(mode));
        }
        else{
            this.filters.getChildren().clear();
        }
    }

    private void loadFilter(Etype mode, String s) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(s));
            filterView.put(mode,loader.load());
            Initializable c = loader.getController();
            filterControllers.put(mode,loader.getController());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fillColumns(){
        TableColumn<UserDto, Long> id = new TableColumn<>("id");
        id.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<UserDto, String> username = new TableColumn<>("username");
        username.setCellValueFactory(new PropertyValueFactory<>("username"));

        columns.putAll(Etype.USER, Lists.newArrayList(id, username));
///////////////////////////////////PROJECT///////////////////////////////////

        TableColumn<ProjectDto, String> name = new TableColumn<>("name");
        name.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<ProjectDto, String> projectState = new TableColumn<>("type");
        projectState.setCellValueFactory(new PropertyValueFactory<>("type"));

        TableColumn<ProjectDto, String> projectcolor = new TableColumn<>("color");
        projectcolor.setCellValueFactory(new PropertyValueFactory<>("color"));
        columns.putAll(Etype.PROJECT,Lists.newArrayList(id, name, projectState, projectcolor));
//////////////////////////////////////////INVOICE//////////////////////////
        TableColumn<InvoiceDto, String> price = new TableColumn<>("price");
        price.setCellValueFactory(new PropertyValueFactory("priceString"));

        TableColumn<InvoiceDto, Date> date = new TableColumn<>("date");
        date.setCellValueFactory(new PropertyValueFactory<>("exposedDate"));

        TableColumn<InvoiceDto, String> invoiceState = new TableColumn<>("type");
        invoiceState.setCellValueFactory(new PropertyValueFactory<>("invoiceState"));

        TableColumn<InvoiceDto, String> projectName = new TableColumn<>("agreement name");
        projectName.setCellValueFactory(new PropertyValueFactory<>("agreementName"));
        columns.putAll(Etype.INVOICE, Lists.newArrayList(id, price, date, invoiceState, projectName));

        TableColumn<LeadDto, String> leadStateColumn = new TableColumn<>("type");
        leadStateColumn.setCellValueFactory(new PropertyValueFactory<>("leadState"));
        TableColumn<LeadDto,String> leadTitle = new TableColumn<>("title");
        leadTitle.setCellValueFactory(new PropertyValueFactory<>("title"));
        columns.putAll(Etype.LEAD, Lists.newArrayList(id, price, leadStateColumn, leadTitle));
//
//        TableColumn<BidDto, Long> idColumn = new TableColumn<>("id");
//        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
//        TableColumn<BidDto, String> nameColumn = new TableColumn<>("value");
//        nameColumn.setCellValueFactory(new PropertyValueFactory<>("value"));
        columns.putAll(Etype.BID, Lists.newArrayList(id));

        TableColumn<InvoiceDto, Date> paymentPaidDate = new TableColumn<>("date");
        paymentPaidDate.setCellValueFactory(new PropertyValueFactory<>("paidDate"));
        TableColumn<LeadDto, String> paymentPriceColumn = new TableColumn<>("price");
        paymentPriceColumn.setCellValueFactory(new PropertyValueFactory<>("priceAsString"));
        TableColumn<LeadDto, String> paymentStateColumn = new TableColumn<>("type");
        paymentStateColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        columns.putAll(Etype.PAYMENT, Lists.newArrayList(id, paymentStateColumn, paymentPriceColumn, paymentPaidDate));

        TableColumn<ExpenseDto, Date> expenseDate = new TableColumn<>("date");
        expenseDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        TableColumn<ExpenseDto, String> expensePriceColumn = new TableColumn<>("price");
        expensePriceColumn.setCellValueFactory(new PropertyValueFactory<>("priceAsString"));
        columns.putAll(Etype.EXPENSE, Lists.newArrayList(id, expenseDate, expensePriceColumn));

        TableColumn<AgreementDto, Date> agreementDate = new TableColumn<>("date");
        agreementDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        TableColumn<AgreementDto, String> agreementPriceColumn = new TableColumn<>("price");
        agreementPriceColumn.setCellValueFactory(new PropertyValueFactory<>("priceAsString"));
        TableColumn<AgreementDto, String> namePriceColumn = new TableColumn<>("name");
        namePriceColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        columns.putAll(Etype.AGREEMENT, Lists.newArrayList(id, namePriceColumn, agreementDate, agreementPriceColumn));

        TableColumn<LogPeriodDto, Date> fromDate = new TableColumn<>("from");
        fromDate.setCellValueFactory(new PropertyValueFactory<>("fromDate"));
        TableColumn<LogPeriodDto, Date> toDate = new TableColumn<>("to");
        toDate.setCellValueFactory(new PropertyValueFactory<>("toDate"));
        TableColumn<LogPeriodDto, String>duration = new TableColumn<>("minutes");
        duration.setCellValueFactory(new PropertyValueFactory<>("minutes"));
        TableColumn<LogPeriodDto, String> logPeriodType = new TableColumn<>("type");
        logPeriodType.setCellValueFactory(new PropertyValueFactory<>("periodType"));
        columns.putAll(Etype.LOG_PERIOD, Lists.newArrayList(id, fromDate, toDate, duration, logPeriodType));

        TableColumn<LogItemDto, Date> logItemDate = new TableColumn<>("date");
        logItemDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        columns.putAll(Etype.LOG_ITEM, Lists.newArrayList(id,logItemDate,duration));

        TableColumn<ClientDto, String> clientName = new TableColumn<>("name");
        clientName.setCellValueFactory(new PropertyValueFactory<>("name"));
        columns.putAll(Etype.CLIENT, Lists.newArrayList(id, clientName));

        TableColumn<FileDto, String> filename = new TableColumn<>("filename");
        filename.setCellValueFactory(new PropertyValueFactory<>("filename"));
        TableColumn<FileDto, String>filepath = new TableColumn<>("path");
        filepath.setCellValueFactory(new PropertyValueFactory<>("path"));
        TableColumn<FileDto, FileType>filetype = new TableColumn<>("type");
        filetype.setCellValueFactory(new PropertyValueFactory<>("fileType"));
        columns.putAll(Etype.FILE,Lists.newArrayList(id, filename, filepath, filetype));

        TableColumn<MemberDto, String> usermember = new TableColumn<>("username");
        usermember.setCellValueFactory(new PropertyValueFactory<>("username"));
        TableColumn<MemberDto, UserRole>memberRole = new TableColumn<>("role");
        memberRole.setCellValueFactory(new PropertyValueFactory<>("role"));
        columns.putAll(Etype.MEMBER, Lists.newArrayList(id, usermember, memberRole));

        TableColumn<BankDto,String> bankName = new TableColumn<>("name");
        bankName.setCellValueFactory(new PropertyValueFactory<>("name"));
        columns.putAll(Etype.BANK, Lists.newArrayList(id, bankName));
    }

    public void setEventBus(EventBus eventBus) {
        this.eventBus = eventBus;
        filterControllers.values().forEach(c->c.setEventBus(eventBus));
    }

    public BasicEntity getSelected() {
        return new BasicEntity(table.getSelectionModel().getSelectedItem());
    }

    public void fillTable(List<BasicEntity> source) {
        table.getItems().setAll(source.stream().map(BasicEntity::getObject).collect(Collectors.toList()));
        this.filters.getChildren().clear();
        table.getColumns().setAll(columns.get(source.get(0).getType()));
    }
}
