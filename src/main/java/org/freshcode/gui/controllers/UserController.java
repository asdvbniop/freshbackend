package org.freshcode.gui.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseButton;
import javafx.scene.text.Text;
import javafx.util.StringConverter;
import org.freshcode.domain.PersonFullData;
import org.freshcode.domain.Price;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.InternalDocumentType;
import org.freshcode.domain.enums.UserRole;
import org.freshcode.dto.*;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.Etype;
import org.freshcode.gui.utils.NavigationEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by user on 28.06.16.
 */
public class UserController extends BaseEntityController<UserDto>{

    public TextField nameF;
    public ComboBox<UserRole> roleC;
    public TextField usernameF;
    public ListView<MemberDto> projectList;
    public ListView<InternalDocumentDto> documents;

    public ComboBox<InternalDocumentType> documentType;
    public Label userDataLabel;
    public Button createUserData;
    public Button editUserData;
    public Button deleteUserData;

    private UserDto user;
    private PersonFullDataDto data;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        roleC.getItems().addAll(UserRole.values());
        documentType.getItems().addAll(InternalDocumentType.values());
        documentType.getItems().remove(InternalDocumentType.INVOICE);
        projectList.setCellFactory(TextFieldListCell.forListView(new StringConverter<MemberDto>() {
            @Override
            public String toString(MemberDto object) {
                if(object!=null)
                    if(object.getPerson()!=null)
                        return object.getProject().getName()+"  "+object.getRatio();
                return "object is null";
            }

            @Override
            public MemberDto fromString(String string) {
                return null;
            }
        }));

        documents.setCellFactory(TextFieldListCell.forListView(new StringConverter<InternalDocumentDto>() {
            @Override
            public String toString(InternalDocumentDto doc) {
                return doc.getName()+" "+doc.getType();
            }

            @Override
            public InternalDocumentDto fromString(String string) {
                return null;
            }
        }));

        documents.setOnMouseClicked(event -> {
            if(event.getClickCount()==2 && event.getButton().equals(MouseButton.PRIMARY))
            {
                if(documents.getSelectionModel().getSelectedItem()!=null)
                {
                    eventBus.post(NavigationEvent.createViewView(documents.getSelectionModel().getSelectedItem(), user));
                }
            }
        });
        editUserData.setOnAction(event -> eventBus.post(NavigationEvent.createViewView(data, user)));
        createUserData.setOnAction(event -> eventBus.post(NavigationEvent.createCreateView(Etype.USER_DATA,user,user)));
    }

    public UserDto fillUser(UserDto user)
    {
        user.setUsername(usernameF.getText());
        user.setName(nameF.getText());
//        user.setSalary(Integer.valueOf(rateF.getText()));
        user.setUserRole(roleC.getSelectionModel().getSelectedItem());
        return user;
    }

    @Override
    public BasicEntity getResult() {
        TService<UserDto> service = TService.get(Etype.USER);
        UserDto result;
        if(user.getId()==null)
            result = service.save(fillUser(user));
        else
            result = service.update(fillUser(user));
        return new BasicEntity(result);
    }

    @Override
    public void setEntity(UserDto userDto) {
        this.user = userDto;
        usernameF.setText(userDto.getUsername());
        nameF.setText(userDto.getName());
        roleC.getSelectionModel().select(userDto.getUserRole());
        if (userDto.getMemberList()!=null)
            projectList.getItems().setAll(userDto.getMemberList());

        if(userDto.getId()!=null)
        {
            documents.getItems().setAll(TService.get(Etype.INTERNAL_DOCUMENT).getAllByParentId(userDto.getId()));
        }
        setUserData(user.getPersonData());
        //TODO remove this line and implement removing
        deleteUserData.setVisible(false);
    }

    private void setUserData(PersonFullDataDto userData)
    {
        data = userData;
        userDataLabel.setText(data==null?"null":"exist!");
        editUserData.setVisible(data!=null);
        deleteUserData.setVisible(data!=null);
        createUserData.setVisible(data==null);
    }

    @Override
    public UserDto createEntity(BasicEntity parent) {
        return UserDto.create();
    }

    public void goProject(ActionEvent actionEvent) {
        MemberDto memberDto = projectList.getSelectionModel().getSelectedItem();
        eventBus.post(NavigationEvent.createViewView(memberDto.getProject(),user));
    }

    public void onAddDocument(ActionEvent actionEvent) {
        InternalDocumentDto pass = new InternalDocumentDto();
        pass.setPerson(user);
        pass.setPrice(new PriceDto(0, Currency.UAH));
        pass.setType(documentType.getSelectionModel().getSelectedItem());
        eventBus.post(NavigationEvent.createCreateView(Etype.INTERNAL_DOCUMENT,pass,user));
    }
}
