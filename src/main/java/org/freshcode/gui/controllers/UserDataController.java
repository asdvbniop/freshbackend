package org.freshcode.gui.controllers;

import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import org.freshcode.domain.FinancialInfo;
import org.freshcode.domain.enums.*;
import org.freshcode.domain.enums.Currency;
import org.freshcode.dto.*;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.*;
import org.freshcode.utils.Utils;

import java.net.URL;
import java.util.*;

/**
 * Created by user on 09.08.16.
 */
public class UserDataController extends BaseEntityController<PersonFullDataDto> {

    public TextField nameEng;
    public TextField nameRus;
    public TextField phone;
    public TextField taxNumber;
    public TextField password;
    public DatePicker birthday;
    public TextField usdAcc;
    public Button createUsd;
    public Button editUsd;
    public TextField eurAcc;
    public Button createEur;
    public Button editEur;
    public TextField uahAcc;
    public Button createUah;
    public Button editUah;
    public AnchorPane addrEditor;
    public TextField nameUa;
    public Button transliterate;

    private PersonFullDataDto data;
    private BiAddrController addrController;

    private Map<Currency,FinancialInfoDto> info = new HashMap<>();
    private Map<Currency,TextField> labels = new HashMap<>();
    private Map<Currency,Button> createInfo = new HashMap<>();
    private Map<Currency,Button> editInfo = new HashMap<>();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addrController = new BiAddrController().load(addrEditor);
        birthday.setConverter(new DateTimeConverter());
        labels.put(Currency.EUR,eurAcc);
        labels.put(Currency.UAH,uahAcc);
        labels.put(Currency.USD,usdAcc);

        createInfo.put(Currency.EUR,createEur);
        createInfo.put(Currency.UAH,createUah);
        createInfo.put(Currency.USD,createUsd);

        editInfo.put(Currency.EUR,editEur);
        editInfo.put(Currency.UAH,editUah);
        editInfo.put(Currency.USD,editUsd);

        editEur.setOnAction(e -> eventBus.post(NavigationEvent.createViewView(prepareDto(info.get(Currency.EUR)), data)));
        editUah.setOnAction(e -> eventBus.post(NavigationEvent.createViewView(prepareDto(info.get(Currency.UAH)), data)));
        editUsd.setOnAction(e -> eventBus.post(NavigationEvent.createViewView(prepareDto(info.get(Currency.USD)), data)));

        createEur.setOnAction(e -> eventBus.post(NavigationEvent.createViewView(prepareNewDto(Currency.EUR),data)));
        createUsd.setOnAction(e -> eventBus.post(NavigationEvent.createViewView(prepareNewDto(Currency.USD),data)));
        createUah.setOnAction(e -> eventBus.post(NavigationEvent.createViewView(prepareNewDto(Currency.UAH),data)));
        transliterate.setOnAction(e -> nameEng.setText(Utils.tru.transliterate(nameRus.getText())));
    }

    private FinancialInfoDto prepareNewDto(Currency currency)
    {
        FinancialInfoDto result = new FinancialInfoDto();
        result.setCurrency(currency);
        result.setParentId(data.getId());
        result.setParentType(EntityType.USER_DATA);
        return result;
    }

    private FinancialInfoDto prepareDto(FinancialInfoDto dto)
    {
        if(dto!=null)
        {
            dto.setParentType(EntityType.USER_DATA);
        }
        return dto;
    }

    @Override
    public BasicEntity getResult() {
        return new BasicEntity(TService.get(Etype.USER_DATA).saveOrUpdate(fill()));
    }

    private AbstractDto fill() {
        data.setFullNameEng(nameEng.getText());
        data.setFullNameRus(nameRus.getText());
        data.setFullNameUa(nameUa.getText());
        data.setPhone(phone.getText());
        data.setPassword(password.getText());
        data.setTaxNumber(taxNumber.getText());
        data.setBirthday(DateUtils.asUtilDate(birthday.getValue()));
        data.setEngAddr(addrController.getData().getEng());
        data.setRusAddr(addrController.getData().getRus());
        data.setUaAddr(addrController.getData().getUa());
        return data;
    }

    @Override
    public void setEntity(PersonFullDataDto personFullDataDto) {
        data=personFullDataDto;
        nameEng.setText(data.getFullNameEng());
        nameRus.setText(data.getFullNameRus());
        nameUa.setText(data.getFullNameUa());
        phone.setText(data.getPhone());
        password.setText(data.getPassword());
        taxNumber.setText(data.getTaxNumber());
        birthday.setValue(DateUtils.asLocalDate(data.getBirthday()));
        addrController.setAddr(new BiAddrController.BiAddrData(data.getEngAddr(),data.getRusAddr(), data.getUaAddr()));
        info.put(Currency.USD,data.getUsdAcc());
        info.put(Currency.EUR,data.getEurAcc());
        info.put(Currency.UAH,data.getUahAcc());
        processAccounts();
    }

    private void processAccounts() {
        for (Currency currency : labels.keySet()) {
            FinancialInfoDto i = info.get(currency);
            if(i==null)
            {
                labels.get(currency).setText("null");
                editInfo.get(currency).setVisible(false);
                createInfo.get(currency).setVisible(true);
            }else
            {
                labels.get(currency).setText(i.getAccount());
                editInfo.get(currency).setVisible(true);
                createInfo.get(currency).setVisible(false);
            }
        }
    }


    @Override
    public PersonFullDataDto createEntity(BasicEntity parent) {
        if(parent.getType().equals(Etype.USER))
        {
            PersonFullDataDto data = new PersonFullDataDto();
            data.setUser(parent.getObject());
            data.setRusAddr(new FullAddressDataDto());
            data.setEngAddr(new FullAddressDataDto());
            return data;
        }
        return null;
    }
}
