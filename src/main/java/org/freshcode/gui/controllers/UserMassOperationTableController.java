package org.freshcode.gui.controllers;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.eventbus.EventBus;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import lombok.Getter;
import org.freshcode.domain.enums.FileType;
import org.freshcode.domain.enums.UserRole;
import org.freshcode.dto.*;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.Etype;
import org.freshcode.gui.utils.NavigationEvent;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by user on 09.08.16.
 */
public class UserMassOperationTableController implements Initializable {

    public AnchorPane filters;
    public AnchorPane actions;
    public Button refreshB;
    public Button createB;
    public Button onSync;
    public TableView<UserDto> table;
    public Label statusLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TableColumn<UserDto, Long> id = new TableColumn<>("id");
        id.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<UserDto, String> username = new TableColumn<>("username");
        username.setCellValueFactory(new PropertyValueFactory<>("username"));
        table.getColumns().addAll(id,username);

        table.getItems().setAll(TService.get(Etype.USER).getAll());
    }
}
