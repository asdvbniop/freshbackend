package org.freshcode.gui.controllers.filters;

import com.google.common.collect.Lists;
import javafx.scene.control.ComboBox;
import javafx.util.StringConverter;
import org.freshcode.domain.Invoice;
import org.freshcode.dto.InvoiceDto;
import org.freshcode.dto.ProjectDto;
import org.freshcode.gui.controllers.BaseFilterController;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.Etype;
import org.freshcode.gui.utils.NavigationEvent;
import org.freshcode.utils.Utils;

import java.net.URL;
import java.util.*;

/**
 * Created by user on 06.08.16.
 */
public class InvoiceFilter extends BaseFilterController<InvoiceDto, InvoiceDto.Filters> {
    public ComboBox<ProjectDto> agreement;

    public ComboBox<String> invoiceState;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        getPredicates().put(InvoiceDto.Filters.STATE, (invoice, value) -> invoice.getInvoiceState().equals(Invoice.InvoiceState.valueOf(value)));
        getPredicates().put(InvoiceDto.Filters.FROM_DATE, (invoice, value) -> invoice.getDate().before(new Date(Long.valueOf(value))));
        getPredicates().put(InvoiceDto.Filters.TO_DATE, (invoice, value) -> invoice.getDate().after(new Date(Long.valueOf(value))));

        invoiceState.getItems().add("ALL");
        invoiceState.getItems().addAll(Utils.mapEnumToString(Invoice.InvoiceState.class));
        ProjectDto all = new ProjectDto();all.setName("ALL");all.setId(-1L);
        agreement.getItems().add(all);
        invoiceState.getSelectionModel().selectFirst();
        agreement.getSelectionModel().selectFirst();

        Lists.newArrayList(Invoice.InvoiceState.values()).forEach(i -> invoiceState.getItems().add(i.toString()));

        TService<ProjectDto> projects = TService.get(Etype.PROJECT);
        agreement.getItems().addAll(projects.getAll());
        agreement.setConverter(new StringConverter<ProjectDto>() {
            @Override
            public String toString(ProjectDto object) {
               return object.getName();
            }

            @Override
            public ProjectDto fromString(String string) {
                return null;
            }
        });

        agreement.getSelectionModel().selectedItemProperty().addListener((o, old, n) -> processFilters());
        invoiceState.getSelectionModel().selectedItemProperty().addListener((o, old, n) -> processFilters());
    }

    private void processFilters() {
        Long pr = agreement.getSelectionModel().getSelectedItem().getId();
        String state = invoiceState.getSelectionModel().getSelectedItem();
        NavigationEvent e = new NavigationEvent();
        e.setTx(NavigationEvent.Action.LIST, Etype.INVOICE);
//        e.getTx().getFilters().put(InvoiceDto.Filters.STATE, state);
//        e.getTx().getFilters().put(InvoiceDto.Filters.AGREEMENT, pr==null?"ALL":pr.toString());
        eventBus.post(e);
    }
}
