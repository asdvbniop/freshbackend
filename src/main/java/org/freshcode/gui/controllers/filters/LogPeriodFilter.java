package org.freshcode.gui.controllers.filters;

import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;
import javafx.util.Pair;
import javafx.util.StringConverter;
import org.filters.CommonFilter;
import org.freshcode.domain.LogPeriod;
import org.freshcode.dto.AgreementDto;
import org.freshcode.dto.LogPeriodDto;
import org.freshcode.dto.MemberDto;
import org.freshcode.gui.controllers.BaseFilterController;
import org.freshcode.gui.utils.DateTimeConverter;
import org.freshcode.gui.utils.DateUtils;
import org.w3c.dom.stylesheets.LinkStyle;

import java.net.URL;
import java.time.LocalDate;
import java.time.Month;
import java.util.*;

/**
 * Created by user on 22.08.16.
 */
public class LogPeriodFilter extends BaseFilterController<LogPeriodDto, LogPeriodDto.Filters> {
    public ComboBox<MemberDto> member;
    public DatePicker fromDate;
    public DatePicker toDate;
    public ComboBox<Integer> year;
    public ComboBox<Month> month;
    public Tab rangeTab;
    public Tab simpleTab;

    private List<CommonFilter> filters = new ArrayList<>();
    private Map<Long, MemberDto> members = new HashMap<>();
    private Map<Long, AgreementDto> agreements = new HashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        month.getItems().addAll(Month.values());
        year.getItems().add(2016);
        fromDate.setConverter(new DateTimeConverter());
        toDate.setConverter(new DateTimeConverter());
        member.getItems().setAll(new MemberDto(null));

        member.setConverter(new StringConverter<MemberDto>() {
            @Override
            public String toString(MemberDto object) {
                return object==null||object.getId()==null?"ALL":object.getPerson().getUsername();
            }

            @Override
            public MemberDto fromString(String string) {
                return null;
            }
        });
        year.getSelectionModel().selectFirst();
        month.getSelectionModel().select(LocalDate.now().getMonth());
        member.getSelectionModel().selectFirst();
    }

    public void setFilters(List<CommonFilter> filters)
    {
        for (CommonFilter filter : filters) {
            if(filter.getEnumName().equals(LogPeriodDto.Filters.DATE))
            {
                Pair<Date,Date> range = (Pair<Date, Date>) filter.getValue();
                fromDate.setValue(DateUtils.asLocalDate(range.getKey()));
                toDate.setValue(DateUtils.asLocalDate(range.getValue()));
            }
            else if (filter.getEnumName().equals(LogPeriodDto.Filters.MEMBER_ID))
            {
                Long memberId = (Long) filter.getValue();
                member.getSelectionModel().select(members.get(memberId));
            }
        }
    }

    @Override
    public List<CommonFilter> getFilters() {
        filters.clear();
        MemberDto selectedMember = member.getSelectionModel().getSelectedItem();
        if(selectedMember!=null && selectedMember.getId()!=null)
        {
            filters.add(CommonFilter.create(
                    CommonFilter.FilterType.SINGLE_LONG,LogPeriodDto.Filters.MEMBER_ID,selectedMember.getPerson().getId()));
        }
        if(rangeTab.isSelected())
        {
            Pair<Date,Date> range = new Pair<>(DateUtils.asUtilDate(fromDate.getValue()),DateUtils.asUtilDate(toDate.getValue()));
            filters.add(CommonFilter.create(
                    CommonFilter.FilterType.DATE_RANGE, LogPeriodDto.Filters.DATE, range
            ));
        }
        if(simpleTab.isSelected())
        {
            LocalDate from = LocalDate.of(year.getSelectionModel().getSelectedItem(), month.getSelectionModel().getSelectedItem(),1);
            LocalDate to = from.plusMonths(1).minusDays(1);
            Pair<Date,Date> range = new Pair<>(DateUtils.asUtilDate(from),DateUtils.asUtilDate(to));
            filters.add(CommonFilter.create(
                    CommonFilter.FilterType.DATE_RANGE, LogPeriodDto.Filters.DATE, range
            ));
        }
        return filters;
    }

    @Override
    public void prepareData(List<LogPeriodDto> results) {
        for (LogPeriodDto result : results) {
            members.put(result.getMember().getId(), result.getMember());
            if(result.getMember().getAgreement()!=null)
            {
                agreements.put(result.getMember().getAgreement().getId(), result.getMember().getAgreement());
            }
        }

        member.getItems().add(new MemberDto(null));
        member.getItems().setAll(members.values());
        member.getSelectionModel().selectFirst();
    }
}
