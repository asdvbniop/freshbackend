package org.freshcode.gui.controllers.filters;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import javafx.scene.control.ComboBox;
import javafx.util.StringConverter;
import org.freshcode.domain.Invoice;
import org.freshcode.domain.Payment;
import org.freshcode.domain.enums.PaymentState;
import org.freshcode.dto.InvoiceDto;
import org.freshcode.dto.PaymentDto;
import org.freshcode.dto.ProjectDto;
import org.freshcode.gui.controllers.BaseFilterController;
import org.freshcode.gui.service.TService;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.Etype;
import org.freshcode.gui.utils.NavigationEvent;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by user on 06.08.16.
 */
public class PaymentFilter extends BaseFilterController {
    public ComboBox<ProjectDto> project;

    public ComboBox<String> paymentState;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        paymentState.getItems().add("ALL");
        ProjectDto all = new ProjectDto();all.setName("ALL");all.setId(-1L);
        project.getItems().add(all);

        paymentState.getSelectionModel().selectFirst();
        project.getSelectionModel().selectFirst();

        Lists.newArrayList(PaymentState.values()).forEach(i -> paymentState.getItems().add(i.toString()));

        TService<ProjectDto> projects = TService.get(Etype.PROJECT);
        project.getItems().addAll(projects.getAll());
        project.setConverter(new StringConverter<ProjectDto>() {
            @Override
            public String toString(ProjectDto object) {
               return object.getName();
            }

            @Override
            public ProjectDto fromString(String string) {
                return null;
            }
        });

        project.getSelectionModel().selectedItemProperty().addListener((o, old, n) -> processFilters());
        paymentState.getSelectionModel().selectedItemProperty().addListener((o, old, n) -> processFilters());
    }

    private void processFilters() {
        Long pr = project.getSelectionModel().getSelectedItem().getId();
        String state = paymentState.getSelectionModel().getSelectedItem();
        NavigationEvent e = new NavigationEvent();
        e.setTx(NavigationEvent.Action.LIST, Etype.PAYMENT);
//        e.getTx().getFilters().put("paymentState", state);
//        if(pr!=null && pr!=-1)
//        {
//            e.getTx().getFilters().put("agreement", pr.toString());
//        }
        eventBus.post(e);
    }

//    @Override
//    public List process(List all, Map<String, Object> filters) {
//        if(filters==null)
//            return all;
//        Object projectId = filters.get("agreement");
//
//        String state = (String) filters.get("paymentState");
////
//        if(projectId instanceof String)
//        {
//            all.removeIf(o -> !((PaymentDto)o).getProject().getId().equals(Long.valueOf((String) projectId)));
//            agreement.getSelectionModel().select(
//                    agreement.getItems().stream().filter(p->p.getId().equals(Long.valueOf((String) projectId))).findFirst().get());
//        }
//        if(!Strings.isNullOrEmpty(state)&&!state.equals("ALL"))
//        {
//            all.removeIf(o->!((PaymentDto)o).getState().equals(PaymentState.valueOf(state)));
//        }
//        return all;
//    }
}
