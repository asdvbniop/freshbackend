package org.freshcode.gui.controllers.filters;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import javafx.scene.control.ComboBox;
import org.freshcode.domain.Project;
import org.freshcode.dto.ProjectDto;
import org.freshcode.gui.controllers.BaseFilterController;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.Etype;
import org.freshcode.gui.utils.NavigationEvent;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Predicate;

/**
 * Created by user on 06.08.16.
 */
public class ProjectFilter extends BaseFilterController<ProjectDto, ProjectDto.Filters> {
    public ComboBox<String> color;
    public ComboBox<String> projectState;

    private void init(){
        getPredicates().put(ProjectDto.Filters.COLOR, (project, value) -> project.getColor().equals(Project.Color.valueOf(value)));
        getPredicates().put(ProjectDto.Filters.STATE, (project, value) -> project.getState().equals(Project.State.valueOf(value)));
        getPredicates().put(ProjectDto.Filters.NAME,(project, value) -> project.getName().contains(value));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        init();
        projectState.getItems().add("ALL");
        color.getItems().add("ALL");
        Lists.newArrayList(Project.State.values()).forEach(s -> projectState.getItems().add(s.toString()));
        Lists.newArrayList(Project.Color.values()).forEach(c -> color.getItems().add(c.toString()));

        projectState.getSelectionModel().selectFirst();
        color.getSelectionModel().selectFirst();

        //set handler after any changes, event bus here is null
        color.getSelectionModel().selectedItemProperty().addListener((ob, old, n) -> filterTable());
        projectState.getSelectionModel().selectedItemProperty().addListener((o, old, n) -> filterTable());
    }

    private void filterTable() {
        String colorString = color.getSelectionModel().getSelectedItem();
        String state = projectState.getSelectionModel().getSelectedItem();

        NavigationEvent event = new NavigationEvent();
        event.setTx(NavigationEvent.Action.LIST, Etype.PROJECT);
//        event.getTx().getFilters().put(ProjectDto.Filters.COLOR, colorString);
//        event.getTx().getFilters().put(ProjectDto.Filters.STATE, state);
        eventBus.post(event);
    }
}
