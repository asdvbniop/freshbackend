package org.freshcode.gui.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import org.freshcode.dto.UserDto;
import org.freshcode.gui.utils.BasicEntity;

import java.io.IOException;

/**
 * Created by user on 29.07.16.
 */
public class BaseService {

//    public static String baseUrl = "http://94.176.236.0:8080/";
    public static String baseUrl = "http://127.0.0.1:8080/";
    protected com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();

    protected HttpRequestWithBody post(String method)
    {
        return Unirest.post(baseUrl+method)
                .header("accept", "application/json")
                .header("Content-Type", "application/json");
    }

    protected GetRequest get(String method)
    {
        return Unirest.get(baseUrl+method);
    }

    protected HttpRequestWithBody patch(String method)
    {
        return Unirest.patch(baseUrl+method)
                .header("accept", "application/json")
                .header("Content-Type", "application/json");
    }

    protected HttpRequestWithBody delete(String method) {
        return Unirest.delete(baseUrl+method)
                .header("accept", "application/json")
                .header("Content-Type", "application/json");
    }

//    public static <T> T transformBean(final Object source, final Class<T> destination) {
//        return source == null ? null : DozerBeanMapperSingletonWrapper.getInstance().map(source, destination);
//    }

    public <T> T getAll(Long id, Class<T> dest)
    {
        try {
            return get("user/"+id).asObject(dest).getBody();
        } catch (UnirestException e) {
            return null;
        }
    }


public static void init()
{
    Unirest.setObjectMapper(new ObjectMapper() {
        private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                = new com.fasterxml.jackson.databind.ObjectMapper();

        public <T> T readValue(String value, Class<T> valueType) {
            try {
                return jacksonObjectMapper.readValue(value, valueType);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        public String writeValue(Object value) {
            try {
                return jacksonObjectMapper.writeValueAsString(value);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
    });
}


}
