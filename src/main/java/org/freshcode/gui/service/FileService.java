package org.freshcode.gui.service;

import org.freshcode.dto.FileDto;

import java.util.List;

/**
 * Created by user on 28.08.16.
 */
public class FileService extends TService<FileDto> {

    public FileService() {
        super(FileDto.class, "file");
    }

    public List<FileDto> getFileByLeadId(Long leadId)
    {
        return getAllByParentId(leadId,"filesForLead");
    }

    public FileDto render(Long docId)
    {
        return getById(docId,"internalDocument/render");
    }
}
