package org.freshcode.gui.service;

import org.freshcode.dto.FileDto;
import org.freshcode.dto.LogPeriodDto;

import java.util.List;

/**
 * Created by user on 28.08.16.
 */
public class LogPeriodService extends TService<LogPeriodDto> {

    public LogPeriodService() {super(LogPeriodDto.class, "logPeriod");
    }

    public List<LogPeriodDto> getLogPeriodsByAgreement(Long id)
    {
        return getAllByParentId(id,"logPeriodsByAgreement");
    }

    public List<LogPeriodDto> getLogPeriodsByProject(Long id){
        return getAllByParentId(id,"logPeriodsByProject");
    }

}
