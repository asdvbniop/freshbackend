package org.freshcode.gui.service;

import org.filters.CommonFilter;

import java.util.List;

/**
 * Created by user on 05.09.16.
 */
public interface SourceListProvider {
//    public List get();
    public List get(List<CommonFilter> filters);
}
