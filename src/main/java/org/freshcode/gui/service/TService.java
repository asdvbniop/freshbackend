package org.freshcode.gui.service;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.filters.CommonFilter;
import org.freshcode.domain.LogItem;
import org.freshcode.domain.Person;
import org.freshcode.dto.*;
import org.freshcode.gui.utils.BasicEntity;
import org.freshcode.gui.utils.Etype;

import java.io.IOException;
import java.util.*;

/**
 * Created by user on 04.07.16.
 */
public class TService<T extends AbstractDto> extends BaseService {
    private Class clazz;
    private String path;

    private static Map<Etype, TService> services = new HashMap<>();
    private List<CommonFilter>filters = null;

    public static void initServices() {
        services.put(Etype.USER, new TService<>(UserDto.class, "user"));
        services.put(Etype.PROJECT, new TService<>(ProjectDto.class, "project"));
        services.put(Etype.INVOICE, new TService<>(InvoiceDto.class, "invoice"));
        services.put(Etype.LEAD, new TService<>(LeadDto.class, "lead"));
        services.put(Etype.BID, new TService<>(BidDto.class, "bid"));
        services.put(Etype.PAYMENT, new TService<>(PaymentDto.class, "payment"));
        services.put(Etype.EXPENSE, new TService<>(ExpenseDto.class, "expense"));
        services.put(Etype.AGREEMENT, new TService<>(AgreementDto.class, "agreement"));
        services.put(Etype.MEMBER, new TService<>(MemberDto.class, "member"));
        services.put(Etype.LOG_PERIOD, new TService<>(LogPeriodDto.class, "logPeriod"));
        services.put(Etype.LOG_ITEM, new TService<>(LogItemDto.class, "logItem"));
        services.put(Etype.CLIENT, new TService<>(ClientDto.class, "client"));
        services.put(Etype.INVOICE_TEMPLATE, new TService<>(InvoiceTemplateDto.class, "invoiceTemplate"));
        services.put(Etype.CONTACT_INFO, new TService<>(ContactInfoDto.class, "contactInfo"));
        services.put(Etype.FINANCIAL_INFO, new TService<>(FinancialInfoDto.class, "financialInfo"));
        services.put(Etype.INTERNAL_DOCUMENT, new TService<>(InternalDocumentDto.class, "internalDocument"));
        services.put(Etype.BANK, new TService<>(BankDto.class, "bank"));
        services.put(Etype.USER_DATA, new TService<>(PersonFullDataDto.class, "userData"));
        services.put(Etype.FILE, new FileService());
    }

    public static TService get(Etype etype) {
        return services.get(etype);
    }

    protected TService(Class<T> target, String path) {
        clazz = target;
        this.path = path;
    }

    public List<T> getAll() {
        try {
            String body = get(path + "s").asString().getBody();
            JavaType type = mapper.getTypeFactory().constructCollectionType(ArrayList.class, clazz);
            return mapper.readValue(body, type);
        } catch (UnirestException | IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public List<T> getAllByParentId(Long id) {
        try {
            String body = get(path + "s" + "/" + id.toString()).asString().getBody();
            JavaType type = mapper.getTypeFactory().constructCollectionType(ArrayList.class, clazz);
            return mapper.readValue(body, type);
        } catch (UnirestException | IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public T getById(Long id) {
        try {
            JavaType type = mapper.getTypeFactory().constructType(clazz);
            String body = get(path + "/" + id).asString().getBody();
            return mapper.readValue(body, type);
        } catch (UnirestException e) {
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public T save(T t) {
        try {
            JavaType type = mapper.getTypeFactory().constructType(t.getClass());
            String body = post(path).body(t).asString().getBody();
            return mapper.readValue(body, type);
        } catch (UnirestException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public T update(T t) {
        try {
            JavaType type = mapper.getTypeFactory().constructType(t.getClass());
            String body = patch(path).body(t).asString().getBody();
            return mapper.readValue(body, type);
        } catch (UnirestException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public T saveOrUpdate(T t) {
        if (t.getId() == null)
            return save(t);
        else
            return update(t);
    }

    public void delete(Long id) {
        try {
            JavaType type = mapper.getTypeFactory().constructType(clazz);
            String body = delete(path + "/" + id).asString().getBody();
            return;
        } catch (UnirestException e) {
            return;
        }
    }

    public List<T> getAllByParentId(Long id, String path)
    {
        try {
            String body = get(path+"/"+id.toString()).asString().getBody();
            JavaType type = mapper.getTypeFactory().constructCollectionType(ArrayList.class, clazz);
            return mapper.readValue(body, type);
        } catch (UnirestException |IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    protected T getById(Long id, String path)
    {
        try {
            JavaType type = mapper.getTypeFactory().constructType(clazz);
            String body = get(path+"/"+id.toString()).asString().getBody();
            return mapper.readValue(body, type);
        } catch (UnirestException |IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public TService withFilters(List<CommonFilter> filters) {
        this.filters=filters;
        return this;
    }
}
