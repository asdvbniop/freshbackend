package org.freshcode.gui.utils;

import lombok.Getter;
import lombok.Setter;
import org.freshcode.dto.*;

import java.util.HashMap;

/**
 * Created by user on 01.08.16.
 */

public class BasicEntity {
    private static HashMap<Class, Etype> mapping = new HashMap<>();

    public BasicEntity(Object o) {
        object = o instanceof BasicEntity?((BasicEntity)o).getObject(): (AbstractDto) o;
        type=mapping.get(object.getClass());
    }

    public BasicEntity(AbstractDto n) {
        object=n;
        type=mapping.get(object.getClass());
    }

    public BasicEntity(Etype etype) {
        type=etype;
    }

    public static void initMap()
    {
        mapping.put(UserDto.class, Etype.USER);
        mapping.put(ProjectDto.class, Etype.PROJECT);
        mapping.put(InvoiceDto.class, Etype.INVOICE);
        mapping.put(LeadDto.class, Etype.LEAD);
        mapping.put(BidDto.class, Etype.BID);
        mapping.put(MemberDto.class, Etype.MEMBER);
        mapping.put(PaymentDto.class, Etype.PAYMENT);
        mapping.put(ExpenseDto.class, Etype.EXPENSE);
        mapping.put(AgreementDto.class, Etype.AGREEMENT);
        mapping.put(InternalDocumentDto.class, Etype.INTERNAL_DOCUMENT);
        mapping.put(LogPeriodDto.class, Etype.LOG_PERIOD);
        mapping.put(LogItemDto.class, Etype.LOG_ITEM);
        mapping.put(InvoiceTemplateDto.class, Etype.INVOICE_TEMPLATE);
        mapping.put(ClientDto.class, Etype.CLIENT);
        mapping.put(ContactInfoDto.class, Etype.CONTACT_INFO);
        mapping.put(FinancialInfoDto.class, Etype.FINANCIAL_INFO);
        mapping.put(FileDto.class, Etype.FILE);
        mapping.put(BankDto.class, Etype.BANK);
        mapping.put(PersonFullDataDto.class, Etype.USER_DATA);
    }

    @Setter
    protected AbstractDto object=null;

    @Getter
    @Setter
    protected Etype type;

    public <T extends AbstractDto> T getObject() {
        return (T)object;
    }
}
