package org.freshcode.gui.utils;

import javafx.application.HostServices;
import org.freshcode.domain.Bid;
import org.freshcode.domain.Lead;
import org.freshcode.domain.Person;
import org.freshcode.domain.Project;
import org.freshcode.dto.BidDto;
import org.freshcode.dto.LeadDto;
import org.freshcode.dto.ProjectDto;
import org.freshcode.dto.UserDto;

/**
 * Created by user on 01.08.16.
 */
public class Convert {
    public static HostServices services;

    public static String fromUser(UserDto person)
    {
        return person.getName();
    }

    public static String fromProject(ProjectDto project)
    {
        return project.getName();
    }

    public static String fromBid(BidDto bid)
    {
        return bid.getPrice().format();
    }

    public static String fromLead(LeadDto lead)
    {
        return lead.getPrice().format();
    }
}
