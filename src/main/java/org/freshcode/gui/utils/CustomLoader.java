package org.freshcode.gui.utils;


import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.Getter;

import java.io.IOException;


public class CustomLoader {
    @Getter
    private Object controller;
    protected Stage stage = new Stage();
    @Getter
    private Parent view;

    public void load(String url, String title) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(url));
            view = loader.load();
            stage.setTitle(title);
            stage.setScene(new Scene(view));
            controller = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void init() {
//        stage.setOnCloseRequest(r -> controller.onClose(null));
        stage.show();
    }

    public void initModal() {
//        stage.setOnCloseRequest(r -> controller.onClose(null));
        stage.showAndWait();
    }

}
