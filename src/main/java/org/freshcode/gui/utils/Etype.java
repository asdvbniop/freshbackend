package org.freshcode.gui.utils;

/**
 * Created by user on 01.08.16.
 */
public enum Etype{
        NONE,
        BID,
        BANK,
        LEAD,
        PROJECT,
        USER,
        MEMBER,
        INVOICE,
        INVOICE_TEMPLATE,
        PAYMENT,
        CLIENT,
        EXPENSE,
        AGREEMENT,
        INTERNAL_DOCUMENT,
        LOG_PERIOD,
        LOG_ITEM,
        CONTACT_INFO,
        FINANCIAL_INFO,
        USER_DATA,
        FILE
}
