package org.freshcode.gui.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.filters.CommonFilter;
import org.freshcode.dto.AbstractDto;
import org.freshcode.gui.service.SourceFn;
import org.freshcode.gui.service.SourceListProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 01.08.16.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class NavigationEvent {
    public enum Action{
        CREATE,
        VIEW,
        EDIT,
        LIST
    }

    public enum Source{
        PROVIDER,
        REQUIRED
    }

    @Data
    public class NavigationData
    {
        public BasicEntity entity;
        public Action action;
        public List<CommonFilter> filters = new ArrayList<>();
        public BasicEntity parenEntity;
        public Source source = Source.REQUIRED;
        public SourceListProvider listProvider;
        public SourceFn.SourceItemProvider itemProvider;
    }

    NavigationData tx = new NavigationData();
    NavigationData rx = new NavigationData();


    public void setTx(Action txAction, Object entity)
    {
        tx.action = txAction;
        tx.entity = new BasicEntity(entity);
    }

    public void setTx(Action txAction, Etype etype)
    {
        tx.action = txAction;
        tx.entity = new BasicEntity(etype);
    }

    public void setRx(Action rxAction, Object entity)
    {
        rx.action = rxAction;
        rx.entity = new BasicEntity(entity);
    }

    public void setRx(Action rxAction, Etype etype)
    {
        rx.action = rxAction;
        rx.entity = new BasicEntity(etype);
    }

    public static NavigationEvent createViewView(Object tx, Object rx)
    {
        NavigationEvent event = new NavigationEvent();
        event.setTx(Action.VIEW, tx);
        event.setRx(Action.VIEW, rx);
        return event;
    }

    public static NavigationEvent createCreateView(Etype txType, AbstractDto parent, Object rx)
    {
        NavigationEvent event = new NavigationEvent();
        event.setTx(Action.CREATE, txType);
        event.getTx().setParenEntity(new BasicEntity(parent));
        event.setRx(Action.VIEW, rx);
        return event;
    }

    public static NavigationEvent createCreateList(Etype txType, AbstractDto parent, Etype rx)
    {
        NavigationEvent event = new NavigationEvent();
        event.setTx(Action.CREATE, txType);
        if (parent!=null)
        {
            event.getTx().setParenEntity(new BasicEntity(parent));
        }
        event.setRx(Action.LIST, rx);
        return event;
    }
}
