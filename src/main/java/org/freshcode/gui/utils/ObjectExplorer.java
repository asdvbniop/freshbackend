package org.freshcode.gui.utils;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.Getter;
import org.freshcode.domain.enums.EntityType;
import org.freshcode.gui.controllers.TableController;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by user on 09.08.16.
 */
public class ObjectExplorer {

    private static ObjectExplorer self = new ObjectExplorer();

    private ObjectExplorerController controller;
    private TableController tableController;
    private Stage stage = new Stage();

    public ObjectExplorer()
    {
        load();
    }

    public void load()
    {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("objectExplorer.fxml"));
            stage.setTitle("Object Explorer");
            stage.setScene(new Scene(loader.load()));
            controller = loader.getController();
            tableController = controller.getTableController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void fillTable(Etype mode, Map<Enum, String> filters)
    {
        NavigationEvent e = new NavigationEvent();
        e.getTx().setEntity(new BasicEntity(mode));
//        e.getTx().setFilters(filters);
        tableController.fillTable(e.getTx());
    }

    public void show()
    {
        stage.showAndWait();
    }

    public static void select(Etype type, Map<Enum, String> filters)
    {
        self.fillTable(type, filters);
        self.show();
    }

    public interface ResultHandler
    {
        public void process(BasicEntity entity);
    }

    public static void select(Etype type, Map<Enum, String> filters, ResultHandler handler)
    {
        self.fillTable(type, filters);
        self.show();
        if(getSelectedEntity()!=null)
        {
            handler.process(getSelectedEntity());
        }
    }

    public static void select(List<BasicEntity> source, ResultHandler handler)
    {
        self.fillTable(source);
        self.show();
        if(getSelectedEntity()!=null)
        {
            handler.process(getSelectedEntity());
        }
    }

    private void fillTable(List<BasicEntity> source) {
        tableController.fillTable(source);
    }

    public static BasicEntity getSelectedEntity()
    {
        return self.controller.getSelectedEntity();
    }

}
