package org.freshcode.gui.utils;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lombok.Getter;
import org.freshcode.gui.controllers.TableController;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by user on 09.08.16.
 */
public class ObjectExplorerController implements Initializable {

    public AnchorPane tableArea;

    @Getter
    private TableController tableController;
    private AnchorPane tableView;
    private EventBus eventBus = new EventBus();

    @Getter
    private BasicEntity selectedEntity=null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            eventBus.register(this);
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("table.fxml"));
            tableView = loader.load();
            tableArea.getChildren().setAll(tableView);
            AnchorPane.setBottomAnchor(tableView,0.0);
            AnchorPane.setTopAnchor(tableView,0.0);
            AnchorPane.setLeftAnchor(tableView,0.0);
            AnchorPane.setRightAnchor(tableView,0.0);
            tableController = loader.getController();
            tableController.setEventBus(eventBus);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void eventsListener(NavigationEvent event)
    {
        if(event.getTx().action.equals(NavigationEvent.Action.LIST))
        {
//            tableController.fillTable(event.getTx().getEntity().getType(), event.getTx().getFilters());
        }
    }

    public void onClose(ActionEvent actionEvent) {
        ((Stage) ((Node) actionEvent.getSource()).getScene().getWindow()).close();
    }


    public void onSelect(ActionEvent actionEvent) {
        selectedEntity = new BasicEntity(tableController.getSelected());
        onClose(actionEvent);
    }
}
