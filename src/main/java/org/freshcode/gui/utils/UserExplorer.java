package org.freshcode.gui.utils;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import lombok.Getter;
import org.freshcode.domain.Person;
import org.freshcode.dto.UserDto;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * Created by user on 30.07.16.
 */
public class UserExplorer implements Initializable {
    public ListView<UserDto> userList;
    public TextField searchString;
    public ComboBox roleFilter;

    @Getter
    private UserDto selectedUser;

    private List<UserDto> users=new ArrayList<>();
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        userList.setCellFactory(TextFieldListCell.forListView(new StringConverter<UserDto>() {
            @Override
            public String toString(UserDto object) {
                return object.getName();
            }

            @Override
            public UserDto fromString(String string) {
                return null;
            }
        }));
        userList.setOnMouseClicked(this::onDoubleClick);


//        users=service.getAllUsers();
        userList.getItems().setAll(users);
    }

    public void onCancel(ActionEvent actionEvent) {
        if (actionEvent != null) {
            ((Stage) ((Node) actionEvent.getSource()).getScene().getWindow()).close();
        }
    }

    public void onSelect(ActionEvent actionEvent) {
        selectedUser = userList.getSelectionModel().getSelectedItem();
        onCancel(actionEvent);
    }

    private void onDoubleClick(MouseEvent event)
    {
        if(event.getClickCount()==2 && event.getButton().equals(MouseButton.PRIMARY))
        {
            onSelect(null);
            ((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
        }
    }

    public void excludeUsers(List<Person> exusers) {
//        users = service.getAllUsers().stream()
//                .filter(p -> !exusers.contains(p)).collect(Collectors.toList());
//        userList.getItems().setAll(users);
    }
}
