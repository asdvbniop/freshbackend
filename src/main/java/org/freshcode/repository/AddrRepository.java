package org.freshcode.repository;

import org.freshcode.domain.Bank;
import org.freshcode.domain.FullAddressData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by user on 05.07.16.
 */
@Repository
public interface AddrRepository extends JpaRepository<FullAddressData, Long>, JpaSpecificationExecutor<FullAddressData> {
    List<FullAddressData> findByActiveTrue();
}
