package org.freshcode.repository;

import org.freshcode.domain.Agreement;
import org.freshcode.domain.Bid;
import org.freshcode.dto.AgreementDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by user on 05.07.16.
 */
@Repository
public interface AgreementRepository extends JpaRepository<Agreement, Long>, JpaSpecificationExecutor<Agreement> {
    List<Agreement> findByActiveTrue();
}
