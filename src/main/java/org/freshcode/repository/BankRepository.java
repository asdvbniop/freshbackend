package org.freshcode.repository;

import org.freshcode.domain.Bank;
import org.freshcode.domain.Bid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by user on 05.07.16.
 */
@Repository
public interface BankRepository extends JpaRepository<Bank, Long>, JpaSpecificationExecutor<Bank> {
    List<Bank> findByActiveTrue();
    Bank findBankBySwift(String swift);
}
