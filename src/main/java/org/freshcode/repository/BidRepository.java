package org.freshcode.repository;

import org.freshcode.domain.Bid;
import org.freshcode.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by user on 05.07.16.
 */
@Repository
public interface BidRepository extends JpaRepository<Bid, Long>, JpaSpecificationExecutor<Bid> {
    List<Bid> findByActiveTrue();
}
