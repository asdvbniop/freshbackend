package org.freshcode.repository;

import org.freshcode.domain.InternalDocument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends JpaRepository<InternalDocument, Long>, JpaSpecificationExecutor<InternalDocument> {
    public List<InternalDocument> findByActiveTrue();
    public List<InternalDocument> findByPersonIdAndType(Long userId, String type);
    public List<InternalDocument> findByPersonId(Long userId);
}