package org.freshcode.repository;

import org.freshcode.domain.ContactInfo;
import org.freshcode.domain.FinancialInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by user on 05.07.16.
 */
@Repository
public interface FinancialInfoRepository extends JpaRepository<FinancialInfo, Long>, JpaSpecificationExecutor<FinancialInfo> {
    List<FinancialInfo> findByActiveTrue();
}
