package org.freshcode.repository;

import org.freshcode.domain.InvoiceTemplate;
import org.freshcode.domain.Payment;
import org.freshcode.dto.PaymentDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by user on 05.07.16.
 */
@Repository
public interface InvoiceTemplateRepository extends JpaRepository<InvoiceTemplate, Long>, JpaSpecificationExecutor<InvoiceTemplate> {
    List<InvoiceTemplate> findByActiveTrue();
}
