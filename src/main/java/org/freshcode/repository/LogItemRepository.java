package org.freshcode.repository;

import org.freshcode.domain.LogItem;
import org.freshcode.domain.LogPeriod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by user on 05.07.16.
 */
@Repository
public interface LogItemRepository extends JpaRepository<LogItem, Long>, JpaSpecificationExecutor<LogItem> {
    List<LogItem> findByActiveTrue();
}
