package org.freshcode.sevice;

import lombok.extern.slf4j.Slf4j;
import org.freshcode.domain.FileModel;
import org.freshcode.repository.FileRepository;
import org.freshcode.sevice.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

/**
 * Created by user on 20.10.16.
 */
@Service
@Transactional
@Slf4j
public class FileService {

    @Autowired
    FileRepository fileRepository;

    @Autowired
    StorageService storageService;

    public FileModel saveFile(File file, FileModel fileModel)
    {
        storageService.store(file);
        return fileRepository.save(fileModel);
    }

    public File getFile(Long id)
    {
        return new File("null");
    }
}
