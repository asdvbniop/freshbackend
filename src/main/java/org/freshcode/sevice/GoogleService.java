package org.freshcode.sevice;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.ibm.icu.text.Transliterator;
import com.sun.javafx.binding.StringFormatter;
import lombok.extern.slf4j.Slf4j;
import org.freshcode.domain.*;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.Language;
import org.freshcode.domain.enums.UserRole;
import org.freshcode.gui.utils.DateUtils;
import org.freshcode.repository.*;
import org.freshcode.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by user on 09.04.17.
 */
@Service
@Transactional
@Slf4j
public class GoogleService {

    @Autowired
    PersonRepository personRepository;
    @Autowired
    BankRepository bankRepository;
    @Autowired
    UserDataRepository userDataRepository;
    @Autowired
    FinancialInfoRepository financialInfoRepository;
    @Autowired
    AddrRepository addrRepository;
    @Autowired
    DocumentRepository documentRepository;

    private static final String APPLICATION_NAME =
            "Google Sheets API Java Quickstart";

    /** Directory to store user credentials for this application. */
    private static final java.io.File DATA_STORE_DIR = new java.io.File(
            System.getProperty("user.home"), ".credentials/sheets.googleapis.com-java-quickstart");

    /** Global instance of the {@link FileDataStoreFactory}. */
    private static FileDataStoreFactory DATA_STORE_FACTORY;

    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY =
            JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    private static HttpTransport HTTP_TRANSPORT;

    /** Global instance of the scopes required by this quickstart.
     *
     * If modifying these scopes, delete your previously saved credentials
     * at ~/.credentials/sheets.googleapis.com-java-quickstart
     */
    private static final List<String> SCOPES =
            Arrays.asList(SheetsScopes.SPREADSHEETS);

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Creates an authorized Credential object.
     * @return an authorized Credential object.
     * @throws IOException
     */
    public static Credential authorize() throws IOException {
        // Load client secrets.
        InputStream in =
                GoogleService.class.getResourceAsStream("/client_secret.json");
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                        .setDataStoreFactory(DATA_STORE_FACTORY)
                        .setAccessType("offline")
                        .build();
        Credential credential = new AuthorizationCodeInstalledApp(
                flow, new LocalServerReceiver()).authorize("kostya");
        System.out.println(
                "Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
        return credential;
    }

    /**
     * Build and return an authorized Sheets API client service.
     * @return an authorized Sheets API client service
     * @throws IOException
     */
    public static Sheets getSheetsService() throws IOException {
        Credential credential = authorize();
        return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    public enum Fields{
        ID,NAME,PTN,UAH,EUR,USD,PHONE
    }

    public static void main(String[] args) throws IOException {

        Sheets service = getSheetsService();
//        String spreadsheetId = "1Rx6lPKaJX1Zu1LPxxt0Bg10FbjA_tvXmmxrxKgBWx0o";
        String spreadsheetId = "1V9haNcyXCkCk3ILcFsTt3WGp8oz4OVfP6NEraHCHgSs";



        List<List<Object>> values = new ArrayList<>();
        values.add(new ArrayList<>());
        values.get(0).add("0");
        values.get(0).add("1");
        values.get(0).add("2");

        ValueRange val = new ValueRange();
        val.setRange("A1:C1");
        val.setValues(values);

        service.spreadsheets().values().update(spreadsheetId,"A1:C1",val).setValueInputOption("RAW").execute();

    }

    private String getData(List<Object> data, Fields f)
    {
        if(f.ordinal()<data.size())
            return String.valueOf(data.get(f.ordinal()));
        else
            return "";
    }



    public void sync() throws IOException {
        Sheets service = getSheetsService();
        String spreadsheetId = "1Rx6lPKaJX1Zu1LPxxt0Bg10FbjA_tvXmmxrxKgBWx0o";
        ValueRange response = service.spreadsheets().values().get(spreadsheetId, "A2:G31").execute();

        List<List<Object>> values = response.getValues();
        for (List<Object> data : values) {
            Person person = personRepository.findOne(Long.valueOf(getData(data, Fields.ID)));
            syncUser(person, data);
        }

    }


    public void syncUser(Person person, List<Object> data)
    {
        person.setName(getData(data,Fields.NAME).replace("ФОП ",""));
        person.setUsername(Utils.trua.transliterate(getData(data,Fields.NAME)).split(" ")[1]);
        person.setUserRole(UserRole.DEVELOPER);
        PersonFullData fullData;
        if(person.getPersonData()==null)
        {
            fullData = userDataRepository.save(new PersonFullData());
            fullData.setEngAddr(addrRepository.save(new FullAddressData()));
            fullData.setRusAddr(addrRepository.save(new FullAddressData()));
            fullData.setUaAddr(addrRepository.save(new FullAddressData()));
            person.setPersonData(fullData);
            personRepository.save(person);
        }
        else
        {
            fullData=person.getPersonData();
        }

        fullData.setFullNameRus(getData(data,Fields.NAME).replace("ФОП ",""));
        fullData.setFullNameUa(getData(data,Fields.NAME).replace("ФОП ",""));
        fullData.setPhone(getData(data,Fields.PHONE));
        fullData.setTaxNumber(getData(data,Fields.PTN));
        fullData.setEurAcc(createOrUpdateAccount(data,Currency.EUR,fullData.getEurAcc()));
        fullData.setUsdAcc(createOrUpdateAccount(data,Currency.USD,fullData.getUsdAcc()));
        fullData.setUahAcc(createOrUpdateAccount(data,Currency.UAH,fullData.getUahAcc()));
        userDataRepository.save(fullData);
        personRepository.save(person);
    }

    private FinancialInfo createOrUpdateAccount(List data, Currency currency, FinancialInfo source)
    {
        Bank bank = bankRepository.findBankBySwift("AVALUAXXX");
        if(source==null)
            source = new FinancialInfo();
        source.setName(currency.toString());
        source.setAccount(getData(data,Fields.valueOf(currency.toString())));
        source.setBank(bank);
        return financialInfoRepository.save(source);
    }

    public void printTable(List<Long> invoiceId) throws IOException {
        Sheets service = getSheetsService();
        List<List<Object>> values = new ArrayList<>();
        for (Long aLong : invoiceId) {
            InternalDocument doc = documentRepository.getOne(invoiceId.get(0));
            dumpDoc(doc,values);
        }

        String spreadsheetId = "1V9haNcyXCkCk3ILcFsTt3WGp8oz4OVfP6NEraHCHgSs";
        ValueRange val = new ValueRange();
        val.setRange("A2:D50");
        val.setValues(values);
        service.spreadsheets().values().update(spreadsheetId,"A2:D50",val).setValueInputOption("RAW").execute();
    }

    private void dumpDoc(InternalDocument doc, List<List<Object>> data) {
        List<Object> result = new ArrayList<>();
        //1 get name
        result.add(doc.getPerson().getPersonData().getFullNameEng());
        //2 get acc
        result.add(doc.getPerson().getPersonData().getUsdAcc().getAccount());
        //3 create dest like Payment for Invoice #CCIT-004-6/17 from 6.04.2017, Service agreement #CCIT-1401/17
        String iName = doc.getName();
        String aName = doc.getParent().getName();
        String iDate = DateUtils.getDateShort(doc.getDate());
        String aDate = DateUtils.getDateShort(doc.getParent().getDate());
        result.add(String.format("Payment for Invoice #%s from %s , Service agreement #%s from %s",iName,iDate,aName,aDate));
        //4 add addr
        result.add(doc.getPerson().getPersonData().getEngAddr().asString(Language.ENG));
        data.add(result);
    }
}
