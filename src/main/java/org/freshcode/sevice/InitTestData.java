package org.freshcode.sevice;

import com.google.common.collect.Lists;
import org.freshcode.domain.*;
import org.freshcode.domain.enums.*;
import org.freshcode.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 04.08.16.
 */
@Service
@Transactional
public class InitTestData {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    LeadRepository leadRepository;
    @Autowired
    BidRepository bidRepository;
    @Autowired
    ExpenseRepository expenseRepository;
    @Autowired
    AgreementRepository agreementRepository;
    @Autowired
    InvoiceRepository invoiceRepository;
    @Autowired
    MemberRepository memberRepository;
    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    LogPeriodRepository logPeriodRepository;
    @Autowired
    LogItemRepository logItemRepository;
    @Autowired
    InvoiceTemplateRepository invoiceTemplateRepository;
    @Autowired
    ContactInfoRepository contactInfoRepository;
    @Autowired
    FinancialInfoRepository financialInfoRepository;
    @Autowired
    FileRepository fileRepository;
    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    BankRepository bankRepository;
    @Autowired
    AddrRepository addrRepository;
    @Autowired
    UserDataRepository userDataRepository;

    @Autowired
    GoogleService googleService;

    void putData()
    {
        FullAddressData data1 = addrRepository.save(new FullAddressData("Aval","69002","Ukraine","Kiev","","","15"));
        FullAddressData data2 = addrRepository.save(new FullAddressData("Аваль","69002","Украина","Киев","","","15"));
        Bank bank1 = new Bank("Aval","AVALUAXXX","65656","skwskw",data1,data2,data2);
        bankRepository.save(bank1);

        for(int q=0;q<30;q++)
        {
            personRepository.save(new Person());
        }
        try {
            googleService.sync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void putSomeData()
    {
        Person p1 = new Person("user1",null,"First User", new Price(2,Currency.UAH), UserRole.DEVELOPER, null, null, null,null);
        Person p2 = new Person("user2",null,"Second User", new Price(2,Currency.UAH), UserRole.DESIGNER, null, null, null,null);
        Person p3 = new Person("user3",null,"Third User", new Price(2,Currency.UAH), UserRole.DEVELOPER, null, null, null,null);
        Person p4 = new Person("user4",null,"Fourth User", new Price(2,Currency.UAH), UserRole.DEVELOPER,null, null, null,null);
        Person p5 = new Person("user5",null,"Fifth User", new Price(2,Currency.UAH), UserRole.DEVELOPER,null, null, null,null);
        Person p6 = new Person("user6",null,"Sixth User", new Price(2,Currency.UAH), UserRole.DESIGNER, null, null, null,null);
        Person p7 = new Person("user7",null,"Seventh User", new Price(2,Currency.UAH), UserRole.PROJECT_MANAGER, null, null, null,null);
        Person p8 = new Person("user8",null,"Eights User", new Price(2,Currency.UAH), UserRole.PROJECT_MANAGER, null, null, null,null);
        List<Person> l = Lists.newArrayList(p1, p2, p3, p4, p5, p7, p8, p6);
        l.forEach(person -> person.setActive(true));
        l = personRepository.save(l);

        FullAddressData data1 = addrRepository.save(new FullAddressData("Aval","69002","Ukraine","Kiev","","","15"));
        FullAddressData data2 = addrRepository.save(new FullAddressData("Аваль","69002","Украина","Киев","","","15"));
        Bank bank1 = new Bank("Aval","AVALUAXXX","65656","skwskw",data1,data2,data2);
        bankRepository.save(bank1);

        FullAddressData data3 = addrRepository.save(new FullAddressData("Aval","69002","Ukraine","Kiev","","","20"));
        FullAddressData data4 = addrRepository.save(new FullAddressData("Аваль","69002","Украина","Киев","","","20"));

        FinancialInfo fi1 = financialInfoRepository.save(new FinancialInfo("EUR","20065656648", Currency.EUR,bank1));
        FinancialInfo fi2 = financialInfoRepository.save(new FinancialInfo("UAH","20024897512", Currency.UAH,bank1));
        FinancialInfo fi3 = financialInfoRepository.save(new FinancialInfo("USD","20036987512", Currency.USD,bank1));

        PersonFullData data = new PersonFullData("Bodnya","Бодня","Бодня", new Date(),"063-408-34-80","256898888","67^%$f4$",fi1,fi2,fi3,data3,data4,data4);
        data = userDataRepository.save(data);
        p1.setPersonData(data);
        personRepository.save(p1);



        /////
        InternalDocument d1 = new InternalDocument("test111",new Date(),new Date(),new Date(),new Price(455,Currency.USD),AgreementState.ACTIVE,InternalDocumentType.INVOICE,"template0",p1,null,null,null,null,null);
        InternalDocument d2 = new InternalDocument("test222",new Date(),new Date(),new Date(),new Price(455,Currency.USD),AgreementState.ACTIVE,InternalDocumentType.INVOICE,"template0",p1,null,null,null,null,null);
        documentRepository.save(d1);
        documentRepository.save(d2);
        ////

        Client c1 = clientRepository.save(new Client("customer1","Denmark", null, null));;
        Client c2 = clientRepository.save(new Client("customer2", "LA", null, null));;

        Lead lead = new Lead("Lead1", "---", LeadState.SENT,null,c1,null,null,null);
        lead.setPrice(new Price(8000, Currency.USD));
        lead = leadRepository.save(lead);
        Bid bid = bidRepository.save(new Bid(new Price(9000, Currency.USD), "", new Date(), Bid.BidState.SENT, "4w", "", lead));
        lead.setBid(bid);
        leadRepository.save(lead);

        Agreement ag = new Agreement("Educadio", new Date(),AgreementState.ACTIVE, lead, new Price(7000, Currency.USD), AgreementType.FIXED);

        Member m3=new Member(l.get(3),50,UserRole.DEVELOPER, new Price(100, Currency.EUR), AgreementType.FIXED);
        Member m4=new Member(l.get(4),50,UserRole.DEVELOPER, new Price(25,Currency.UAH), AgreementType.PER_HOUR);
        ag.setMembers(memberRepository.save(Lists.newArrayList(m3, m4)));
        agreementRepository.save(ag);

        Invoice i1=invoiceRepository.save(new Invoice(ag,"01-03-2016",new Date(),new Price(2000, Currency.EUR), Invoice.InvoiceState.DRAFT, null));
        Payment py1 = new Payment(new Date(), new Price(500, Currency.UAH), i1, PaymentState.ACCEPT);
        Payment py2 = new Payment(new Date(), new Price(500, Currency.USD), i1,PaymentState.REFUNDED);
        i1.setPayments(paymentRepository.save(Lists.newArrayList(py1,py2)));


        Invoice i2=invoiceRepository.save(new Invoice(ag,"01-04-2016",new Date(),new Price(2000, Currency.USD), Invoice.InvoiceState.CLOSED,null));
        Payment py3 = new Payment(new Date(), new Price(2000, Currency.USD), i2, PaymentState.ACCEPT);
        i2.setPayments(paymentRepository.save(Lists.newArrayList(py3)));


        Invoice i3=invoiceRepository.save(new Invoice(ag,"01-05-2016",new Date(),new Price(2800, Currency.USD), Invoice.InvoiceState.PENDING,null));
        Payment py4 = new Payment(new Date(), new Price(2000, Currency.USD), i3, PaymentState.SENT);
        i3.setPayments(paymentRepository.save(Lists.newArrayList(py4)));
        ag.setInvoices(invoiceRepository.save(Lists.newArrayList(i1,i2,i3)));

        i1=ag.getInvoices().get(0);
        i1.setRows(new ArrayList<>());
        i1.getRows().add(new InvoiceRow("test",1,2f, 60f));
        i1.getRows().add(new InvoiceRow("test2",1,2f, 30f));

        Expense e1 = new Expense(new Price(800, Currency.USD), new Date(),"code review", ExpenseType.PROJECT,null, ag);
        Expense e2 = new Expense(new Price(100, Currency.USD), new Date(),"auto testing", ExpenseType.PROJECT,null, ag);
        Expense e3 = new Expense(new Price(500, Currency.UAH), new Date(),"deploy", ExpenseType.PROJECT,null, null);
        expenseRepository.save(Lists.newArrayList(e1,e2, e3));

        ag.setExpenses(expenseRepository.findByActiveTrue().subList(0,1));

        InvoiceTemplate template  = new InvoiceTemplate();
        template.setCurrency(Currency.USD);
        template.setClient(c1);
        template.setOurClient(c2);
        template.getOptions().put(InvoiceTemplateOption.SHOW_DUE_DATE, "true");
        template.getOptions().put(InvoiceTemplateOption.HEADER_BLOCK, "Java programming");
        template = invoiceTemplateRepository.save(template);
        agreementRepository.save(ag);
        ag.setInvoiceTemplate(template);
        agreementRepository.save(ag);

        Project pr1 = new Project("Educadio", Project.Color.GREEN, Project.State.DRAFT,ag,null,null);
        projectRepository.save(pr1);
        ag.setProject(pr1);
        agreementRepository.save(ag);
        Member m1=new Member(l.get(0),50,UserRole.DEVELOPER, new Price(0,Currency.UAH), AgreementType.FIXED);
        Member m2=new Member(l.get(1),10,UserRole.DEVELOPER, new Price(0,Currency.UAH),AgreementType.FIXED);
        pr1.setMembers(memberRepository.save(Lists.newArrayList(m1,m2)));
        projectRepository.save(pr1);

        lead.setAgreement(ag);
        lead.setBid(bid);
        lead.setClient(c1);
        leadRepository.save(lead);

        LogPeriod lp1 = new LogPeriod(new Date(), new Date(), LogPeriod.LogPeriodType.MANUAL, 60, ag.getMembers().get(0),null);
        LogPeriod lp2 = new LogPeriod(new Date(), new Date(), LogPeriod.LogPeriodType.MANUAL, 80, ag.getMembers().get(0),null);
        LogPeriod lp3 = new LogPeriod(new Date(), new Date(), LogPeriod.LogPeriodType.MANUAL, 100,ag.getMembers().get(0),null);

        ag.getMembers().get(0).setLog(logPeriodRepository.save(Lists.newArrayList(lp1,lp2,lp3)));
        memberRepository.save(ag.getMembers().get(0));

        LogItem li1 = logItemRepository.save(new LogItem(60,"fix issues 619", new Date(),""));
        LogItem li2 = logItemRepository.save(new LogItem(60,"fix issues 619", new Date(new Date().getTime()-24*60*60*1000),""));

        LogPeriod logPeriod = ag.getMembers().get(0).getLog().get(0);
        logPeriod.setLog(Lists.newArrayList(li1, li2));
        logPeriodRepository.save(logPeriod);

        ContactInfo ci1 = contactInfoRepository.save(new ContactInfo("main email",InfoType.EMAIL,"test@gmail.com","CFO"));
        ContactInfo ci2 = contactInfoRepository.save(new ContactInfo("main skype",InfoType.SKYPE,"cfotestskype","CFO"));
        c1.setContacts(Lists.newArrayList(ci1,ci2));
        clientRepository.save(c1);

        FinancialInfo f1 = financialInfoRepository.save(new FinancialInfo("NORVIK", "25689863232",Currency.USD,null));
        FinancialInfo f2 = financialInfoRepository.save(new FinancialInfo("PRIVAT", "25689863232",Currency.USD,null));
        c1.setBankAccounts(Lists.newArrayList(f1, f2));
        clientRepository.save(c1);
    }
}
