package org.freshcode.sevice;

import com.google.common.collect.Lists;
import org.freshcode.domain.*;
import org.freshcode.domain.enums.*;
import org.freshcode.domain.enums.Currency;
import org.freshcode.gui.utils.DateUtils;
import org.freshcode.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.*;

/**
 * Created by user on 04.08.16.
 */
@Service
@Transactional
public class OtherInitTestData {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    LeadRepository leadRepository;
    @Autowired
    BidRepository bidRepository;
    @Autowired
    ExpenseRepository expenseRepository;
    @Autowired
    AgreementRepository agreementRepository;
    @Autowired
    InvoiceRepository invoiceRepository;
    @Autowired
    MemberRepository memberRepository;
    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    LogPeriodRepository logPeriodRepository;
    @Autowired
    LogItemRepository logItemRepository;
    @Autowired
    InvoiceTemplateRepository invoiceTemplateRepository;
    @Autowired
    ContactInfoRepository contactInfoRepository;
    @Autowired
    FinancialInfoRepository financialInfoRepository;
    @Autowired
    FileRepository fileRepository;
    @Autowired
    DocumentRepository documentRepository;

    void putSomeData()
    {
        Client freshcode = new Client();
        freshcode.setName("FreshCode");
        freshcode.setAddress("division of BrightTech IT LP\n99/88 Mainstone Mains Stret\nEdinburg EQ8 1LB\n United Kingdom");
        freshcode =clientRepository.save(freshcode);

        FinancialInfo info = new FinancialInfo();
        info.setName("USD account");
        info.setAccount("LV33LATB00002024788");
        financialInfoRepository.save(info);

        freshcode.setBankAccounts(Lists.newArrayList(info));
        clientRepository.save(freshcode);

        Client icudbe = new Client();
        icudbe.setName("IC CUBE");
        icudbe.setAddress("Route de la Pierre 22\n1024 Ecublens\nSwitzerland");
        icudbe = clientRepository.save(icudbe);

        Invoice invoice = new Invoice();
        invoice.setDate(DateUtils.asUtilDate(LocalDate.of(2015,11,15)));
        invoice.setNumber("#01122015");
        invoice.setInvoiceState(Invoice.InvoiceState.PENDING);
        invoice.setPrice(new Price(800, Currency.USD));
        InvoiceRow r1 = new InvoiceRow("Worked hours for Motorniy Konstanin (02.11 - 30.11)", 100, 2f, 200f);
        InvoiceRow r2 = new InvoiceRow("Worked hours for Ryskal Sergey (02.11 - 30.11)", 100, 2f, 200f);
        invoice.setRows(Lists.newArrayList(r1, r2));
        invoice = invoiceRepository.save(invoice);

        Lead lead = new Lead("some lead1","---",LeadState.SENT, null, icudbe, null,null,null);
        lead.setPrice(new Price(800,Currency.USD));
        lead = leadRepository.save(lead);
        Agreement agreement = new Agreement("itext7test",new Date(),AgreementState.ACTIVE,lead, new Price(400, Currency.USD), AgreementType.FIXED);
        agreement = agreementRepository.save(agreement);
        invoice.setAgreement(agreement);
        invoice = invoiceRepository.save(invoice);


        Map<InvoiceTemplateOption,String> options = new HashMap<>();
        options.put(InvoiceTemplateOption.HEADER_BLOCK, "Project 'Report modernization', module 'Add - self - BI'");
        options.put(InvoiceTemplateOption.SHOW_DUE_DATE, "false");
        InvoiceTemplate template = new InvoiceTemplate(Currency.USD,icudbe,freshcode,freshcode.getBankAccounts().get(0),true,true,true,options);
        template = invoiceTemplateRepository.save(template);
        agreement.setInvoiceTemplate(template);
        agreement.setInvoices(Lists.newArrayList(invoice));
        agreementRepository.save(agreement);
    }
}
