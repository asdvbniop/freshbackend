package org.freshcode.templates;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.colorspace.PdfColorSpace;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.HorizontalAlignment;
import jdk.nashorn.internal.objects.NativeJava;
import org.freshcode.domain.Invoice;
import org.freshcode.domain.InvoiceRow;
import org.freshcode.domain.InvoiceTemplate;
import org.freshcode.domain.enums.InvoiceTemplateOption;
import org.freshcode.dto.InvoiceDto;
import org.freshcode.dto.InvoiceRowDto;
import org.freshcode.dto.InvoiceTemplateDto;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by user on 27.08.16.
 */
public class BasicInvoiceTemplate {
    public void render(InvoiceTemplate template, Invoice datasource) throws IOException {
        PdfWriter writer = new PdfWriter("invoice.pdf");
        PdfDocument pdf = new PdfDocument(writer);
        Document document = new Document(pdf);

        PdfFont font = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
        PdfFont bold = PdfFontFactory.createFont(FontConstants.TIMES_BOLD);
        Text company = new Text(template.getOurClient().getName()+"\n").setFont(bold);
        Text addr = new Text(template.getOurClient().getAddress()).setFont(font);
        Paragraph p = new Paragraph().add(company).add(addr).setMarginLeft(10);
        document.add(p);
        document.add(new Paragraph("\n Bill To:").setMarginLeft(10));

        Text billToComapany = new Text(template.getClient().getName()+"\n").setFont(bold);
        Text billAddr = new Text(template.getClient().getAddress()).setFont(font);
        Paragraph p1 = new Paragraph().add(billToComapany).add(billAddr).setMarginLeft(10);
        document.add(p1);

        if(template.getOptions().get(InvoiceTemplateOption.HEADER_BLOCK)!=null)
        {
            document.add(new Paragraph(template.getOptions().get(InvoiceTemplateOption.HEADER_BLOCK)));
        }

        Table table = new Table(new float[]{120,30,30,30});
        table.setWidthPercent(100f);

        table.addCell(headerCell("Item")).addCell(headerCell("Qty")).addCell(headerCell("Rate")).addCell(headerCell("Amount"));

        for (InvoiceRow rowDto : datasource.getRows()) {
            table.
                    addCell(bodyCell(rowDto.getItem()))
                    .addCell(bodyCell(rowDto.getQuantity().toString()))
                    .addCell(bodyCell(rowDto.getRate().toString()))
                    .addCell(bodyCell(rowDto.getAmount().toString()));
        }
        document.add(table.setBorder(Border.NO_BORDER));

        Paragraph total = new Paragraph();
        total.add("\nSubtotal:     $ "+datasource.getPrice().getValue().toString()+"\n");
        total.add("Total:     $ "+datasource.getPrice().getValue().toString()+"\n");
        total.setHorizontalAlignment(HorizontalAlignment.RIGHT);
        total.setMarginLeft(300);
        total.setMarginBottom(70);

        Paragraph test = new Paragraph().add(new Text("INVOICE").setBold().setFontSize(26).setBackgroundColor(Color.LIGHT_GRAY))
                .add(new Text("\n"+datasource.getNumber()).setFontColor(Color.DARK_GRAY))
                .setHorizontalAlignment(HorizontalAlignment.RIGHT)
                .setFixedPosition(1,400,750,150);

        document.add(test);

        document.add(total);
        document.close();
    }

    private Cell headerCell(String s)
    {
        Cell cell = new Cell().setBorder(Border.NO_BORDER);
        Paragraph paragraph = new Paragraph(s).setFontColor(Color.WHITE);
        cell.add(paragraph).setBackgroundColor(Color.DARK_GRAY);
        return cell;
    }

    private Cell bodyCell(String s)
    {
        Cell cell = new Cell().setBorder(Border.NO_BORDER);
        cell.add(new Paragraph(s).setFontSize(10));
        return cell;
    }


}
