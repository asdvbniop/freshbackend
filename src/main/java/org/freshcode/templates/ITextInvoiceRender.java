package org.freshcode.templates;

import org.freshcode.domain.Invoice;
import org.freshcode.domain.InvoiceTemplate;
import org.freshcode.dto.InvoiceDto;
import org.freshcode.dto.InvoiceTemplateDto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by user on 27.08.16.
 */
public interface ITextInvoiceRender {
    File render(InvoiceTemplate template, Invoice datasource) throws IOException;
}
