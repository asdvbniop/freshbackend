package org.freshcode.templates;

/**
 * Created by user on 10.03.17.
 */
public interface RenderInterface {
    public String render(String dest);
}
