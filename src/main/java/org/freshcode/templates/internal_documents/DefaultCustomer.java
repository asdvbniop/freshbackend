package org.freshcode.templates.internal_documents;

import org.freshcode.api.BankApi;
import org.freshcode.domain.Bank;
import org.freshcode.domain.Client;
import org.freshcode.domain.FinancialInfo;
import org.freshcode.domain.FullAddressData;

import java.util.ArrayList;

/**
 * Created by user on 14.03.17.
 */
public class DefaultCustomer {
    public static Client client=new Client();
    public static Bank avalBank = new Bank();

    static {
        client.setName("COFFEECUP IT LP");
        client.setAddress("39/5 GRANTON CRESCENT\n" +"EDINBURGH EH5 1BN\n" +"UNITED KINGDOM");

        Bank norvik = new Bank();
        norvik.setName("JSC NORVIK BANKA");
        norvik.setSwift("LATBLV22");
        FullAddressData norvikAddr = new FullAddressData();
        norvikAddr.setCountry("Latvia");
        norvikAddr.setCity("Riga");
        norvikAddr.setIndex("LV-1011");
        norvikAddr.setStreet("E.Birznieka-Upisa Street 21");
        norvik.setEng(norvikAddr);
        norvik.setRus(norvikAddr);

        client.setBankAccounts(new ArrayList<>());
        FinancialInfo info = new FinancialInfo();
        info.setAccount("LV40003072918");
        info.setBank(norvik);

        client.getBankAccounts().add(info);

        avalBank.setName("AVAL");
        avalBank.setSwift("AVALUAUKXXX");
        FullAddressData eng = new FullAddressData();
        eng.setName("Public Joint Stock Company \"Raiffeisen BANK AVAL\"");
        eng.setCountry("Ukraine");
        eng.setCity("Kiev");

        FullAddressData rus = new FullAddressData();
        rus.setName("АТ \"Райффайзен Банк Аваль\"");
        rus.setCountry("Украина");
        rus.setCity("Киев");
        avalBank.setEng(eng);
        avalBank.setRus(rus);
    }

}
