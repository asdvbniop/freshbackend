package org.freshcode.templates.internal_documents;

import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.AreaBreakType;
import com.itextpdf.layout.property.ListNumberingType;
import com.itextpdf.layout.property.TextAlignment;
import com.rits.cloning.Cloner;
import org.apache.commons.lang3.StringEscapeUtils;
import org.freshcode.domain.FileModel;
import org.freshcode.domain.InternalDocument;
import org.freshcode.domain.enums.EntityType;
import org.freshcode.domain.enums.FileType;
import org.freshcode.domain.enums.InternalDocumentType;
import org.freshcode.domain.enums.Language;
import org.freshcode.gui.utils.DateUtils;
import org.freshcode.templates.internal_documents.agrrement_data.*;
import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class InternalBaseTemplate {
    XmlTemplateReader reader = new XmlTemplateReader();
    Map<String,String> currentTemplateContext = new HashMap<>();
    ArrayList<Map<String, String>> contexts = new ArrayList<>();
    JtwigModel engModel = JtwigModel.newModel();
    JtwigModel rusModel = JtwigModel.newModel();
    protected Document document;
    protected Table currentTable;
    protected Counter counter = new Counter();

    interface Source{
        String get(Language language);
    }

    protected abstract void buildDataSource(InternalDocument source);
    protected abstract void renderDoc(InternalDocument source, String path);
    protected abstract String buildName(InternalDocument source);


    void restoreTemplateContext(){
        contexts.remove(currentTemplateContext);
        currentTemplateContext=contexts.get(contexts.size()-1);
    }

    void buildBaseDatasource(InternalDocument source)
    {
        addDataToDataSource("docName", source.getName());
        addDataToDataSource("docDate", l-> DateUtils.getDateAsString(source.getDate(), l));
        addDataToDataSource("userName", l->source.getPerson().getPersonData().getFullName(l));
        addDataToDataSource("userAddr", l->source.getPerson().getPersonData().getAddr(l));
        addDataToDataSource("customerName", DefaultCustomer.client.getName());
        addDataToDataSource("customerAddr",DefaultCustomer.client.getAddress());
        addDataToDataSource("fromDate",l->DateUtils.getDateAsString(source.getFromDate(),l));
        addDataToDataSource("toDate",l->DateUtils.getDateAsString(source.getToDate(),l));
    }

    void dumpTemplateContext(TemplateBlockElement element)
    {
        Cloner cloner = new Cloner();
        Map<String,String> newContext = cloner.deepClone(currentTemplateContext);
        contexts.add(newContext);
        currentTemplateContext=newContext;
        putToMap(newContext,"alignment",element.getAlignment());
        putToMap(newContext,"leading",element.getLeading());
        putToMap(newContext,"visible",element.getVisible());
        putToMap(newContext,"countMode",element.getCountMode());
        putToMap(newContext,"indent",element.getIndent());
        putToMap(newContext,"padding-top",element.getPaddingTop());
        putToMap(newContext,"padding", element.getPadding());
        putToMap(newContext, "firstLineIndent", element.getFirstLineIndent());
        putToMap(newContext, "padding-right", element.getPaddingRight());
    }

    private void putToMap(Map map, String key, String value)
    {
        if (value != null && !value.isEmpty())
            map.put(key, value);
    }

    static class Counter {


        enum Mode{NONE,MINOR,MAJOR;}
        int major = 1;
        int minor = 1;
        int nested = 1;
        void incMajor() {major++;minor = 1;nested=1;}
        void incMinor() {minor++;nested=1;}
        void incNested(){nested++;}
        public void decMinor() {minor--;}
    }

    public FileModel render(InternalDocument source) throws Exception {
        FileModel result = new FileModel();
        String path = source.getPerson().getUsername() + "/internal/" + buildName(source);
        prepareDoc(source,path);
        buildBaseDatasource(source);
        buildDataSource(source);
        renderDoc(source, path);
        document.close();
        result.setPath(source.getPerson().getUsername() + "/internal");
        result.setFilename(buildName(source));
        result.setParentType(EntityType.USER);
        result.setFileType(FileType.PDF);
        return result;
    }

    private void prepareDoc(InternalDocument internalDocument, String path) throws IOException {
        path = "upload-dir/" + path;
        new File(path).getParentFile().mkdirs();
        PdfWriter writer = new PdfWriter(path);
        PdfDocument pdf = new PdfDocument(writer);
        document = new Document(pdf);
        PdfFont regular = PdfFontFactory.createFont("noto-serif/NotoSerif-Regular.ttf", "Cp1251", true);

        PdfFontFactory.register("noto-serif/NotoSerif-Regular.ttf");
        PdfFontFactory.register("noto-serif/NotoSerif-Bold.ttf");

        document.setFont(regular);
        document.setFontSize(10);
    }

    String processString(String s, Language language)
    {
        JtwigTemplate template = JtwigTemplate.inlineTemplate(StringEscapeUtils.unescapeJava(s));
        switch (language) {
            case ENG:
                return template.render(engModel);
            case RUS:
                return template.render(rusModel);
        }
        return "!!!error!!!";
    }

    String processString(String s)
    {
        JtwigTemplate template = JtwigTemplate.inlineTemplate(StringEscapeUtils.unescapeJava(s));
        return template.render(engModel);
    }

    String getStringFromModel(String key, Language language)
    {
        switch (language) {
            case ENG:
                return (String) engModel.get(key).get().getValue();
            case RUS:
                return (String) rusModel.get(key).get().getValue();
        }
        return "!!!error!!!";
    }

    void buildBilingualDoc(Document document, XmlTemplateReader reader)
    {

        for (AgreementSection section : reader.templateData.getSections()) {
            dumpTemplateContext(section);
            buildSection(document,section);
            for (AgreementItem item : section.getItems()) {
                dumpTemplateContext(item);
                buildItem(document,item);
                if(item.getSkipNumber()==null || !item.getSkipNumber())
                    counter.incMinor();
                restoreTemplateContext();
            }
            restoreTemplateContext();
            if(section.getSkipNumber()==null || !section.getSkipNumber())
                counter.incMajor();
        }
    }


    private void buildItem(Document document, AgreementItem item) {
        InternalServiceAgreementAcceptance.Counter.Mode mode = InternalServiceAgreementAcceptance.Counter.Mode.valueOf(currentTemplateContext.get("countMode"));
        if(item.getSkipNumber()!=null && item.getSkipNumber())
            mode= InternalServiceAgreementAcceptance.Counter.Mode.NONE;
        currentTable.addCell(new Cell()
                .setPaddingLeft(Float.valueOf(currentTemplateContext.get("indent")))
                .setPaddingTop(Float.valueOf(currentTemplateContext.get("padding-top")))
                .setBorder(Border.NO_BORDER).add(
                        buildBlock(processString(item.getString(Language.RUS),Language.RUS),mode)));
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER));
        currentTable.addCell(new Cell()
                .setPaddingLeft(Float.valueOf(currentTemplateContext.get("indent")))
                .setPaddingTop(Float.valueOf(currentTemplateContext.get("padding-top")))
                .setBorder(Border.NO_BORDER).add(
                        buildBlock(processString(item.getString(Language.ENG),Language.ENG),mode)));
    }



    private void buildSection(Document document,AgreementSection section)
    {
        if (section.getOnNewPage() != null && section.getOnNewPage()) {
            document.add(currentTable);
            currentTable = new Table(new float[]{255, 15, 255}).setBorder(Border.NO_BORDER);
            document.add(new AreaBreak(AreaBreakType.NEXT_AREA));
        }
        if(Boolean.valueOf(currentTemplateContext.get("visible")))
        {
            addRow(l->"!!"+counter.major + ". " + section.getString(l)+"!!",currentTable,TextAlignment.CENTER);
        }
    }

    private BlockElement buildBlock(String text, InternalServiceAgreementAcceptance.Counter.Mode mode)
    {
        if(mode.equals(InternalServiceAgreementAcceptance.Counter.Mode.NONE)){
            return buildCell(text);
        }else{
            List list = new List(ListNumberingType.DECIMAL);
            if (mode.equals(InternalServiceAgreementAcceptance.Counter.Mode.MINOR)){
                list.setItemStartIndex(counter.minor);
            }
            else //MAJOR
            {
                list.setPreSymbolText(String.valueOf(counter.major) + ".");
                list.setItemStartIndex(counter.minor);

            }
            list.add(buildListItem(text));
            return list;
        }
    }

    private void addRow(InternalServiceAgreementAcceptance.Source source, Table table, TextAlignment alignment)
    {
        if(alignment==null)
            alignment=TextAlignment.LEFT;

        table.addCell(buildCell(source.get(Language.RUS)).setTextAlignment(alignment));
        table.addCell(new Cell().setBorder(Border.NO_BORDER));
        table.addCell(buildCell(source.get(Language.ENG)).setTextAlignment(alignment));
    }

    Cell buildCell(String text)
    {
        Cell cell = new Cell().setBorder(Border.NO_BORDER);
        Paragraph paragraph = buildParagraph(text);
        paragraph.setFixedLeading(Float.parseFloat(currentTemplateContext.get("leading")));
        paragraph.setFirstLineIndent(Float.parseFloat(currentTemplateContext.get("firstLineIndent")));
        cell.add(paragraph).setTextAlignment(TextAlignment.valueOf(currentTemplateContext.get("alignment")));
        cell.setPadding(Float.parseFloat(currentTemplateContext.get("padding")));
        cell.setPaddingTop(Float.parseFloat(currentTemplateContext.get("padding-top")));
        cell.setPaddingRight(Float.parseFloat(currentTemplateContext.get("padding-right")));
        cell.setPaddingLeft(Float.valueOf(currentTemplateContext.get("indent")));
        return cell;
    }

    ListItem buildListItem(String text)
    {
        ListItem listItem = new ListItem();
        Paragraph paragraph = buildParagraph(text);
        paragraph.setFixedLeading(Float.parseFloat(currentTemplateContext.get("leading")));
        paragraph.setCharacterSpacing(0.1f);
        paragraph.setTextAlignment(TextAlignment.valueOf(currentTemplateContext.get("alignment")));
        listItem.add(paragraph);
        return listItem;
    }

    Paragraph buildParagraph(String s)
    {
        ArrayList<Text> textList = new ArrayList<>();
        String[] split = s.split("!!");
        if(split.length<0)
            return new Paragraph();
        if(split.length==1)
            return new Paragraph(s);

        if(!split[0].isEmpty())
        {
            textList.add(new Text(split[0]));
        }

        for(int q=1;q<split.length;q++)
        {
            Text text = new Text(split[q]);
            text.setWordSpacing(0.1f);
            text.setCharacterSpacing(0.1f);

            if(q%2!=0)
            {
                text.setBold();
            }
            textList.add(text);
        }
        return new Paragraph().addAll(textList);
    }

    void addDataToDataSource(String name, Source source)
    {
        engModel.with(name,source.get(Language.ENG));
        rusModel.with(name,source.get(Language.RUS));
    }
    void addDataToDataSource(String name, String source)
    {
        engModel.with(name,source);
        rusModel.with(name,source);
    }

    public static InternalBaseTemplate getTemplate(InternalDocumentType type) {
        switch (type) {
            case INVOICE:
                return new InternalInvoiceTemplate();
            case RENT_SPACE:
                break;
            case RENT_EQUIPMENT:
                break;
            case STUDY_AGREEMENT:
                break;
            case SERVICE_AGREEMENT:
                return new InternalServiceAgreement();
            case SERVICE_AGREEMENT_ACCEPTANCE:
                return new InternalServiceAgreementAcceptance();
        }
        return null;
    }


}
