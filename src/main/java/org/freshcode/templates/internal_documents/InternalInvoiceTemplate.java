package org.freshcode.templates.internal_documents;

import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TextAlignment;
import javafx.util.Pair;
import org.freshcode.domain.InternalDocument;
import org.freshcode.domain.enums.Language;
import org.freshcode.gui.utils.DateUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 27.08.16.
 */
public class InternalInvoiceTemplate extends InternalBaseTemplate{
    static Map<String,Pair<String,String>> strings = new HashMap<>();

    public static void main(String args[])
    {
        InternalInvoiceTemplate t = new InternalInvoiceTemplate();
        try {
            t.render(new TestData().invoice);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void buildDataSource(InternalDocument source) {
        addDataToDataSource("agreeName",source.getParent().getName());
        addDataToDataSource("agreeDate",l-> DateUtils.getDateAsString(source.getParent().getFromDate(),l));
        addDataToDataSource("price", source.getPrice().asString());
    }

    @Override
    protected void renderDoc(InternalDocument source, String path) {
        document.setFontSize(12);

        renderHalf(Language.ENG);
        document.add(new Paragraph("").setHeight(80));

        PdfCanvas canvas = new PdfCanvas(document.getPdfDocument(),1);
        canvas.setStrokeColor(Color.BLACK)
                .setLineWidth(2)
                .moveTo(36, 440)
                .lineTo(570, 440)
                .closePathStroke();

        renderHalf(Language.RUS);
    }

    private void renderHalf(Language l)
    {
        Table table = new Table(new float[]{450,260}).setBorder(Border.NO_BORDER);
        table.setWidthPercent(100f);
        table.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph("").setHeight(60)));
        Paragraph invoiceHeader=buildParagraph(getString("header",l)).setTextAlignment(TextAlignment.RIGHT);
        table.addCell(new Cell().setBorder(Border.NO_BORDER).add(invoiceHeader));

        table.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(getString("from",l))));
        table.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(getString("to",l))));
        table.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph("").setHeight(10)));
        table.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph("").setHeight(10)));
        table.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(getStringFromModel("userAddr",l))));
        table.addCell(new Cell().setBorder(Border.NO_BORDER).add(new Paragraph(getStringFromModel("customerAddr",l))));

        document.add(table);
        document.add(new Paragraph("").setHeight(30));
        document.add(new Paragraph(getString("desc",l)));
        document.add(new Paragraph().add(new Text(getString("total",l))).setTextAlignment(TextAlignment.RIGHT));
    }

    private String getString(String key, Language language)
    {
        Pair<String,String> p = strings.get(key);
        return processString(language.equals(Language.ENG)?p.getKey():p.getValue(),language);
    }

    static {
        strings.put("header", new Pair<>("!!Invoice #{{docName}}!!\nDate: {{docDate}}","!!Счет № {{docName}}!!\nДата: {{docDate}}"));
        strings.put("from",new Pair<>("From: {{userName}}","От: {{userName}}"));
        strings.put("to", new Pair<>("To: {{customerName}}","Кому: {{customerName}}"));
        strings.put("total", new Pair<>("Total: {{price}}","Всего: {{price}}"));
        strings.put("desc", new Pair<>(
                "For services according to Service Agreement #{{agreeName}} of {{agreeDate}}\nSoftware development using JavaScript programming language.",
                "Согласно контракту № {{agreeName}} от {{agreeDate}}\nОписание работ: разработка программного обеспечения."));
    }

    @Override
    protected String buildName(InternalDocument source) {
        return "invoice-"+DateUtils.shortFormat.format(source.getDate())+".pdf";
    }
}
