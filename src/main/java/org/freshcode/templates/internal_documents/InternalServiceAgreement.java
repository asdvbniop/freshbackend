package org.freshcode.templates.internal_documents;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import org.freshcode.domain.FullAddressData;
import org.freshcode.domain.InternalDocument;
import org.freshcode.domain.enums.InternalDocumentType;
import org.freshcode.domain.enums.Language;
import org.freshcode.gui.utils.DateUtils;

import java.io.IOException;

/**
 * Created by user on 12.03.17.
 */
public class InternalServiceAgreement extends InternalBaseTemplate{

    public static void main(String args[]) {
        try {
            new InternalServiceAgreement().render(new TestData().agreement);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void buildDataSource(InternalDocument source) {
        addDataToDataSource("iban",DefaultCustomer.client.getBankAccounts().get(0).getAccount());
        addDataToDataSource("swift",DefaultCustomer.client.getBankAccounts().get(0).getBank().getSwift());
        addDataToDataSource("price",source.getPrice().toString());
        addDataToDataSource("usdAccount", source.getPerson().getPersonData().getUsdAcc().getAccount());
        addDataToDataSource("eurAccount", source.getPerson().getPersonData().getEurAcc().getAccount());
        addDataToDataSource("userBankAddr", l ->
                DefaultCustomer.avalBank.getFieldForLanguage(FullAddressData.Fields.NAME, l) + "\n" +
                        DefaultCustomer.avalBank.getFieldForLanguage(FullAddressData.Fields.CITY, l) + ", " +
                        DefaultCustomer.avalBank.getFieldForLanguage(FullAddressData.Fields.COUNTRY,l));
        addDataToDataSource("userBankSwift",DefaultCustomer.avalBank.getSwift());
    }

    protected void renderDoc(InternalDocument source, String path) {
        document.getPdfDocument().addEventHandler(PdfDocumentEvent.END_PAGE, new InternalServiceAgreement.TextFooterEventHandler(document, source));
        reader.load(InternalDocumentType.SERVICE_AGREEMENT);
        dumpTemplateContext(reader.templateData);
        currentTable= new Table(new float[]{255, 15, 255}).setBorder(Border.NO_BORDER);
        document.add(currentTable);

        //header
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER));
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER));
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER)
                .setPadding(-3)
                .add(new Paragraph(DefaultCustomer.client.getAddress())
                        .setTextAlignment(TextAlignment.RIGHT)
                        .setHeight(70)
                        .setFixedLeading(15)
                        .setBold()));

        //main body
        buildBilingualDoc(document,reader);

        //footer for sign
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(1.5f)).setHeight(250));
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER));
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(1.5f)));

        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER)
                .add(new Paragraph(DefaultCustomer.client.getName()+"\n"+"Remigijus Mikalauskas").setFontSize(13)));
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER));
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER)
                .add(new Paragraph(source.getPerson().getPersonData().getFullNameRus()+"\n"+
                        source.getPerson().getPersonData().getFullNameEng())).setFontSize(13));

        document.add(currentTable);
    }

    @Override
    protected String buildName(InternalDocument source) {
        return "agree-"+DateUtils.shortFormat.format(source.getDate())+".pdf";
    }

    private class TextFooterEventHandler implements IEventHandler {
        Document doc;
        InternalDocument document;
        TextFooterEventHandler(Document doc, InternalDocument document) {
            this.doc = doc;
            this.document=document;
        }

        @Override
        public void handleEvent(Event event) {
            PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
            PdfCanvas canvas = new PdfCanvas(docEvent.getPage());
            Rectangle pageSize = docEvent.getPage().getPageSize();
            canvas.beginText();
            try {
                canvas.setFontAndSize(PdfFontFactory.createFont(FontConstants.TIMES_ROMAN), 13);
                canvas.setColor(Color.GRAY,true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            canvas.moveText(pageSize.getLeft() + doc.getLeftMargin(), pageSize.getTop() - doc.getTopMargin() + 10)
                    .moveText(0, (pageSize.getBottom() + doc.getBottomMargin()) - (pageSize.getTop() + doc.getTopMargin()) + 48)
                    .showText("SERVICE AGREEMENT #"+document.getName()+" of "+DateUtils.getDateAsString(document.getFromDate(), Language.ENG))
                    .moveText(520,0)
                    .showText(String.valueOf(docEvent.getPage().getStructParentIndex()+2))
                    .endText()
                    .release();
        }
    }
}
