package org.freshcode.templates.internal_documents;

import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import javafx.util.Pair;
import org.freshcode.domain.InternalDocument;
import org.freshcode.domain.Price;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.InternalDocumentType;
import org.freshcode.domain.enums.Language;
import org.freshcode.gui.utils.DateUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 12.03.17.
 */
public class InternalServiceAgreementAcceptance extends InternalBaseTemplate{

    public static void main(String args[]) {
        try {
            new InternalServiceAgreementAcceptance().render(new TestData().agreement.getChildren().get(0).getAcceptance());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Map<Pair<Language,Currency>,String> constants = new HashMap<>();

    protected void buildDataSource(InternalDocument source) {
        constants.put(new Pair<>(Language.ENG,Currency.USD),"US dollars");
        constants.put(new Pair<>(Language.ENG,Currency.EUR),"EUR");
        constants.put(new Pair<>(Language.ENG,Currency.UAH),"UAH");

        constants.put(new Pair<>(Language.RUS,Currency.USD),"долларов США");
        constants.put(new Pair<>(Language.RUS,Currency.EUR),"евро");
        constants.put(new Pair<>(Language.RUS,Currency.UAH),"грн");

        addDataToDataSource("agreeName", source.getParent().getName());
        addDataToDataSource("agreeDate",l->DateUtils.getDateAsString(source.getToDate(), l));
        addDataToDataSource("price", l->formatPrice(source.getPrice(),l));
    }

    protected void renderDoc(InternalDocument source, String path) {
        reader.load(InternalDocumentType.SERVICE_AGREEMENT_ACCEPTANCE);
        dumpTemplateContext(reader.templateData);
        currentTable= new Table(new float[]{255, 15, 255}).setBorder(Border.NO_BORDER);
        document.add(currentTable);
        buildBilingualDoc(document,reader);

        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(1.5f)).setHeight(160));
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER));
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(1.5f)));

        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER)
                .add(new Paragraph(DefaultCustomer.client.getName()+"\n"+"Remigijus Mikalauskas").setFontSize(13)));
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER));
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER)
                .add(new Paragraph(source.getPerson().getPersonData().getFullNameRus()+"\n"+
                        source.getPerson().getPersonData().getFullNameEng())).setFontSize(13));

        document.add(currentTable);
    }

    private String formatPrice(Price price, Language language)
    {
        return price.getValue()+" "+constants.get(new Pair<>(language, price.getCurrency()));
    }

    @Override
    protected String buildName(InternalDocument source) {
        return "sAcceptance-"+DateUtils.shortFormat.format(source.getDate())+".pdf";
    }
}
