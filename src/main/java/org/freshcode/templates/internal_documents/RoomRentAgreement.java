package org.freshcode.templates.internal_documents;

import com.ibm.icu.text.Transliterator;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.ListNumberingType;
import com.itextpdf.layout.property.TextAlignment;
import org.freshcode.domain.InternalDocument;
import org.freshcode.domain.enums.InternalDocumentType;
import org.freshcode.domain.enums.Language;
import org.freshcode.gui.utils.DateUtils;
import org.freshcode.templates.internal_documents.agrrement_data.AgreementItem;
import org.freshcode.templates.internal_documents.agrrement_data.AgreementPoint;
import org.freshcode.templates.internal_documents.agrrement_data.AgreementSection;

/**
 * Created by user on 12.03.17.
 */
public class RoomRentAgreement extends InternalBaseTemplate{

    public static void main(String args[]) {
        try {
            new RoomRentAgreement().render(new TestData().rentSpace);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void buildDataSource(InternalDocument source) {
        addDataToDataSource("price", "1500 грн.");
    }

    protected void renderDoc(InternalDocument source, String path) {
        reader.load(InternalDocumentType.RENT_SPACE);
        dumpTemplateContext(reader.templateData);


        document.add(buildParagraph(processString(reader.getString("header"))).setTextAlignment(TextAlignment.CENTER));




        currentTable= new Table(new float[]{255, 15, 255}).setBorder(Border.NO_BORDER);
        currentTable.addCell(buildCell("м. Запоріжжя").setTextAlignment(TextAlignment.LEFT));
        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER));
        currentTable.addCell(buildCell(DateUtils.getDateAsString(source.getDate(),Language.RUS)).setTextAlignment(TextAlignment.RIGHT));
        document.add(currentTable);

        AgreementPoint p = reader.getPoint("header1");

        document.add(new Paragraph(processString(p.getText())).setFirstLineIndent(50));
        p = reader.getPoint("header2");
        document.add(new Paragraph(processString(p.getText())).setFirstLineIndent(50).setPaddingTop(-6));

        currentTable = new Table(1);
        currentTable.setBorder(Border.NO_BORDER);

        for (AgreementSection section : reader.templateData.getSections()) {
            currentTable.addCell(buildCell(processString("!!"+counter.major + ". " + section.getTitle()+"!!")).setTextAlignment(TextAlignment.CENTER));
            boolean wasNested=false;
            for (AgreementPoint point : section.getPoints()) {
                dumpTemplateContext(point);
                if (point.getSkipNumber()!=null && point.getSkipNumber()) {
                    currentTable.addCell(buildCell(point.getText()));
                } else {
                    List l = new List(ListNumberingType.DECIMAL);
                    l.add(buildListItem(processString(point.getText())));

                    if (point.isNested()) {
                        if (!wasNested) {
                            counter.decMinor();
                            wasNested = true;
                        }
                        l.setPreSymbolText(String.valueOf(counter.major) + "." + String.valueOf(counter.minor) + ".");
                        l.setItemStartIndex(counter.nested);
                        counter.incNested();
                    } else {
                        if (wasNested) {
                            counter.incMinor();
                            wasNested = false;
                        }
                        l.setPreSymbolText(String.valueOf(counter.major) + ".");
                        l.setItemStartIndex(counter.minor);
                        counter.incMinor();
                    }
                    currentTable.addCell(new Cell()
                            .setPaddingLeft(Float.valueOf(currentTemplateContext.get("indent")))
                            .setPaddingTop(Float.valueOf(currentTemplateContext.get("padding-top")))
                            .setBorder(Border.NO_BORDER)
                            .add(l));
                }
                restoreTemplateContext();
            }
            counter.incMajor();
        }
        document.add(currentTable);

        document.add(buildParagraph(processString(reader.getString("footer"))).setTextAlignment(TextAlignment.CENTER));
        currentTable = new Table(new float[]{255, 15, 255}).setBorder(Border.NO_BORDER);

        currentTable.addCell(buildCell("ОРЕНДОДАВЕЦЬ"));
        currentTable.addCell(buildCell(""));
        currentTable.addCell(buildCell("СУБОРЕНДАР"));

        currentTable.addCell(buildCell(processString(reader.getString("details"))));
        currentTable.addCell(buildCell(""));
        currentTable.addCell(buildCell(processString(reader.getString("details"))));

        currentTable.addCell(buildCell("\nЗА ОРЕНДОДАВЕЦЯ"));
        currentTable.addCell(buildCell(""));
        currentTable.addCell(buildCell("\nЗА СУБОРЕНДАРЯ"));

        currentTable.addCell(buildCell("ФОП Моторний Костянтин Олегович"));
        currentTable.addCell(buildCell(""));
        currentTable.addCell(buildCell("ФОП Коваль Владислав Ярославович"));


        currentTable.addCell(buildCell("\n\n\n\nм.п."));
        currentTable.addCell(buildCell(""));
        currentTable.addCell(buildCell("\n\n\n\nм.п."));




//        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(1.5f)).setHeight(200));
//        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER));
//        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(1.5f)));
//
//        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER)
//                .add(new Paragraph(DefaultCustomer.client.getName()+"\n"+"Remigijus Mikalauskas").setFontSize(13)));
//        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER));
//        currentTable.addCell(new Cell().setBorder(Border.NO_BORDER)
//                .add(new Paragraph(source.getPerson().getPersonData().getFullNameRus()+"\n"+
//                        source.getPerson().getPersonData().getFullNameEng())).setFontSize(13));
//
document.add(currentTable);
    }

    @Override
    protected String buildName(InternalDocument source) {
        return "roomRent-"+DateUtils.shortFormat.format(source.getDate())+".pdf";
    }
}
