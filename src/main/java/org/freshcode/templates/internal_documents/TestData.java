package org.freshcode.templates.internal_documents;

import com.google.common.collect.Lists;
import org.freshcode.domain.*;
import org.freshcode.domain.enums.Currency;
import org.freshcode.domain.enums.InternalDocumentType;

import java.util.Date;

/**
 * Created by user on 14.03.17.
 */
public class TestData {

    public Person person;
    public InternalDocument invoice;
    public InternalDocument agreement;
    public InternalDocument rentSpace;

    public TestData()
    {
        Person p1 = new Person();
        p1.setUsername("test");

        PersonFullData data = new PersonFullData();
        data.setFullNameRus("Бодня Владислав Юрьевич");
        data.setFullNameEng("Bodnya Vladislav Yurievich");
        //71700, Запорізька обл., місто Токмак, вул. Пролетарська, буд. 10, кв. 63
        FullAddressData engAddr = new FullAddressData();
        engAddr.setCity("Tokmak");
        engAddr.setCountry("Ukraine");
        engAddr.setIndex("71700");
        engAddr.setStreet("Proletarska 10");
        engAddr.setFlat("63");
        engAddr.setDistrict("Zaporiz'ka");
        data.setEngAddr(engAddr);


        FullAddressData rusAddr = new FullAddressData();
        rusAddr.setCity("Токмак");
        rusAddr.setCountry("Украина");
        rusAddr.setIndex("71700");
        rusAddr.setStreet("Пролетарская 10");
        rusAddr.setFlat("63");
        rusAddr.setDistrict("Запорожская");
        data.setRusAddr(rusAddr);
        p1.setPersonData(data);

        person = p1;

        FinancialInfo usd = new FinancialInfo("","26001556487", Currency.USD,null);
        FinancialInfo eur = new FinancialInfo("","26009556489", Currency.EUR,null);
        person.getPersonData().setEurAcc(eur);
        person.getPersonData().setUsdAcc(usd);


        InternalDocument iAgreement = new InternalDocument();
        iAgreement.setType(InternalDocumentType.SERVICE_AGREEMENT);
        iAgreement.setPerson(p1);
        iAgreement.setName("CCI-20-20220");
        iAgreement.setFromDate(new Date());
        iAgreement.setToDate(new Date());
        iAgreement.setDate(new Date());
        iAgreement.setPrice(new Price(888,Currency.EUR));

        agreement = iAgreement;

        InternalDocument invoiceDoc = new InternalDocument();
        invoiceDoc.setType(InternalDocumentType.INVOICE);
        invoiceDoc.setPerson(p1);
        invoiceDoc.setDate(new Date());
        invoiceDoc.setToDate(new Date());
        invoiceDoc.setFromDate(new Date());
        invoiceDoc.setName("INV-20-20220");
        invoiceDoc.setPrice(new Price(6000, Currency.USD));
        invoiceDoc.setParent(iAgreement);

        InternalDocument acceptance = new InternalDocument();
        acceptance.setParent(iAgreement);
        acceptance.setName("0102354");
        acceptance.setDate(new Date());
        acceptance.setType(InternalDocumentType.SERVICE_AGREEMENT_ACCEPTANCE);
        acceptance.setFromDate(new Date());
        acceptance.setToDate(new Date());
        acceptance.setPrice(new Price(5000, Currency.EUR));
        acceptance.setPerson(person);

        invoiceDoc.setAcceptance(acceptance);

        iAgreement.setChildren(Lists.newArrayList(invoiceDoc));
        invoice=invoiceDoc;

        rentSpace= new InternalDocument();
        rentSpace.setName("lsdwsdkw");
        rentSpace.setFromDate(new Date());
        rentSpace.setDate(new Date());
        rentSpace.setToDate(new Date());
        rentSpace.setPrice(new Price(1500,Currency.UAH));
        rentSpace.setPerson(p1);
        rentSpace.setType(InternalDocumentType.RENT_SPACE);
    }
}
