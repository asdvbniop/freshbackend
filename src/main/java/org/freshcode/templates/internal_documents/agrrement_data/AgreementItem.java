package org.freshcode.templates.internal_documents.agrrement_data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.Language;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

import javax.validation.constraints.AssertTrue;

/**
 * Created by user on 12.03.17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Root(name = "item")
public class AgreementItem extends TemplateBlockElement implements LanguageInterface{
    @Element
    private String eng;

    @Element
    private String rus;

    @Attribute(required = false)
    private String name;

    @Attribute(required = false)
    private Boolean skipNumber;

    @Override
    public String getString(Language language) {
        switch (language) {
            case ENG:
                return eng;
            case RUS:
                return rus;
        }
        return "!!!error!!!";
    }
}
