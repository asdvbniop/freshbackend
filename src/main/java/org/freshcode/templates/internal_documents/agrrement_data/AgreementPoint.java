package org.freshcode.templates.internal_documents.agrrement_data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.Language;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

/**
 * Created by user on 12.03.17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Root(name = "item")
public class AgreementPoint extends TemplateBlockElement{
    @Text
    String text;

    @Attribute(required = false)
    private String name;

    @Attribute(required = false)
    private Boolean skipNumber;

    @Attribute(required = false)
    private boolean nested;
}
