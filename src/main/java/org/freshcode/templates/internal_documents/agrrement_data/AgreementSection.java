package org.freshcode.templates.internal_documents.agrrement_data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.freshcode.domain.enums.Language;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by user on 12.03.17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Root(name = "section")
public class AgreementSection extends TemplateBlockElement implements LanguageInterface{
    @ElementList(name = "items", type = AgreementItem.class, required = false)
    private List<AgreementItem> items;

    @ElementList(name = "points", type = AgreementPoint.class, required = false)
    private List<AgreementPoint> points;

    @Element(name = "titles", required = false)
    private AgreementSectionTitles titles;

    @Element(required = false)
    private String title;

    @Attribute(required = false)
    private String name;

    @Attribute(required = false)
    private Boolean onNewPage;

    @Attribute(required = false)
    private Boolean skipNumber;

    @Override
    public String getString(Language language) {
        switch (language) {
            case ENG:
                return titles.getEng();
            case RUS:
                return titles.getRus();
        }
        return "!!!error!!!";
    }
}
