package org.freshcode.templates.internal_documents.agrrement_data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by user on 12.03.17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Root(name = "titles")
public class AgreementSectionTitles extends TemplateBlockElement{
    @Element
    private String eng;

    @Element
    private String rus;
}
