package org.freshcode.templates.internal_documents.agrrement_data;

import org.freshcode.domain.enums.Language;

/**
 * Created by user on 16.03.17.
 */
public interface LanguageInterface {
    String getString(Language language);

}
