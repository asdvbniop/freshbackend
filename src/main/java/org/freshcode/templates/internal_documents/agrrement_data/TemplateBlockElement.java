package org.freshcode.templates.internal_documents.agrrement_data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.simpleframework.xml.Attribute;

/**
 * Created by user on 15.03.17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemplateBlockElement {
    @Attribute(required = false)
    private String alignment;

    @Attribute(required = false)
    private String leading;

    @Attribute(required = false)
    private String visible;

    @Attribute(required = false)
    private String countMode;

    @Attribute(required = false)
    private String indent;

    @Attribute(required = false)
    private String padding;

    @Attribute(required = false)
    private String firstLineIndent;

    @Attribute(required = false, name = "padding-right")
    private String paddingRight;


    @Attribute(required = false, name = "padding-top", empty = "0")
    String paddingTop;
}
