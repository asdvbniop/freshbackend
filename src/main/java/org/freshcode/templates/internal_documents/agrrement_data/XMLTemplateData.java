package org.freshcode.templates.internal_documents.agrrement_data;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.simpleframework.xml.*;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.util.List;

/**
 * Created by user on 12.03.17.
 */
@Root(name="root")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class XMLTemplateData extends TemplateBlockElement{

    @ElementList(name = "sections", entry = "section", type = AgreementSection.class,inline = false)
    private List<AgreementSection> sections;

    @ElementList(name = "items",entry = "item", type = AgreementItem.class,inline = false,required = false)
    private List<AgreementItem> items;

    @ElementList(name = "points",entry = "point", type = AgreementPoint.class,inline = false,required = false)
    private List<AgreementPoint> points;



}
