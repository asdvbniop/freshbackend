package org.freshcode.templates.internal_documents.agrrement_data;

import org.freshcode.domain.InternalDocument;
import org.freshcode.domain.enums.InternalDocumentType;
import org.freshcode.domain.enums.Language;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by user on 14.03.17.
 */
public class XmlTemplateReader {

    public static HashMap<InternalDocumentType,String> urls = new HashMap<>();
    static {
        urls.put(InternalDocumentType.SERVICE_AGREEMENT,"internalAgreement.xml");
        urls.put(InternalDocumentType.SERVICE_AGREEMENT_ACCEPTANCE,"internalAgreementAcceptance.xml");
        urls.put(InternalDocumentType.RENT_SPACE, "roomRentAgreement.xml");
    }

    public XMLTemplateData templateData;

    public String getString(String key, Language language)
    {
        for (AgreementItem item : templateData.getItems()) {
            if(item.getName().equals(key))
            {
                return item.getString(language);
            }
        }
        return "!!!key-"+key+" doesn't exist!!!";
    }

    public String getString(String key)
    {
        for (AgreementPoint point : templateData.getPoints()) {
            if(point.getName().equals(key))
            {
                return point.getText();
            }
        }
        return "!!!key-"+key+" doesn't exist!!!";
    }

    public AgreementPoint getPoint(String key)
    {
        for (AgreementPoint point : templateData.getPoints()) {
            if(point.getName().equals(key))
            {
                return point;
            }
        }
        return null;
    }

    public void load(InternalDocumentType type)
    {
        Serializer serializer = new Persister();
        try {
            templateData = serializer.read(XMLTemplateData.class,getClass().getClassLoader().getResource("templates/"+urls.get(type)).openStream(), false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
