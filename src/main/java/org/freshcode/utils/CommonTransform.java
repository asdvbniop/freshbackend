package org.freshcode.utils;

import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.loader.api.BeanMappingBuilder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CommonTransform {
    public static <T> List<T> transformBeans(final Collection<?> sourceList, final Class<T> destination) {
        List<T> result = new ArrayList<>();
        sourceList.forEach(o -> result.add(transformBean(o, destination)));
        return result;
    }

    public static <T> T transformBean(final Object source, final Class<T> destination) {
        return source == null ? null : DozerBeanMapperSingletonWrapper.getInstance().map(source, destination);
    }

    public static void copyBean(final Object source, final Object destination) {
        DozerBeanMapperSingletonWrapper.getInstance().map(source, destination);
    }
}
