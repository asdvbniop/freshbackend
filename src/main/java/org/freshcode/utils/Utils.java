package org.freshcode.utils;

import com.ibm.icu.text.Transliterator;
import lombok.NoArgsConstructor;
import org.freshcode.domain.Invoice;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;

@Service
@NoArgsConstructor
public class Utils implements ApplicationContextAware {

    private static final Utils INSTANCE = new Utils();
    private static ApplicationContext applicationContext;

    public static Transliterator tru = Transliterator.getInstance("russian-latin/bgn");
    public static Transliterator trua = Transliterator.getInstance("ukrainian-latin/bgn");

    public static void autowire(Object classToAutowire, Object... beansToAutowireInClass) {
        for (Object bean : beansToAutowireInClass) {
            if (bean == null) {
                applicationContext.getAutowireCapableBeanFactory().autowireBean(classToAutowire);
            }
        }
    }

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) {
        Utils.applicationContext = applicationContext;
    }

    public static Utils getInstance() {
        return INSTANCE;
    }

    public static int getCurrentTime() {
        return (int) (System.currentTimeMillis() / 1000);
    }

    public static <T> List<T> alw(final List<T> sourceList, T object) {
        Assert.notNull(object);
        if (sourceList == null) {
            return Collections.singletonList(object);
        } else {
            sourceList.add(object);
            return sourceList;
        }
    }

    public static <T> List<T> alwn(final List<T> sourceList, T object) {
        if (sourceList == null) {
            if (object == null) {
                return new ArrayList<>();
            } else {
                return Collections.singletonList(object);
            }
        } else {
            if (object != null) {
                sourceList.add(object);
            }
            return sourceList;
        }
    }

    public static <T> List<T> rlw(final List<T> sourceList, T object) {
        Assert.notNull(object);
        if (sourceList == null) {
            return new ArrayList<>();
        } else {
            sourceList.remove(object);
            return sourceList;
        }
    }

    public static <T> List<T> rlw(final List<T> sourceList, final List<T> toRemove) {
        Assert.notNull(toRemove);
        if (sourceList == null) {
            return new ArrayList<>();
        } else {
            sourceList.removeAll(toRemove);
            return sourceList;
        }
    }

    public static <T> List<T> alwl(final List<T> sourceList) {
        return sourceList == null ? new ArrayList<>() : sourceList;
    }

    public static <T> List<T> alw2(final List<T> sourceList) {
        List<T> result = new ArrayList<>();
        if (sourceList != null) {
            result.addAll(sourceList);
        }
        return result;
    }

    public static <T> List<T> alw2wc(final List<T> sourceList) {
        List<T> result = new ArrayList<>();
        if (sourceList != null) {
            result.addAll(sourceList);
            sourceList.clear();
        }
        return result;
    }

    public static <T> List<T> union(List<T> list1, List<T> list2) {
        Set<T> set = new HashSet<>(list1);
        set.addAll(list2);
        return new ArrayList<>(set);
    }

    public static <R> List<String> mapEnumToString(Class<R> r) {
        for (R value : r.getEnumConstants()) {

        }

        return null;
    }
}
